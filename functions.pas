unit functions;
{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}
interface
function substrpos(substr, str: string): integer;
function keypos(k, s: string): integer;

function SecondsToFmtStr(seconds: longint): string;
function whereisfile(fname: string; wait: boolean): string;
function UTF8toLatin1(utfstring: ansistring): ansistring;
function pos_(substr, s: string): integer;

implementation
uses process,
  classes;

function substrpos(substr, str: string): integer;
var
  i, y: integer;
  possubstr: integer;
  b: boolean;
begin
  possubstr := 0;
  b := false;

  if length(str) >= length(substr)
    then
    for i := 1 to length(str) do
    begin
      if str[i] = substr[1]
        then
        if length(substr) > 1
          then
        begin
          b := true;
          for y := 2 to length(substr) do
          begin
            b := true;
            if substr[y] <> str[i + y - 1] then
            begin
              b := false;
              break;
            end;
          end;

          if b = true
            then
          begin
            possubstr := i;
            break;
          end;
        end
        else
        begin
          possubstr := i;
          break;
        end;
    end;
  result := possubstr;
end;

function keypos(k, s: string): integer;
var
  i: integer;
begin

  result := pos(k, s);
end;

function pos_(substr, s: string): integer;
begin
  result := system.pos(substr, s);
end;

function SecondsToFmtStr(seconds: longint): string;
var
  min, sec, hour: longint;
  sm, ss, sh: string;
begin
  if seconds > 0 then
  begin
    hour := seconds div (60 * 60);

    if hour = 0
      then min := seconds div 60
    else min := (seconds - 60 * 60 * hour) div 60;

    sec := seconds mod 60;

    str(min, sm);
    str(sec, ss);
    str(hour, sh);

    if min < 10 then sm := '0' + sm;
    if sec < 10 then ss := '0' + ss;
    if hour < 10 then sh := '0' + sh;

    result := sh + ':' + sm + ':' + ss;
  end
  else Result := '00:00:00';
end;

function whereisfile(fname: string; wait: boolean): string;
var
  p: tprocess;
  sl: tstringlist;
  s: string;
begin
  p := tprocess.create(nil);
  sl := tstringlist.create;
  if wait then p.options := p.options + [powaitonexit, pousepipes];
  if not wait then p.options := p.options + [pousepipes];
  p.commandline := 'whereis ' + fname;
  p.execute;
  sl.loadfromstream(p.output);
  s := sl[0];
  sl.free;
  p.free;
  delete(s, 1, pos(':', s));
  if s = '' then
  begin
    result := s;
    exit;
  end;
  delete(s, 1, 1);
  delete(s, pos(fname, s), length(s) - pos(fname, s) + 1);
  result := s;
end;

function UTF8toLatin1(utfstring: ansistring): ansistring;

var
  i: integer;
  tmps: string;
  utf16: boolean;
begin
  i := 0;
  tmps := '';
  utf16 := false;

  if length(utfstring) > 0 then
  begin
    repeat
      begin
        inc(i);
        case byte(utfstring[i]) of
          $FF: if byte(utfstring[i + 1]) = $FE then utf16 := true;
          $C3:
            begin
              delete(utfstring, i, 1);
              utfstring[i] := char(byte(utfstring[i]) + 64);
            end;
          $C2:
            begin
              delete(utfstring, i, 1);
              dec(i);
            end;
        end;
      end;
    until (i >= length(utfstring) - 1) or utf16;

    if utf16 then
    begin
      i := i + 2;
      writeln('utf16');
      repeat
        begin
          inc(i);
          if byte(utfstring[i]) <> 0 then tmps := tmps + utfstring[i];
        end;
      until (i >= length(utfstring));
    end;
  end;

  if not utf16 then result := utfstring
  else Result := tmps;
end;

end.
