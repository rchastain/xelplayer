unit copyform;
{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}
interface
uses
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  mseforms,
  msesplitter,
  msesimplewidgets,
  msewidgets,
  msetimer,
  msegraphedits,
  mseifiglob,
  msetypes;
type
  tcopyfo = class(tmseform)
    s_top: tspacer;
    trichbutton1: trichbutton;
    timer_move: ttimer;
    l_: tlabel;
    pb: tprogressbar;
    procedure on_move(const sender: TObject);
    procedure on_childmouseevens(const sender: twidget;
      var ainfo: mouseeventinfoty);
    procedure on_close(const sender: TObject);
  end;
var
  copyfo: tcopyfo;
  moveXX, moveYY: integer;

implementation
uses
  copyform_mfm;

procedure tcopyfo.on_move(const sender: TObject);
begin
  left := gui_getpointerpos.x - moveXX;
  top := gui_getpointerpos.y - moveYY;
end;

procedure tcopyfo.on_childmouseevens(const sender: twidget;
  var ainfo: mouseeventinfoty);
begin
  if (ainfo.eventkind = EK_BUTTONPRESS) then
  begin
    bringtofront;
    if (s_top = sender) or (copyfo = sender) then
    begin
      moveXX := gui_getpointerpos.x - left;
      moveYY := gui_getpointerpos.y - top;
      timer_move.enabled := true;
    end;
  end;
  if (ainfo.eventkind = EK_BUTTONRELEASE) then
  begin
    if timer_move.enabled then timer_move.enabled := false;
  end;
end;

procedure tcopyfo.on_close(const sender: TObject);
begin
  close;
end;

end.
