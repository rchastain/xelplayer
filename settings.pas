unit settings;
{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}
interface
uses
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  mseforms,
  msesplitter,
  msesimplewidgets,
  msewidgets,
  msedock,
  msetimer,
  msescrollbar,
  msestatfile,
  msestream,
  msestrings,
  msetabs,
  sysutils,
  msebitmap,
  msedataedits,
  msedatanodes,
  mseedit,
  msefiledialog,
  msegrids,
  mseifiglob,
  mselistbrowser,
  msesys,
  msetypes,
  msegraphedits,
  mseterminal,
  msefileutils,
  msecolordialog,
  msewidgetgrid,
  typinfo,
  mseact,
  mseimage;

type
  tsetsfo = class(tmseform)
    s_top: tspacer;
    trichbutton1: trichbutton;
    tdockhandle1: tdockhandle;
    timer_move: ttimer;
    timer_resize: ttimer;
    ttabwidget1: ttabwidget;
    ttabpage1: ttabpage;
    ttabpage6: ttabpage;
    tscrollbox4: tscrollbox;
    be_indexing: tbooleanedit;
    se_additional_params: tstringedit;
    se_additional_video_params: tstringedit;
    se_webcam_preload: tstringedit;
    be_aval_ao: tbooleanedit;
    edit_videooutput: tdropdownlistedit;
    edit_audiooutput: tdropdownlistedit;
    edit_mplayercache: tdropdownlistedit;
    edit_mplayerpath: tfilenameedit;
    tp_devices: ttabpage;
    b_update_devices: trichbutton;
    edit_webcam: tdropdownlistedit;
    edit_tvtuner: tdropdownlistedit;
    edit_cdrom: tdropdownlistedit;
    ttabpage7: ttabpage;
    edit_command: tstringedit;
    button_send: trichbutton;
    sg_debug: tstringgrid;
    trichbutton3: trichbutton;
    be_writetoconsolewithmplayeroutput: tbooleanedit;
    be_writetoconsole: tbooleanedit;
    be_usefifo: tbooleanedit;
    be_showplayling_debug: tbooleanedit;
    tbooleanedit2: tbooleanedit;
    be_doublebuf: tbooleanedit;
    be_noscreensaver: tbooleanedit;
    ttabwidget2: ttabwidget;
    ttabpage3: ttabpage;
    tscrollbox2: tscrollbox;
    ttabpage4: ttabpage;
    edit_magic: tfilenameedit;
    be_showonlymediafiles: tbooleanedit;
    edit_navigatorhome: tdirdropdownedit;
    tscrollbox1: tscrollbox;
    be_ask_forexit: tbooleanedit;
    trichbutton4: trichbutton;
    se_videoID: tstringedit;
    term: tterminal;
    ber_on_desktop: tbooleanedit;
    b_dubblevideowimdow: tbooleanedit;
    be_nexttrackallow: tbooleanedit;
    be_allowloademblems: tbooleanedit;
    edit_playlistsdir: tdirdropdownedit;
    be_useonecopy: tbooleanedit;
    tspacer1: tspacer;
    trichbutton6: trichbutton;
    trichbutton9: trichbutton;
    tspacer2: tspacer;
    edit_: tedit;
    ttabpage2: ttabpage;
    edit_scheme: tdropdownlistedit;
    trichbutton7: trichbutton;
    trichbutton8: trichbutton;
    edit_language: tdropdownlistedit;
    trichbutton5: trichbutton;
    trichbutton10: trichbutton;
    edit_icons: tdropdownlistedit;
    im2: timage;
    im: timage;
    procedure on_resize(const sender: TObject);
    procedure on_move(const sender: TObject);
    procedure on_childmouseevens(const sender: twidget;
      var ainfo: mouseeventinfoty);
    procedure on_close(const sender: TObject);
    procedure ON_CLOSEFORM(const sender: TObject);
    procedure on_choose_playlist_dir(const sender: TObject);
    procedure on_set_screenshots_dirs(const sender: TObject);
    procedure on_setallowemblems_change(const sender: TObject);
    procedure on_set_video_on_desktop(const sender: TObject; var avalue: Boolean;
      var accept: Boolean);
    procedure on_choose_window_for_videooutput(const sender: TObject);
    procedure on_recivetext(const sender: TObject; var atext: AnsiString;
      const errorinput: Boolean);
    procedure on_setNextTrackAllow(const sender: TObject; var avalue: Boolean;
      var accept: Boolean);

    procedure on_set_dubble_videowindow(const sender: TObject;
      var avalue: Boolean; var accept: Boolean);
    procedure on_set_mplayer(const sender: TObject; var avalue: msestring;
      var accept: Boolean);
    procedure on_setcache(const sender: TObject; var avalue: msestring;
      var accept: Boolean);
    procedure on_setaudioOutput(const sender: TObject; var avalue: msestring;
      var accept: Boolean);
    procedure on_aval_audio_only(const sender: TObject; var avalue: Boolean;
      var accept: Boolean);
    procedure on_setvideooutput(const sender: TObject; var avalue: msestring;
      var accept: Boolean);
    procedure on_set_webcampreload(const sender: TObject);
    procedure on_set_additional_videoparam(const sender: TObject);
    procedure on_set_additional_params(const sender: TObject);
    procedure on_setaudiocddevice(const sender: TObject; var avalue: msestring;
      var accept: Boolean);
    procedure on_settvtunerdevice(const sender: TObject; var avalue: msestring;
      var accept: Boolean);
    procedure on_setwebcameradevice(const sender: TObject; var avalue: msestring;
      var accept: Boolean);
    procedure on_update_devices(const sender: TObject);
    procedure on_setwriteouttoconsole(const sender: TObject; var avalue: Boolean;
      var accept: Boolean);
    procedure on_addtodebugmplayeroutput(const sender: TObject;
      var avalue: Boolean; var accept: Boolean);
    procedure button_send_onexecute(const sender: TObject);
    procedure on_clear_messages(const sender: TObject);
    procedure on_defocus(const sender: TObject);
    procedure on_set_use_fifo(const sender: TObject; var avalue: Boolean;
      var accept: Boolean);
    procedure on_setindexing(const sender: TObject; var avalue: Boolean;
      var accept: Boolean);
    procedure be_show_process_debug(const sender: TObject; var avalue: Boolean;
      var accept: Boolean);
    procedure on_changeactivetab(const sender: TObject);
    procedure on_load_sheme(const sender: TObject);
    procedure on_edit_sheme(const sender: TObject);
    procedure on_choose_sheme(const sender: TObject; var avalue: msestring;
      var accept: Boolean);
    procedure on_scroll(const sender: twidget; var ainfo: naviginfoty);
    procedure on_set_useonecopy(const sender: TObject; var avalue: Boolean;
      var accept: Boolean);
    procedure on_set_screenshotdir(const sender: TObject; var avalue: msestring;
      var accept: Boolean);
    procedure on_set_showonlymediafiles(const sender: TObject;
      var avalue: Boolean; var accept: Boolean);
    procedure on_set_doublebuf(const sender: TObject; var avalue: Boolean;
      var accept: Boolean);
    procedure on_set_magicfile_path(const sender: TObject; var avalue: msestring;
      var accept: Boolean);
    procedure on_set_lang(const sender: TObject);
    procedure on_copymessages(const sender: TObject);
    procedure on_load_icons(const sender: TObject);
  end;
var
  setsfo: tsetsfo;
  moveXX, moveYY: integer;

implementation

uses
  settings_mfm,
  main,
  cdrom,
  editor,
  inifiles,
  process,
  classes,
  xelutils;

function FileToString(FileName: string): string;
var
  hFile: integer; nSize: integer;
begin
  hFile := FileOpen(FileName, fmOpenRead);
  try
    nSize := FileSeek(hFile, 0, 2); FileSeek(hFile, 0, 0);
    setlength(Result, nSize);
    FileRead(hFile, Result[1], nSize);
  finally
    FileClose(hFile);
  end;
end;

procedure tsetsfo.on_resize(const sender: TObject);
begin
  width := gui_getpointerpos.x - moveXX;
  height := gui_getpointerpos.y - moveYY;
end;

procedure tsetsfo.on_move(const sender: TObject);
begin
  left := gui_getpointerpos.x - moveXX;
  top := gui_getpointerpos.y - moveYY;
end;

procedure tsetsfo.on_childmouseevens(const sender: twidget;
  var ainfo: mouseeventinfoty);
begin
  if (ainfo.eventkind = EK_BUTTONPRESS) then
  begin
    bringtofront;
    if (s_top = sender) or (sender.classname = 'ttabpage')
      or (sender.classname = 'tcustomtabbar1')
      or (sender.classname = 'tscrollbox') then
    begin
      moveXX := gui_getpointerpos.x - left;
      moveYY := gui_getpointerpos.y - top;
      timer_move.enabled := true;
    end;
    if (tdockhandle1 = sender) then
    begin
      moveXX := gui_getpointerpos.x - width;
      moveYY := gui_getpointerpos.y - height;
      timer_resize.enabled := true;
    end;
  end;
  if (ainfo.eventkind = EK_BUTTONRELEASE) then
  begin
    if timer_move.enabled then timer_move.enabled := false;
    if timer_resize.enabled then timer_resize.enabled := false;
  end;
end;

procedure tsetsfo.on_close(const sender: TObject);
begin
  close;
end;

procedure tsetsfo.ON_CLOSEFORM(const sender: TObject);
begin

  mainfo.on_showdubble(sender);
end;

procedure tsetsfo.on_choose_playlist_dir(const sender: TObject);
begin
  mainfo.on_loadplaylists_list(sender);
end;

procedure tsetsfo.on_set_screenshots_dirs(const sender: TObject);
begin

end;

procedure tsetsfo.on_setallowemblems_change(const sender: TObject);
begin
  mainfo.on_loadradiotv(mainfo.trichbutton20);
  mainfo.on_loadradiotv(mainfo.trichbutton21);
end;

procedure tsetsfo.on_set_video_on_desktop(const sender: TObject; var avalue: Boolean; var accept: Boolean);
begin
  mainfo.player.videotoanotherdesktop := avalue;
  mainfo.player.device_CastomDisplayID := se_videoID.value;
  mainfo.restart_play;
end;

procedure tsetsfo.on_choose_window_for_videooutput(const sender: TObject);
begin
{$IFDEF linux}
  if findfile('xwininfo', ['/bin', '/usr/bin', '/sbin', '/usr/sbin'])
    then term.execprog('xwininfo | grep "Window id:"')
  else mainfo.show_ask(setsfo, 'Utility xwininfo not found!', '', false, false, false);
{$ENDIF}
{$IFDEF windows}

  mainfo.show_ask(setsfo, 'Not maked', '', false, false, false);
{$ENDIF}
end;

procedure tsetsfo.on_recivetext(const sender: TObject; var atext: AnsiString; const errorinput: Boolean);
var
  s: string;
begin
  s := atext;
  if system.pos(':', s) > 0 then
  begin
    delete(s, 1, system.pos(':', s));
    delete(s, 1, system.pos(':', s));
    s := trim(s);
    se_videoID.value := copy(s, 1, system.pos(' ', s));
    mainfo.player.device_CastomDisplayID := se_videoID.value;
  end;
end;

procedure tsetsfo.on_setNextTrackAllow(const sender: TObject;
  var avalue: Boolean; var accept: Boolean);
begin
  mainfo.player.PlayNextTrackAllow := avalue;
end;

procedure tsetsfo.on_set_dubble_videowindow(const sender: TObject;
  var avalue: Boolean; var accept: Boolean);
begin
  if mainfo.b_library.tag = 1
    then mainfo.button_fastmotion_onexecute(sender)
  else
    with mainfo do
    begin
      case button_fastmotion.tag of
        0:
          begin
            s_left.width := s_left.width * 2;
            button_fastmotion.tag := 1;
          end;
        1:
          begin
            s_left.width := s_left.width div 2;
            button_fastmotion.tag := 0;
          end;
      end;
      s_left.height := s_middle.height - (s_left.width div 2);
      sb_left.left := (s_left.width - sb_left.width) div 2;
    end;
end;

procedure tsetsfo.on_set_mplayer(const sender: TObject; var avalue: msestring; var accept: Boolean);
var
  s: string;
begin
  s := avalue; {$IFDEF mswindows}if s > '' then if s[1] = '/' then delete(s, 1, 1); {$ENDIF}
  mainfo.player.mplayer := s;
end;

procedure tsetsfo.on_setcache(const sender: TObject; var avalue: msestring; var accept: Boolean);
begin
  mainfo.player.cache := avalue;
end;

procedure tsetsfo.on_setaudioOutput(const sender: TObject; var avalue: msestring; var accept: Boolean);
begin
  mainfo.player.audiooutput := avalue;
  mainfo.player2.audiooutput := avalue;
end;

procedure tsetsfo.on_aval_audio_only(const sender: TObject; var avalue: Boolean;
  var accept: Boolean);
var
  i: integer;
begin

  begin
    edit_audiooutput.dropdown.cols.clear;
    edit_videooutput.dropdown.cols.clear;
    if not avalue then
    begin
      if mainfo.player <> nil then if mainfo.player.audiooutputlist <> nil then
          if mainfo.player.audiooutputlist.count > 0 then
            for i := 0 to mainfo.player.audiooutputlist.count - 1 do
              edit_audiooutput.dropdown.cols.addrow(msestring(mainfo.player.audiooutputlist[i]));
      if mainfo.player <> nil then if mainfo.player.videooutputlist <> nil then
          if mainfo.player.videooutputlist.count > 0 then
            for i := 0 to mainfo.player.videooutputlist.count - 1 do
              edit_videooutput.dropdown.cols.addrow(msestring(mainfo.player.videooutputlist[i]))
    end
    else
    begin
      if mainfo.player <> nil then if mainfo.player.aval_audiooutputlist <> nil then
          if mainfo.player.aval_audiooutputlist.count > 0 then
            for i := 0 to mainfo.player.aval_audiooutputlist.count - 1 do
              edit_audiooutput.dropdown.cols.addrow(msestring(mainfo.player.aval_audiooutputlist[i]));
      if mainfo.player <> nil then if mainfo.player.aval_videooutputlist <> nil then
          if mainfo.player.aval_videooutputlist.count > 0 then
            for i := 0 to mainfo.player.aval_videooutputlist.count - 1 do
              edit_videooutput.dropdown.cols.addrow(msestring(mainfo.player.aval_videooutputlist[i]));
    end;
  end;
end;

procedure tsetsfo.on_setvideooutput(const sender: TObject;
  var avalue: msestring; var accept: Boolean);
begin
  mainfo.player.videooutput := avalue;
  mainfo.player2.videooutput := 'null';
end;

procedure tsetsfo.on_set_webcampreload(const sender: TObject);
begin
  mainfo.player.preload_webcam := se_webcam_preload.value;
end;

procedure tsetsfo.on_set_additional_videoparam(const sender: TObject);
begin
  mainfo.player.additional_videoparams := se_additional_video_params.value;
end;

procedure tsetsfo.on_set_additional_params(const sender: TObject);
begin
  mainfo.player.aditionalparams := se_additional_params.value;
end;

procedure tsetsfo.on_update_devices(const sender: TObject);
var
  fs: tsearchrec;
  Drives: array[1..33] of string;
  I, Count: integer;
  s: string;
begin
  edit_webcam.dropdown.cols.clear;
  mainfo.ddd_webcam.dropdown.cols.clear;
  edit_tvtuner.dropdown.cols.clear;
  edit_cdrom.dropdown.cols.clear;

  mainfo.ddd_audioCDdevice2.dropdown.cols.clear;
  if findfirst('/dev/video*', faanyfile, fs) = 0 then
    repeat
      edit_webcam.dropdown.cols.addrow(msestring(fs.name));
      mainfo.ddd_webcam.dropdown.cols.addrow(msestring(fs.name));
      edit_tvtuner.dropdown.cols.addrow(msestring(fs.name));
    until findnext(fs) <> 0;
  try
    Count := GetCDRomDevices(Drives);
    if count > 10 then count := 10;
    for I := 1 to count do
    begin
      s := drives[i];
      if system.pos('/dev/', s) = 1 then delete(s, 1, 5);
      edit_cdrom.dropdown.cols.addrow(msestring(s));
      mainfo.ddd_audioCDdevice2.dropdown.cols.addrow(msestring(s));
      mainfo.ddd_audioCDdevice.dropdown.cols.addrow(msestring(s));
    end;
  except
    on E: exception do
    begin
    end;
  end;

end;

procedure tsetsfo.on_setwebcameradevice(const sender: TObject; var avalue: msestring; var accept: Boolean);
begin
{$IFDEF linux}
  mainfo.ddd_webcam.value := avalue;
  mainfo.player.device_webcamera := avalue;
{$ENDIF}
end;

procedure tsetsfo.on_settvtunerdevice(const sender: TObject; var avalue: msestring; var accept: Boolean);
begin
{$IFDEF linux}
  mainfo.player.device_tvtuner := avalue;
{$ENDIF}
end;

procedure tsetsfo.on_setaudiocddevice(const sender: TObject; var avalue: msestring; var accept: Boolean);
begin
{$IFDEF linux}
  mainfo.player.device_cdrom := avalue;
  mainfo.ddd_audioCDdevice.value := avalue;
{$ENDIF}
end;

procedure tsetsfo.on_setwriteouttoconsole(const sender: TObject;
  var avalue: Boolean; var accept: Boolean);
begin
  mainfo.player.debug_writeouttoconsole := avalue;
end;

procedure tsetsfo.on_addtodebugmplayeroutput(const sender: TObject;
  var avalue: Boolean; var accept: Boolean);
begin
  mainfo.player.debug_AddMplayerOutputToDebug := avalue;
end;

procedure tsetsfo.button_send_onexecute(const sender: TObject);
begin
  mainfo.player.SendCommandToMPlayer(edit_command.value);
end;

procedure tsetsfo.on_clear_messages(const sender: TObject);
begin
  sg_debug.clear;
end;

procedure tsetsfo.on_defocus(const sender: TObject);
begin

end;

procedure tsetsfo.on_set_use_fifo(const sender: TObject; var avalue: Boolean; var accept: Boolean);
begin
  mainfo.player.usefifo := avalue;
end;

procedure tsetsfo.on_setindexing(const sender: TObject; var avalue: Boolean;
  var accept: Boolean);
begin
  mainfo.player.indexing := avalue;
end;

procedure tsetsfo.be_show_process_debug(const sender: TObject;
  var avalue: Boolean; var accept: Boolean);
begin
  mainfo.player.debug_showplaying := avalue;
end;

procedure tsetsfo.on_changeactivetab(const sender: TObject);
begin

end;

procedure tsetsfo.on_load_sheme(const sender: TObject);
begin
  editshemefo.sf.readstat(shemepath + edit_scheme.value + '.xelsheme');
  editshemefo.on_apply(sender);
end;

procedure tsetsfo.on_edit_sheme(const sender: TObject);
begin
  editshemefo.bringtofront;
  editshemefo.visible := not editshemefo.visible;
  editshemefo.se_shemename.value := edit_scheme.value;
end;

procedure tsetsfo.on_choose_sheme(const sender: TObject; var avalue: msestring; var accept: Boolean);
begin
  editshemefo.sf.readstat(shemepath + avalue + '.xelsheme');
  editshemefo.se_shemename.value := avalue;
end;

procedure tsetsfo.on_scroll(const sender: twidget; var ainfo: naviginfoty);
begin

  sender.invalidatewidget;
  sender.update;
  sender.show;
end;

procedure tsetsfo.on_set_useonecopy(const sender: TObject; var avalue: Boolean;
  var accept: Boolean);
var
  FI: TINIFILE;
  p: tprocess;
  sl: tstringlist;
  s, id: string;
  i: integer;
begin
  fi := tinifile.create(main.conffile);
  fi.writebool('xelplayer', 'useonecopy', avalue);
  fi.free;
  if avalue then
  begin
{$IFDEF linux}
    sl := tstringlist.create;

    id := inttostr(application.procid);
    p := tprocess.create(nil);
    p.Options := p.Options + [poWaitOnExit, poUsePipes];
    p.commandline := 'ps -C xelplayer';
    p.execute;
    sl.loadfromstream(p.Output);

    if sl.count > 1 then
      for i := 1 to sl.count - 1 do
      begin
        s := trim(sl[i]);
        s := trim(copy(s, 1, system.pos(' ', s)));
        if s = id then continue;
        p.commandline := 'kill -9 ' + s;
        p.execute;
      end;

    p.commandline := 'ps -C mplayer';
    p.execute;
    sl.loadfromstream(p.Output);
    id := inttostr(mainfo.player.processid);
    if sl.count > 1 then
      for i := 1 to sl.count - 1 do
      begin
        s := trim(sl[i]);
        s := trim(copy(s, 1, system.pos(' ', s)));
        if s = id then continue;
        p.commandline := 'kill -9 ' + s;
        p.execute;
      end;

    sl.free;
    p.free;
{$ENDIF}
  end

end;

procedure tsetsfo.on_set_screenshotdir(const sender: TObject; var avalue: msestring; var accept: Boolean);
begin

end;

procedure tsetsfo.on_set_showonlymediafiles(const sender: TObject;
  var avalue: Boolean; var accept: Boolean);
begin
  if avalue then
    if not fileexists(setsfo.edit_magic.value) then
    begin
      mainfo.show_ask(mainfo, 'magic file not found! Set path currectly.' + #10 +
        '(Settings > XelPlayer > Path to magic.mgc)',
        '', false, false, false);
      mainfo.wg_fl.visible := false;
      mainfo.flv.visible := true;
      avalue := false;
      exit;
    end;

  filemagic := setsfo.edit_magic.value;

  mainfo.wg_fl.visible := avalue;
  mainfo.flv.visible := not avalue;
  if avalue
    then mainfo.readlist(mainfo.dd_dir.value)
  else
  begin
    mainfo.flv.directory := mainfo.dd_dir.value;
    mainfo.flv.readlist;
  end;
end;

procedure tsetsfo.on_set_doublebuf(const sender: TObject; var avalue: Boolean;
  var accept: Boolean);
begin
  mainfo.player.doublebuf := avalue;
end;

procedure tsetsfo.on_set_magicfile_path(const sender: TObject;
  var avalue: msestring; var accept: Boolean);
begin
  if not fileexists(avalue) then
    mainfo.show_ask(setsfo, 'magic file not found! Set path currectly.' + #10 +
      '(Settings > XelPlayer > Path to magic.mgc)',
      '', false, false, false);
  main.filemagic := avalue;
end;

procedure tsetsfo.on_set_lang(const sender: TObject);
var
  nummenu, nummenu2, nummenu3, nummenu4: integer;
  obmenu: tmenuitem;

  function prepare_message(st: string): string;
  var
    I: integer;
    ss: string;
  begin
    ST := TRIM(ST);
    if length(st) = 0 then
    begin
      result := '[MESSAGE_NOT_FOUND]';
      exit;
    end;
    for i := length(st) downto 1 do
    begin
      ss := copy(st, i, 3);

      if ss = '#10' then
      begin
        delete(st, i, 3);
        insert(#10, st, i);
      end;
    end;
    result := st;
  end;

  function HasProperty(Obj: TObject; PropName: string; PropKinds: TTypeKinds = []): boolean;
  begin
    Result := (GetPropInfo(Obj, PropName, PropKinds) <> nil);
  end;

  function prepare(s: string): string;
  var
    i, y: integer;
    ss: string;
  begin
    if length(s) = 0 then
    begin
      result := '';
      exit;
    end;

    y := length(s);
    for i := y downto 1 do
    begin
      ss := copy(s, i, 3);
      if (ss = '#13') or (ss = '#10') then
      begin
        delete(s, i, 3);
        if s[i] = '''' then delete(s, i, 1);
        insert(#10, s, i);
        if s[i - 1] = '''' then delete(s, i - 1, 1);
      end;
    end;
    result := s;
  end;

  procedure setcaption(form, component, typ, prop, caption: string; var num: integer);
  var
    formobj: twidget;
    ob: tobject;
    c: integer;
  begin
    if application.findwidget(form, formobj) then
    begin
      with formobj do
      begin
        if findcomponent(component) = nil then
        begin
{$IFDEF linux}
          writeln('component "' + component + '" on form "' + form + '" not found');
{$ENDIF}
          exit;
        end
          ;
      end;

      if system.pos('frame.', prop) = 1
        then with formobj do
        begin
          begin
            try
              (findcomponent(component) as twidget).createframe;
            except
            end;
            ob := GetObjectProp(findcomponent(component), 'frame');
            if HasProperty(ob, 'caption') then
              SetStrProp(ob, 'caption', prepare(caption));
          end;
        end
      else
        if system.pos('caption', prop) = 1 then
        begin
          try
            begin
              if system.pos('grid', typ) > 0
                then with formobj do
                begin
                  if findcomponent(component) as tcustomgrid <> nil then
                    (findcomponent(component) as tcustomgrid).fixrows[-1].captions[num].caption := prepare(caption);
                end;

              if typ = 'tpopupmenu'
                then with formobj do
                begin

                  if findcomponent(component) as tmenu <> nil then
                  begin

                    if (nummenu = -1) and (nummenu2 = -1)
                      then
                    begin
                      nummenu := (findcomponent(component) as tmenu).menu.items[num].count;
                      nummenu2 := nummenu;
                      nummenu3 := -1;
                      nummenu4 := -1;
                    end;

                    if nummenu > 0
                      then
                    begin

                      if (nummenu3 = -1) and (nummenu4 = -1)
                        then
                      begin
                        nummenu3 := (findcomponent(component) as tmenu).menu.items[num].items[nummenu2 - nummenu].count;
                        nummenu4 := nummenu3;
                      end;

                      if nummenu3 > 0
                        then
                      begin
                        obmenu := (findcomponent(component) as tmenu).menu.items[num].items[nummenu2 - nummenu].items[nummenu4 - nummenu3];
                        dec(num);
                        dec(nummenu3);
                        if nummenu3 = 0 then nummenu3 := -1;
                      end

                      else
                      begin
                        obmenu := (findcomponent(component) as tmenu).menu.items[num].items[nummenu2 - nummenu];
                        dec(num);
                        dec(nummenu);
                        if nummenu = 0 then nummenu := -1;
                        nummenu3 := -1;
                        nummenu4 := -1;
                      end;

                    end
                    else
                    begin
                      obmenu := (findcomponent(component) as tmenu).menu.items[num];
                      nummenu := -1;
                      nummenu2 := -1;
                    end;

                    if caption = '<separator>'
                      then obmenu.options := [mao_separator, mao_shortcutcaption]
                    else obmenu.caption := prepare(caption);

                  end;

                end

              else with formobj do
                begin
                  if findcomponent(component) <> nil then
                    if HasProperty(tobject(findcomponent(component)), 'caption') then
                      SetStrProp(tobject(findcomponent(component)), 'caption', prepare(caption));
                end;

            end;
          except
{$IFDEF linux}
            writeln('Component not found ' + component);
{$ENDIF}
          end;
        end
        else
          if system.pos('hint', prop) = 1 then
          begin
            try
              with formobj do
              begin
                if findcomponent(component) <> nil then
                  if HasProperty(tobject(findcomponent(component)), 'hint') then
                    SetStrProp(tobject(findcomponent(component)), 'hint', prepare(caption));
              end;
            except
{$IFDEF linux}
              writeln('Component not found ' + component);
{$ENDIF}
            end;
          end;
    end;
  end;

var
  i: integer;
  sl: tstringlist;
  s: string;
  c: integer;
  form__: string;
  component__: string;
  caption__: string;
  property__: string;
  typ__: string;
  fi: tinifile;
begin
  i := 0;
  s := '';
  c := -1;
  form__ := '';
  typ__ := '';
  component__ := '';
  if edit_language.value = '' then exit;

  if not fileexists(main.langspath + edit_language.value) then
  begin
    showmessage('Can`t find translate file.' + #10 + main.langspath + edit_language.value);
    exit;
  end;

  sl := tstringlist.create;

  sl.text := filetostring(main.langspath + edit_language.value);

  if sl.count > 0 then
    for i := 0 to sl.count - 1 do
    begin
      s := trim(sl[i]);
      if s = 'MESSAGES' then break;
      if (system.pos('#', s) = 1) or (length(s) = 0) then continue;

      if (system.pos('caption', s) = 1) or (system.pos('frame.caption', s) = 1)
        or (system.pos('hint', s) = 1)

      then
      begin
        property__ := copy(s, 1, system.pos('=', s) - 1);
        delete(s, 1, system.pos('=', s));
        caption__ := s;
        inc(c);

        setcaption(form__, component__, typ__, property__, caption__, c);

      end
      else
      begin
        form__ := copy(s, 1, system.pos(' ', s) - 1);
        delete(s, 1, system.pos(' ', s));
        component__ := trim(copy(s, 1, system.pos(':', s) - 1));
        delete(s, 1, system.pos(':', s));
        typ__ := trim(s);
        c := -1;
        nummenu := -1;
        nummenu2 := -1;
      end;
    end;

  sl.free;

end;

procedure tsetsfo.on_copymessages(const sender: TObject);
var
  r: integer;
  s: string;
begin
  edit_.text := '';
  s := '';
  for r := 0 to sg_debug.rowhigh do
  begin
    if sender = trichbutton9
      then s := s + #10 + sg_debug[0][R]
    else
      if sg_debug.datacols[0].selected[r] then
        s := s + #10 + sg_debug[0][R];
  end;
  edit_.text := trim(s);
  edit_.editor.SELECTALL;
  edit_.editor.copytoclipboard;
end;

procedure tsetsfo.on_load_icons(const sender: TObject);
var
  path: string;

  procedure add_(iconname: string);
  begin
    im.bitmap.clear;
    if fileexists(iconname) then
    begin
      im.bitmap.loadfromfile(iconname);
      mainfo.im_buttons.addimage(im.bitmap);
    end
    else
    begin
      mainfo.im_buttons.addimage(im2.bitmap);
{$IFDEF linux}
      writeln(iconname + ' not found');
{$ENDIF}
    end;
  end;

begin
  mainfo.im_buttons.clear;
  path := dirtowin(main.shemepath + 'icons/' + edit_icons.value);
  add_(path + 'prev.png');
  add_(path + 'play.png');
  add_(path + 'stop.png');
  add_(path + 'next.png');
  add_(path + 'pause.png');
  add_(path + 'style.png');
  add_(path + 'fullscreen.png');
  add_(path + 'fullscreen2.png');
  add_(path + 'library.png');
  add_(path + 'playlist.png');
  add_(path + 'vsettings.png');
  add_(path + 'audio.png');
  add_(path + 'audio1.png');
  add_(path + 'audio2.png');
  add_(path + 'audio3.png');
  add_(path + 'audio4.png');
  add_(path + 'screenshot.png');
  add_(path + 'home.png');
  add_(path + 'up.png');
  add_(path + 'update.png');
  add_(path + 'rename.png');
  add_(path + 'delete.png');
  add_(path + 'clear.png');
  add_(path + 'add.png');
  add_(path + 'info.png');
  add_(path + 'playdown.png');
  add_(path + 'playup.png');
  add_(path + 'repeat.png');
  add_(path + 'repeatno.png');
  add_(path + 'moveup.png');
  add_(path + 'movedown.png');
  add_(path + 'ok.png');
  add_(path + 'save.png');
  add_(path + 'filesadd.png');
  add_(path + 'filesopen.png');
  add_(path + 'top_close.png');
  add_(path + 'top_maximize.png');
  add_(path + 'top_minimize.png');
  add_(path + 'settings.png');
end;

end.
* * * (player)VOLUME: 50
* * * (player)CHECK PLAYER: mplayer found: / usr / bin / mplayer

* * * (player)VOLUME: 50
* * * (player)CHECK PLAYER: mplayer found: / usr / bin / mplayer
* * * (player)VOLUME: 50
