
unit fastmotion;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  mseforms;

type
  tdubblefo = class(tmseform)
  end;

var
  dubblefo: tdubblefo;

implementation

uses
  fastmotion_mfm;

end.
