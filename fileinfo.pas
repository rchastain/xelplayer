
unit fileinfo;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  mseforms,
  msesplitter,
  msesimplewidgets,
  msewidgets,
  mseedit,
  msestrings,
  msedataedits,
  mseifiglob,
  msetypes,
  sysutils,
  msegraphedits,
  msebitmap,
  msedatanodes,
  msefiledialog,
  msegrids,
  mselistbrowser,
  msesys,
  functions,
  mp3file;

type
  tfileinfofo = class(tmseform)
    s_head: tspacer;
    tlabel1: tlabel;
    se_artist: tstringedit;
    se_title: tstringedit;
    se_track: tstringedit;
    se_year: tstringedit;
    se_comment: tstringedit;
    se_genre: tstringedit;
    se_playtime: tstringedit;
    se_playlength: tstringedit;
    se_bitrate: tstringedit;
    se_samplerate: tstringedit;
    se_album: tstringedit;
    SE_FILE: tstringedit;
    s_buttons_audio: tspacer;
    tstockglyphbutton3: tstockglyphbutton;
    tstockglyphbutton2: tstockglyphbutton;
    tstockglyphbutton14: tstockglyphbutton;
    dd_sheme: tdropdownlistedit;
    tstockglyphbutton4: tstockglyphbutton;
    se_artistconst: tstringedit;
    procedure on_prev(const sender: TObject);
    procedure on_next(const sender: TObject);
    procedure on_show(const sender: TObject);
    procedure on_save(const sender: TObject);
    procedure on_create(const sender: TObject);
    procedure on_generate(const sender: TObject);
  end;

var
  fileinfofo: tfileinfofo;
  r: integer;

implementation

uses
  fileinfo_mfm,
  main;

procedure tfileinfofo.on_prev(const sender: TObject);
var
  i: integer;
  s: string;
begin
  if r > 0 then dec(r) else exit;

  s := mainfo.se_url[r];
{$IFDEF windows}
  for i := 1 to length(s) do if s[i] = '/' then s[i] := '\';
  if s[1] = '/' then delete(s, 1, 1);
{$ENDIF}
  SE_FILE.value := s;
  main.mp3f := TMP3File.create;
  main.mp3f.ReadTag(s);
  se_artist.value := main.mp3f.Artist;
  se_Album.value := main.mp3f.Album;
  se_Title.value := main.mp3f.Title;
  se_Track.value := main.mp3f.Track;
  se_Year.value := main.mp3f.Year;
  se_Comment.value := main.mp3f.Comment;
  se_Genre.value := inttostr(main.mp3f.GenreID);
  se_Playtime.value := main.mp3f.Playtime;
  se_Playlength.value := inttostr(main.mp3f.Playlength);
  se_Bitrate.value := inttostr(main.mp3f.Bitrate);
  se_Samplerate.value := inttostr(main.mp3f.Samplerate);
  main.mp3f.free;
end;

procedure tfileinfofo.on_next(const sender: TObject);
var
  i: integer;
  s: string;
begin
  if r < mainfo.wg_playlist.rowhigh then inc(r) else exit;

  s := mainfo.se_url[r];
{$IFDEF windows}
  for i := 1 to length(s) do if s[i] = '/' then s[i] := '\';
  if s[1] = '/' then delete(s, 1, 1);
{$ENDIF}
  SE_FILE.value := s;
  main.mp3f := TMP3File.create;
  main.mp3f.ReadTag(s);
  se_artist.value := main.mp3f.Artist;
  se_Album.value := main.mp3f.Album;
  se_Title.value := main.mp3f.Title;
  se_Track.value := main.mp3f.Track;
  se_Year.value := main.mp3f.Year;
  se_Comment.value := main.mp3f.Comment;
  se_Genre.value := inttostr(main.mp3f.GenreID);
  se_Playtime.value := main.mp3f.Playtime;
  se_Playlength.value := inttostr(main.mp3f.Playlength);
  se_Bitrate.value := inttostr(main.mp3f.Bitrate);
  se_Samplerate.value := inttostr(main.mp3f.Samplerate);
  main.mp3f.free;
end;

procedure tfileinfofo.on_show(const sender: TObject);
begin
  r := mainfo.wg_playlist.row;
end;

procedure tfileinfofo.on_save(const sender: TObject);
var
  i: integer;
begin
  if r = mainfo.player.tracknum
    then
  begin
    mainfo.player.stop;
    i := round(mainfo.sl_position.value * 100);
  end;
  main.mp3f := TMP3File.create;
  main.mp3f.Artist := se_artist.value;
  main.mp3f.Album := se_Album.value;
  main.mp3f.Title := se_Title.value;
  main.mp3f.Track := se_Track.value;
  main.mp3f.Year := se_Year.value;
  main.mp3f.Comment := se_Comment.value;
  main.mp3f.WriteTag(mainfo.se_url[r]);
  main.mp3f.free;
  if r = mainfo.player.tracknum
    then
  begin
    mainfo.player.play;
    mainfo.player.setpositionpersent(i);
  end;
end;

procedure tfileinfofo.on_create(const sender: TObject);
var
  s: msestring;
begin
  s := 'Name'; dd_sheme.dropdown.cols.addrow(s);
  s := '<insert artist> Name'; dd_sheme.dropdown.cols.addrow(s);
  s := 'artist - title'; dd_sheme.dropdown.cols.addrow(s);
  s := 'artist - title - album'; dd_sheme.dropdown.cols.addrow(s);
  s := 'artist - album - title'; dd_sheme.dropdown.cols.addrow(s);
  s := 'artist_-_title'; dd_sheme.dropdown.cols.addrow(s);
  s := '<insert artist> track number - title'; dd_sheme.dropdown.cols.addrow(s);
  s := '<insert artist> track number <space> title'; dd_sheme.dropdown.cols.addrow(s);
end;

procedure tfileinfofo.on_generate(const sender: TObject);
var
  s: string;
  ext: string;
begin
  s := extractfilename(SE_FILE.value);
  ext := extractfileext(s);
  delete(s, pos_(ext, s), length(ext));

  case dd_sheme.dropdown.itemindex of
    0:
      begin
        se_artist.value := '';
        se_title.value := trim(s);
      end;
    1:
      begin
        se_artist.value := se_artistconst.value;
        se_title.value := trim(s);
      end;
    2:
      begin
        se_artist.value := trim(copy(s, 1, pos_('-', s) - 1));
        delete(s, 1, pos_('-', s));
        se_title.value := trim(s);
      end;
    3:
      begin
        se_artist.value := trim(copy(s, 1, pos_('-', s) - 1));
        delete(s, 1, pos_('-', s));
        se_title.value := trim(copy(s, 1, pos_('-', s) - 1));
        delete(s, 1, pos_('-', s));
        se_album.value := trim(s);
      end;
    4:
      begin
        se_artist.value := trim(copy(s, 1, pos_('-', s) - 1));
        delete(s, 1, pos_('-', s));
        se_album.value := trim(copy(s, 1, pos_('-', s) - 1));
        delete(s, 1, pos_('-', s));
        se_title.value := trim(s);
      end;
    5:
      begin
        se_artist.value := trim(copy(s, 1, pos_('_-_', s) - 1));
        delete(s, 1, pos_('_-_', s) + 2);
        se_title.value := trim(s);
      end;
    6:
      begin
        se_artist.value := se_artistconst.value;
        se_track.value := trim(copy(s, 1, pos_('-', s) - 1));
        delete(s, 1, pos_('-', s));
        se_title.value := trim(s);
      end;
    7:
      begin
        se_artist.value := se_artistconst.value;
        se_track.value := trim(copy(s, 1, pos_(' ', s) - 1));
        delete(s, 1, pos_(' ', s));
        se_title.value := trim(s);
      end;
  end;
end;

end.
