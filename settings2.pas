
unit settings2;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,

  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  mseforms,

  msesimplewidgets,

  msetimer,

  sysutils,
  msegraphedits,

  msetypes;

type
  tset2fo = class(tmseform)
    timer_move: ttimer;
    trichbutton1: trichbutton;
    procedure on_move(const sender: TObject);
    procedure on_childmouseevens(const sender: twidget; var ainfo: mouseeventinfoty);
    procedure on_close(const sender: TObject);
  end;

var
  set2fo: tset2fo;
  moveXX, moveYY: integer;

implementation

uses
  settings2_mfm,
  main;

procedure tset2fo.on_move(const sender: TObject);
begin
  left := gui_getpointerpos.x - moveXX;
  top := gui_getpointerpos.y - moveYY;
end;

procedure tset2fo.on_childmouseevens(const sender: twidget; var ainfo: mouseeventinfoty);
var
  s: string;
  r: real;
begin
  if (ainfo.eventkind = EK_BUTTONPRESS) then
  begin
    bringtofront;
    if (sender.classname = 'tslider') then
      with mainfo do
      begin
        s := (sender as tslider).name;
        delete(s, 1, 2);
        s := 'pb' + s;
        r := (gui_getpointerpos.x - set2fo.left - (sender as tslider).left) / (sender as tslider).width;
        (sender as tslider).value := r;
      end;
    if (sender.classname = 'tcustomtabbar1') or (sender.classname = 'tscrollbox') then
    begin
      moveXX := gui_getpointerpos.x - left;
      moveYY := gui_getpointerpos.y - top;
      timer_move.enabled := true;
    end;
  end;
  if (ainfo.eventkind = EK_BUTTONRELEASE) then
  begin
    if timer_move.enabled then timer_move.enabled := false;
  end;
end;

procedure tset2fo.on_close(const sender: TObject);
begin
  close;
end;

end.
