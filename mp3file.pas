
unit mp3file;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}
interface

uses Classes,
  SysUtils,
  msegui,
  msewidgets;

const
  ID3Genre: array[0..147] of string[32] = ('', 'Classic Rock', 'Country', 'Dance',
    'Disco', 'Funk', 'Grunge',
    'Hip-Hop', 'Jazz', 'Metal', 'New Age', 'Oldies', 'Other', 'Pop', 'R&B',
    'Rap', 'Reggae', 'Rock', 'Techno', 'Industrial', 'Alternative', 'Ska',
    'Death Metal', 'Pranks', 'Soundtrack', 'Euro-Techno', 'Ambient', 'Trip-Hop',
    'Vocal', 'Jazz&Funk', 'Fusion', 'Trance', 'Classical', 'Instrumental',
    'Acid', 'House', 'Game', 'Sound Clip', 'Gospel', 'Noise', 'Alternative Rock',
    'Bass', 'Soul', 'Punk', 'Space', 'Meditative', 'Instrumental Pop',
    'Instrumental Rock', 'Ethnic', 'Gothic', 'Darkwave', 'Techno-Industrial',
    'Electronic', 'Pop-Folk', 'Eurodance', 'Dream', 'Southern Rock', 'Comedy',
    'Cult', 'Gangsta', 'Top 40', 'Christian Rap', 'Pop/Funk', 'Jungle',
    'Native US', 'Cabaret', 'New Wave', 'Psychedelic', 'Rave', 'Showtunes',
    'Trailer', 'Lo-Fi', 'Tribal', 'Acid Punk', 'Acid Jazz', 'Polka', 'Retro',
    'Musical', 'Rock & Roll', 'Hard Rock', 'Folk', 'Folk-Rock', 'National Folk',
    'Swing', 'Fast Fusion', 'Bebob', 'Latin', 'Revival', 'Celtic', 'Bluegrass',
    'Avantgarde', 'Gothic Rock', 'Progressive Rock', 'Psychedelic Rock',
    'Symphonic Rock', 'Slow Rock', 'Big Band', 'Chorus', 'Easy Listening',
    'Acoustic', 'Humour', 'Speech', 'Chanson', 'Opera', 'Chamber Music',
    'Sonata', 'Symphony', 'Booty Bass', 'Primus', 'Porn Groove', 'Satire',
    'Slow Jam', 'Club', 'Tango', 'Samba', 'Folklore', 'Ballad', 'Power Ballad',
    'Rhythmic Soul', 'Freestyle', 'Duet', 'Punk Rock', 'Drum Solo', 'A capella',
    'Euro-House', 'Dance Hall', 'Goa', 'Drum`n`Bass', 'Club-House', 'Hardcore',
    'Terror', 'Indie', 'BritPop', 'Negerpunk', 'Polsk Punk', 'Beat',
    'Christian Gangsta', 'Heavy Metal', 'Black Metal', 'Crossover',
    'Contemporary Christian', 'Christian Rock', 'Merengue', 'Salsa',
    'Thrash Metal', 'Anime', 'JPop', 'SynthPop');

type

  TMP3File = class
  private
    FArtist, FTitle, FAlbum, FTrack, FYear, FComment, FPlaytime: string;
    FSamplerate, FBitrate, FPlaylength: integer;
    FGenreID: byte;
    FGenre: string;
    FFileName: string;
    procedure ReadHeader;
  public
    constructor create;
    function ReadTag(Filename: string): boolean;
    function WriteTag(Filename: string): boolean;
    property Artist: string read FArtist write FArtist;
    property Album: string read FAlbum write FAlbum;
    property Title: string read FTitle write FTitle;
    property Track: string read FTrack write FTrack;
    property Year: string read FYear write FYear;
    property Comment: string read FComment write FComment;
    property GenreID: byte read FGenreID write FGenreID;
    property Genre: string read FGenre write FGenre;
    property Playtime: string read FPlaytime;

    property Playlength: integer read FPlaylength;
    property Bitrate: integer read FBitrate;
    property Samplerate: integer read FSamplerate;

  end;

implementation

uses functions;

const
  BitratesTable: array[0..15] of integer = (0, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192,
    224, 256, 320, 999);
  SampleratesTable: array[0..3] of integer = (44100, 48000, 32100, 0);

procedure TMP3File.ReadHeader;
var
  FileStream: TFileStream;
  buf: array[0..255] of byte;
  b: byte;
  i, z: integer;
  size: int64;
begin
  FileStream := TFileStream.Create(FFileName, fmOpenRead);

  FileStream.Seek(0, fsfrombeginning);
  FileStream.Read(buf, SizeOf(buf));
  size := FileStream.Size;

  i := 0;
  z := 1;
  repeat
    begin

      inc(i);
      if i = high(buf) - 2 then
      begin

        FileStream.Seek((i * z) - 8, fsFromBeginning);
        FileStream.Read(buf, high(buf));
        inc(z);
        i := 1;
      end;
    end;
  until ((buf[i] = $FF) and ((buf[i + 1] or $3) = $FB)) or (z > 256);

  FileStream.Free;

  if (buf[i] = $FF) and ((buf[i + 1] or $3) = $FB) then
  begin

    b := buf[i + 2] shr 4;

    if b > 15 then b := 0;
    FBitrate := BitratesTable[b];

    b := buf[i + 2] and $0F;

    b := b shr 2;
    if b > 3 then b := 3;
    FSamplerate := SampleratesTable[b];

    if FBitrate > 0 then
    begin
      FPlaylength := round(size / (bitrate * 125));

      FPlaytime := SecondsToFmtStr(FPlaylength);

    end;
  end
  else writeln(FFileName + ' -> no valid mpeg header found');
end;

constructor TMP3File.create;
begin

end;

function TMP3File.ReadTag(Filename: string): boolean;

var
  i, z, tagpos: integer;
  b: byte;

  buf: array[1..2048] of byte;
  artistv2, albumv2, titlev2, commentv2, yearv2, trackv2: string;
  bufstr: string;
  mp3filehandle: longint;
begin
  FFileName := Filename;
  ReadHeader;
  try
    mp3filehandle := fileopen(Filename, fmOpenRead);

    fileseek(mp3filehandle, 0, fsfrombeginning);
    fileread(mp3filehandle, buf, high(buf));
    bufstr := '';
    for i := 1 to high(buf) do
      if (buf[i] <> 0) and (buf[i] <> 10) then bufstr := bufstr + char(buf[i])
      else bufstr := bufstr + ' ';

    albumv2 := '';
    artistv2 := '';
    titlev2 := '';
    trackv2 := '';
    yearv2 := '';
    if pos('ID3', bufstr) <> 0 then
    begin
      i := pos('TPE1', bufstr);
      if i <> 0 then artistv2 := copy(bufstr, i + 11, buf[i + 7] - 1);
      i := pos('TP1', bufstr);
      if i <> 0 then artistv2 := copy(bufstr, i + 7, buf[i + 5] - 1);

      i := pos('TIT2', bufstr);
      if i <> 0 then titlev2 := copy(bufstr, i + 11, buf[i + 7] - 1);

      i := pos('TT2', bufstr);
      if i <> 0 then titlev2 := copy(bufstr, i + 7, buf[i + 5] - 1);
      i := pos('TRCK', bufstr);
      if i <> 0 then trackv2 := copy(bufstr, i + 11, buf[i + 7] - 1);

      i := pos('TRK', bufstr);
      if i <> 0 then trackv2 := copy(bufstr, i + 7, buf[i + 5] - 1);

      if length(trackv2) > 3 then trackv2 := '';

      i := pos('TAL', bufstr);
      if i <> 0 then albumv2 := copy(bufstr, i + 7, buf[i + 5] - 1);
      i := pos('TALB', bufstr);
      if i <> 0 then albumv2 := copy(bufstr, i + 11, buf[i + 7] - 1);

      i := pos('TYE', bufstr);
      if i <> 0 then yearv2 := copy(bufstr, i + 7, buf[i + 5] - 1);

      i := pos('TYER', bufstr);
      if i <> 0 then yearv2 := copy(bufstr, i + 11, buf[i + 7] - 1);
      artistv2 := (artistv2);
      titlev2 := (titlev2);
      albumv2 := (albumv2);
      yearv2 := (yearv2);
      trackv2 := (trackv2);
      if length(yearv2) > 5 then yearv2 := '';
    end;
  except WriteLn(Filename + ' -> exception while reading id3v2 tag... skipped!!');
  end;

  try
    fileseek(mp3filehandle, -128, fsfromend);
    fileread(mp3filehandle, buf, 128);
    bufstr := '';
    for i := 1 to 128 do bufstr := bufstr + char(buf[i]);

    for i := 1 to 128 do
    begin
      b := byte(bufstr[i]);
      if (b < 32) then bufstr[i] := #32;

    end;
    tagpos := pos('TAG', bufstr) + 3;
    if tagpos <> 3 then
    begin
      ftitle := (copy(bufstr, tagpos, 30));
      fartist := (copy(bufstr, tagpos + 30, 30));
      falbum := (copy(bufstr, tagpos + 60, 30));
      fyear := copy(bufstr, tagpos + 90, 4);

      FGenreID := buf[tagpos + 124];
      if FGenreID > high(ID3Genre) then FGenreID := 0;
      if buf[125] <> 0 then
        fcomment := (copy(bufstr, tagpos + 94, 30))
      else
      begin
        fcomment := (copy(bufstr, tagpos + 94, 28));
        if (buf[tagpos + 123]) <> 0 then ftrack := IntToStr(buf[tagpos + 123])
        else ftrack := '';
      end;
    end;
  except WriteLn(Filename + ' -> exception while reading id3v1 tag... skipped!!');
  end;
  if ((artistv2 <> '')) or (artist = '') then
    Fartist := TrimRight(artistv2);
  if ((titlev2 <> '')) or (title = '') then
    Ftitle := TrimRight(titlev2);
  if ((albumv2 <> '')) or (album = '') then
    Falbum := TrimRight(albumv2);
  if ((commentv2 <> '')) or (comment = '') then
    Fcomment := TrimRight(commentv2);
  if ((yearv2 <> '')) or (year = '') then
    Fyear := TrimRight(yearv2);
  if ((trackv2 <> '')) or (track = '') then
    ftrack := TrimRight(trackv2);

  Fartist := TrimRight(Fartist);
  Ftitle := TrimRight(Ftitle);
  Falbum := TrimRight(FAlbum);
  Fcomment := TrimRight(FComment);
  Fyear := TrimRight(FYear);
  fgenre := ID3Genre[fgenreID];
  fileclose(mp3filehandle);
end;

function TMP3File.WriteTag(Filename: string): boolean;
var
  buf: array[1..1024] of byte;
  bufstr, tmptag, tmps: string;
  i, z: integer;
  id3v1str: string[31];
  mp3filehandle: longint;
begin

  mp3filehandle := fileopen(Filename, fmOpenRead);
  fileseek(mp3filehandle, 0, fsfrombeginning);
  fileread(mp3filehandle, buf, high(buf));
  fileclose(mp3filehandle);
  for i := 1 to high(buf) do
    bufstr := bufstr + char(buf[i]);

  if (pos('ID3', bufstr) <> 0) or (length(artist) > 30) or (length(title) > 30) or (length(album) > 30)
    then
  begin
    if pos('ID3', bufstr) = 0 then
    begin
      bufstr := '';
      bufstr := 'ID3' + char($03) + char(0) + char(0) + char(0) + char(0) + char(0) + char(0);

      tmps := char(0) + char(0) + char(0) + char(2) + char(0) + char(0) + char(0) + ' ';
      bufstr := bufstr + 'TPE1' + tmps + 'TIT2' + tmps + 'TRCK' + tmps + 'TYER' + tmps + 'TALB' + tmps + char(0) +
        char(0);
      writeln('creating new ID3v2 tag!');
      writeln(bufstr);
      z := length(bufstr) - 1;
      for i := z to high(buf) do
        bufstr := bufstr + char(0);
    end;

    i := pos('TPE1', bufstr);
    if i <> 0 then
    begin
      tmptag := (artist);
      if length(tmptag) > 0 then
      begin
        delete(bufstr, i + 11, byte(bufstr[i + 7]) - 1);
        Insert(tmptag, bufstr, i + 11);
        bufstr[i + 7] := char(length(tmptag) + 1);
      end
      else delete(bufstr, i, byte(bufstr[i + 7]) + 10);
    end
    else
    begin
      tmptag := (artist);
      if length(tmptag) > 0 then
      begin
        tmps := char(0) + char(0) + char(0) + char(length(tmptag) + 1) + char(0) + char(0) + char(0);
        Insert('TPE1' + tmps + (tmptag), bufstr, pos('ID3', bufstr) + 10);
      end;
    end;

    i := pos('TP1', bufstr);
    if i <> 0 then
    begin
      tmptag := (artist);
      delete(bufstr, i, byte(bufstr[i + 5]) + 6);

    end;

    i := pos('TIT2', bufstr);
    if i <> 0 then
    begin
      tmptag := (title);
      if length(tmptag) > 0 then
      begin
        delete(bufstr, i + 11, byte(bufstr[i + 7]) - 1);
        Insert(tmptag, bufstr, i + 11);
        bufstr[i + 7] := char(length(tmptag) + 1);
      end
      else delete(bufstr, i, byte(bufstr[i + 7]) + 10);
    end
    else
    begin
      tmptag := UTF8toLatin1(title);
      if length(tmptag) > 0 then
      begin
        tmps := char(0) + char(0) + char(0) + char(length(tmptag) + 1) + char(0) + char(0) + char(0);
        Insert('TIT2' + tmps + tmptag, bufstr, pos('ID3', bufstr) + 10);
      end;
    end;

    i := pos('TRCK', bufstr);
    if i <> 0 then
    begin
      tmptag := (track);
      if length(tmptag) > 0 then
      begin
        delete(bufstr, i + 11, byte(bufstr[i + 7]) - 1);
        Insert(tmptag, bufstr, i + 11);
        bufstr[i + 7] := char(length(tmptag) + 1);
      end
      else delete(bufstr, i, byte(bufstr[i + 7]) + 10);
    end
    else
    begin
      tmptag := UTF8toLatin1(track);
      if length(tmptag) > 0 then
      begin
        tmps := char(0) + char(0) + char(0) + char(length(tmptag) + 1) + char(0) + char(0) + char(0);
        Insert('TRCK' + tmps + tmptag, bufstr, pos('ID3', bufstr) + 10);
      end;
    end;

    i := pos('TYER', bufstr);
    if i <> 0 then
    begin
      tmptag := (year);
      if length(tmptag) > 0 then
      begin
        delete(bufstr, i + 11, byte(bufstr[i + 7]) - 1);
        Insert(tmptag, bufstr, i + 11);
        bufstr[i + 7] := char(length(tmptag) + 1);
      end
      else delete(bufstr, i, byte(bufstr[i + 7]) + 10);
    end
    else
    begin
      tmptag := (year);
      if length(tmptag) > 0 then
      begin
        tmps := char(0) + char(0) + char(0) + char(length(tmptag) + 1) + char(0) + char(0) + char(0);
        Insert('TYER' + tmps + tmptag, bufstr, pos('ID3', bufstr) + 10);
      end;
    end;

    i := pos('TALB', bufstr);
    if i <> 0 then
    begin
      tmptag := (album);
      if length(tmptag) > 0 then
      begin
        delete(bufstr, i + 11, byte(bufstr[i + 7]) - 1);
        Insert(tmptag, bufstr, i + 11);
        bufstr[i + 7] := char(length(tmptag) + 1);
      end
      else delete(bufstr, i, byte(bufstr[i + 7]) + 10);
    end
    else
    begin
      tmptag := (album);
      if length(tmptag) > 0 then
      begin
        tmps := char(0) + char(0) + char(0) + char(length(tmptag) + 1) + char(0) + char(0) + char(0);
        Insert('TALB' + tmps + tmptag, bufstr, pos('ID3', bufstr) + 10);
      end;
    end;

    z := length(bufstr) - 1;
    for i := 1 to high(buf) do
      if (i < z) then buf[i] := byte(bufstr[i])
      else buf[i] := 0;
    mp3filehandle := fileopen(Filename, fmOpenWrite);
    if mp3filehandle <> -1 then
    begin
      fileseek(mp3filehandle, 0, fsfrombeginning);
      filewrite(mp3filehandle, buf, high(buf));
      fileclose(mp3filehandle);
    end
    else writeln('ERROR: cant write tag. file not found');
  end;

  writeln('#####ID3V1#######');
  for i := 1 to 128 do
    buf[i] := 0;
  buf[1] := 84;
  buf[2] := 65;
  buf[3] := 71;

  FillChar(id3v1str, SizeOf(id3v1str), #0);
  id3v1str := (title);
  for i := 4 to 33 do
    buf[i] := byte(id3v1str[i - 3]);

  FillChar(id3v1str, SizeOf(id3v1str), #0);
  id3v1str := (artist);
  for i := 34 to 63 do
    buf[i] := byte(id3v1str[i - 33]);

  FillChar(id3v1str, SizeOf(id3v1str), #0);
  id3v1str := (album);
  for i := 64 to 93 do
    buf[i] := byte(id3v1str[i - 63]);

  FillChar(id3v1str, SizeOf(id3v1str), #0);
  id3v1str := (year);
  for i := 94 to 97 do
    buf[i] := byte(id3v1str[i - 93]);

  FillChar(id3v1str, SizeOf(id3v1str), #0);
  id3v1str := (comment);
  for i := 98 to 127 do
    buf[i] := byte(id3v1str[i - 97]);

  if length(track) > 0 then
  begin
    buf[126] := 0;
    buf[127] := StrToInt(track);
  end;

  mp3filehandle := fileopen(Filename, fmOpenWrite);
  if mp3filehandle <> -1 then
  begin
    if FileGetAttr(Filename) = faReadOnly then writeln('file is read only');
    fileseek(mp3filehandle, -128, fsfromend);
    writeln(ftitle);
    writeln(fartist);
    filewrite(mp3filehandle, buf, 128);
    fileclose(mp3filehandle);
  end
  else writeln('ERROR: cant write tag. file not found');

end;

end.
