
unit chanelform;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  mseforms,
  msesplitter,
  msesimplewidgets,
  msewidgets,
  msetimer,
  msedataedits,
  mseedit,
  mseifiglob,
  msestrings,
  msetypes,
  msebitmap,
  msedatanodes,
  msefiledialog,
  msegrids,
  mselistbrowser,
  msesys,
  mseimage;

type
  tchanelfo = class(tmseform)
    tspacer1: tspacer;
    trichbutton1: trichbutton;
    timer_move: ttimer;
    se_name: tstringedit;
    se_url: tstringedit;
    fne_emblem: tfilenameedit;
    trichbutton2: trichbutton;
    trichbutton3: trichbutton;
    se_site: tstringedit;
    se_key: tstringedit;
    im_emblem: timage;
    procedure on_close(const sender: TObject);
    procedure on_timer_move(const sender: TObject);
    procedure on_onchildmouseevent(const sender: twidget;
      var ainfo: mouseeventinfoty);
    procedure on_set_chanel_emblem(const sender: TObject; var avalue: msestring;
      var accept: Boolean);
    procedure on_set_chanel_emblem2(const sender: tcustomdataedit;
      var atext: msestring; var accept: Boolean);
    procedure on_set_chanel_emblem3(const sender: TObject);
  end;

var
  chanelfo: tchanelfo;
  moveXX, moveYY: integer;

implementation

uses
  chanelform_mfm,
  mseformatpngread,
  mseformatjpgread,
  mseformatxpmread,
  mseformatbmpicoread,
  mseformatpnmread,
  mseformattgaread;

procedure tchanelfo.on_close(const sender: TObject);
begin
  close;
end;

procedure tchanelfo.on_timer_move(const sender: TObject);
begin
  left := gui_getpointerpos.x - moveXX;
  top := gui_getpointerpos.y - moveYY;
end;

procedure tchanelfo.on_onchildmouseevent(const sender: twidget; var ainfo: mouseeventinfoty);
begin
  if (ainfo.eventkind = EK_BUTTONPRESS) then
  begin
    bringtofront;
    if (chanelfo = sender) or (tspacer1 = sender) then
    begin
      moveXX := gui_getpointerpos.x - left;
      moveYY := gui_getpointerpos.y - top;
      timer_move.enabled := true;
    end;
  end;
  if (ainfo.eventkind = EK_BUTTONRELEASE) then
  begin
    if timer_move.enabled then timer_move.enabled := false;
  end;
end;

procedure tchanelfo.on_set_chanel_emblem(const sender: TObject; var avalue: msestring; var accept: Boolean);
begin
end;

procedure tchanelfo.on_set_chanel_emblem2(const sender: tcustomdataedit; var atext: msestring; var accept: Boolean);
begin
end;

procedure tchanelfo.on_set_chanel_emblem3(const sender: TObject);
begin
  try
    im_emblem.bitmap.loadfromfile(fne_emblem.value);
  except
  end;
end;

end.
