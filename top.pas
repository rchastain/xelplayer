
unit top;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  mseforms;

type
  ttopfo = class(tmseform)
  end;

var
  topfo: ttopfo;

implementation

uses
  top_mfm;

end.
