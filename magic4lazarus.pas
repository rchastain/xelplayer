
unit magic4lazarus;

{$MODE objfpc}{$H+}

interface

uses
  Classes,
  SysUtils;

type

  TMagic = class
  private
    FHANDLE: integer;
    FOnlyMimeEncoding: boolean;
    FOnlyMimeType: boolean;
    FOnlyMimeEncodig: boolean;
    FFileMagic: string;
    FFlags: integer;
    procedure SetOnlyMimeType(const AValue: boolean);
    procedure SetOnlyMimeEncoding(const AValue: boolean);
    procedure BeforeDetect;
  public
    property HANDLE: integer read FHANDLE;
    property OnlyMimeType: boolean read FOnlyMimeType write SetOnlyMimeType;
    property OnlyMimeEncoding: boolean read FOnlyMimeEncoding write SetOnlyMimeEncoding;
    property FileMagic: string read FFileMagic write FFileMagic;
    constructor Create();
    destructor Destroy; override;
    function FromFile(filename: string): string;
    function FromBuffer(var buffer; count: longint): string;
  end;

function DetectMimeFromFile(filename: string; filemagic: string): string;

function DetectMimeTypeFromFile(filename: string; filemagic: string): string;

function DetectMimeEncodingFromFile(filename: string; filemagic: string): string;

implementation

const
{$IFDEF WINDOWS}
  MAGIC_LIBRARY = 'magic1.dll';
{$ENDIF}
{$IFDEF LINUX}
  MAGIC_LIBRARY = 'libmagic.so';
{$ENDIF}
{$IFDEF DARWIN}
  MAGIC_LIBRARY = 'libmagic.dylib';
{$LINKLIB libmagic}
{$ENDIF}

  MAGIC_NONE = $000000;
{$EXTERNALSYM MAGIC_NONE}
  MAGIC_DEBUG = $000001;
{$EXTERNALSYM MAGIC_DEBUG}
  MAGIC_SYMLINK = $000002;
{$EXTERNALSYM MAGIC_SYMLINK}
  MAGIC_COMPRESS = $000004;
{$EXTERNALSYM MAGIC_COMPRESS}
  MAGIC_DEVICES = $000008;
{$EXTERNALSYM MAGIC_DEVICES}
  MAGIC_MIME_TYPE = $000010;
{$EXTERNALSYM MAGIC_MIME_TYPE}
  MAGIC_CONTINUE = $000020;
{$EXTERNALSYM MAGIC_CONTINUE}
  MAGIC_CHECK = $000040;
{$EXTERNALSYM MAGIC_CHECK}
  MAGIC_PRESERVE_ATIME = $000080;
{$EXTERNALSYM MAGIC_PRESERVE_ATIME}
  MAGIC_RAW = $000100;
{$EXTERNALSYM MAGIC_RAW}
  MAGIC_ERROR = $000200;
{$EXTERNALSYM MAGIC_ERROR}
  MAGIC_MIME_ENCODING = $000400;
{$EXTERNALSYM MAGIC_MIME_ENCODING}
  MAGIC_MIME = (MAGIC_MIME_TYPE or MAGIC_MIME_ENCODING);
{$EXTERNALSYM MAGIC_MIME}
  MAGIC_APPLE = $000800;
{$EXTERNALSYM MAGIC_APPLE}
  MAGIC_NO_CHECK_COMPRESS = $001000;
{$EXTERNALSYM MAGIC_NO_CHECK_COMPRESS}
  MAGIC_NO_CHECK_TAR = $002000;
{$EXTERNALSYM MAGIC_NO_CHECK_TAR}
  MAGIC_NO_CHECK_SOFT = $004000;
{$EXTERNALSYM MAGIC_NO_CHECK_SOFT}
  MAGIC_NO_CHECK_APPTYPE = $008000;
{$EXTERNALSYM MAGIC_NO_CHECK_APPTYPE}
  MAGIC_NO_CHECK_ELF = $010000;
{$EXTERNALSYM MAGIC_NO_CHECK_ELF}
  MAGIC_NO_CHECK_TEXT = $020000;
{$EXTERNALSYM MAGIC_NO_CHECK_TEXT}
  MAGIC_NO_CHECK_CDF = $040000;
{$EXTERNALSYM MAGIC_NO_CHECK_CDF}
  MAGIC_NO_CHECK_TOKENS = $100000;
{$EXTERNALSYM MAGIC_NO_CHECK_TOKENS}
  MAGIC_NO_CHECK_ENCODING = $200000;
{$EXTERNALSYM MAGIC_NO_CHECK_ENCODING}

  MAGIC_NO_CHECK_ASCII = MAGIC_NO_CHECK_TEXT;
{$EXTERNALSYM MAGIC_NO_CHECK_ASCII}

  MAGIC_NO_CHECK_FORTRAN = $000000;
{$EXTERNALSYM MAGIC_NO_CHECK_FORTRAN}
  MAGIC_NO_CHECK_TROFF = $000000;
{$EXTERNALSYM MAGIC_NO_CHECK_TROFF}

function magic_open(flags: integer): integer; cdecl; external MAGIC_LIBRARY;

procedure magic_close(cookie: integer); cdecl; external MAGIC_LIBRARY;

function magic_file(cookie: integer; filename: PChar): PChar; cdecl; external MAGIC_LIBRARY;

function magic_descriptor(cookie: integer): PChar; cdecl; external MAGIC_LIBRARY;

function magic_buffer(cookie: integer; var buffer; Count: longint): PChar; cdecl; external MAGIC_LIBRARY;

function magic_setflags(coockie: integer; flags: integer): integer; cdecl; external MAGIC_LIBRARY;

function magic_load(cookie: integer; magic_file: PChar): integer; cdecl; external MAGIC_LIBRARY;

function magic_compile(cookie: integer; magic_file: PChar): integer; cdecl; external MAGIC_LIBRARY;

function magic_errno(cookie: integer): integer; cdecl; external MAGIC_LIBRARY;

function DetectMimeFromFile(filename: string; filemagic: string): string;
var
  magic: TMagic;
begin
  magic := TMagic.Create();
  try
    magic.FileMagic := filemagic;
    Result := magic.FromFile(filename);
  finally
    FreeAndNil(magic);
  end;
end;

function DetectMimeTypeFromFile(filename: string; filemagic: string): string;
var
  magic: TMagic;
begin
  magic := TMagic.Create();
  try
    magic.FileMagic := filemagic;
    magic.OnlyMimeType := True;
    Result := magic.FromFile(filename);
  finally
    FreeAndNil(magic);
  end;
end;

function DetectMimeEncodingFromFile(filename: string; filemagic: string): string;
var
  magic: TMagic;
begin
  magic := TMagic.Create();
  try
    magic.FileMagic := filemagic;
    magic.OnlyMimeEncoding := True;
    Result := magic.FromFile(filename);
  finally
    FreeAndNil(magic);
  end;
end;

constructor TMagic.Create();
var
  hnd: thandle;
begin
  Self.FHANDLE := 0;
  FOnlyMimeType := False;
  FOnlyMimeEncodig := False;
  FFileMagic := '';
  FFlags := MAGIC_MIME;
  Self.FHANDLE := magic_open(Self.FFlags);

end;

destructor TMagic.Destroy;
begin
  magic_close(Self.FHANDLE);
  inherited Destroy;
end;

procedure TMagic.SetOnlyMimeType(const AValue: boolean);
begin
  if (Self.FOnlyMimeType = AValue) then
  begin
    exit;
  end;

  Self.FOnlyMimeType := AValue;
  if (Self.FOnlyMimeType) then
  begin
    Self.FOnlyMimeEncoding := False;
    Self.FFlags := MAGIC_MIME_TYPE;
  end
  else if (not Self.FOnlyMimeEncoding) then
  begin
    Self.FFlags := MAGIC_MIME;
  end;
end;

procedure TMagic.SetOnlyMimeEncoding(const AValue: boolean);
begin
  if (Self.FOnlyMimeEncoding = AValue) then
  begin
    exit;
  end;

  Self.FOnlyMimeEncoding := AValue;
  if (Self.FOnlyMimeEncoding) then
  begin
    Self.FOnlyMimeType := False;
    Self.FFlags := MAGIC_MIME_ENCODING;
  end
  else if (not Self.FOnlyMimeType) then
  begin
    Self.FFlags := MAGIC_MIME;
  end;
end;

procedure TMagic.BeforeDetect;
begin
  if (not FileExists(Self.FFileMagic)) then
  begin
    raise Exception.Create('File magic does not exist: ' + Self.FFileMagic);
  end;
  magic_load(Self.FHANDLE, PChar(Self.FFileMagic));
  magic_setflags(Self.FHANDLE, Self.FFlags);
end;

function TMagic.FromFile(filename: string): string;
begin
  Self.BeforeDetect();
  if (not FileExists(filename)) then
  begin
    raise Exception.Create('File does not exist: ' + filename);
  end;
  Result := magic_file(Self.FHANDLE, PChar(filename));
end;

function TMagic.FromBuffer(var buffer; count: longint): string;
begin
  Self.BeforeDetect();
  Result := magic_buffer(Self.FHANDLE, buffer, count);
end;

end.
