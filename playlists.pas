unit playlists;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  classes,
  sysutils,
  functions;

procedure openasx_playlist(fname: string; var sl, sl2: tstringlist);
procedure openpls_playlist(fname: string; var sl: tstringlist);
procedure openkpl_playlist(fname: string; var sl: tstringlist);
procedure openm3u_playlist(fname: string; var sl, sl2: tstringlist);
procedure openOnline_playlist(fname: string; var sl, sl2: tstringlist);

implementation

var
  separator: char = {$IFDEF mswindows} '\'{$ENDIF}{$IFDEF linux} '/'{$ENDIF};

procedure openpls_playlist(fname: string; var sl: tstringlist);
var
  f: textfile;
  s: string;
begin
  sl.clear;

{$IFDEF mswindows}
  if fname[1] = '/' then delete(fname, 1, 1);
{$ENDIF}
  if not fileexists(fname) then exit;

  assignfile(f, fname);
  reset(f);
  while not eof(f) do
  begin
    readln(f, s);
    if substrpos('File', s) = 1 then
    begin
      delete(s, 1, substrpos('=', s));

      if extractfilepath(s) = '' then s := extractfilepath(fname) + separator + s;
      sl.add(s);
    end;
  end;
  closefile(f);
end;

procedure openkpl_playlist(fname: string; var sl: tstringlist);
var
  f: textfile;
  s: string;
  s2: string;
begin

end;

procedure openm3u_playlist(fname: string; var sl, sl2: tstringlist);
var
  f: textfile;
  s: string;
begin
  sl.clear;
  sl2.clear;

{$IFDEF mswindows}
  if fname[1] = '/' then delete(fname, 1, 1);
{$ENDIF}
  if not fileexists(fname) then
  begin
    exit;
  end;
  assignfile(f, fname);
  reset(f);
  while not eof(f) do
  begin
    readln(f, s);
    if trim(s) = '#EXTM3U' then continue;
    if substrpos('#', trim(s)) = 1 then
    begin
      delete(s, 1, pos(',', s));
      sl2.add(trim(s));
    end
    else
    begin
      if extractfilepath(s) = '' then s := extractfilepath(fname) + separator + s;
      sl.add(s);
    end;
  end;
  closefile(f);
end;

procedure openasx_playlist(fname: string; var sl, sl2: tstringlist);
var
  f: textfile;
  s, s2: string;
  sl_: tstringlist;
begin
  sl.clear;

{$IFDEF mswindows}
  if fname[1] = '/' then delete(fname, 1, 1);
{$ENDIF}
  if not fileexists(fname) then
  begin
    exit;
  end;
  sl_ := tstringlist.create;
  sl_.loadfromfile(fname);

  while pos('<Entry>', sl_.text) > 0 do
  begin
    s2 := copy(sl_.text, pos('<Entry>', sl_.text), pos('</Entry>', sl_.text) - pos('<Entry>', sl_.text) - 1);

    s := s2;
    if substrpos('<Ref href', s) > 0 then
    begin
      delete(s, 1, substrpos('"', s));

      s := copy(s, 1, substrpos('"', s) - 1);

      if extractfilepath(s) = '' then s := extractfilepath(fname) + separator + s;
      sl2.add(s);
    end;

    s := s2;
    if pos('<Title>', s) > 0 then
    begin
      delete(s, 1, substrpos('<Title>"', s));

      s := copy(s, 1, substrpos('"</Title>', s) - 1);

      if extractfilepath(s) = '' then s := extractfilepath(fname) + separator + s;
      sl.add(s);
    end;

    s2 := sl_.text;
    delete(s2, pos('<Entry>', s2), pos('</Entry>', s2) - pos('<Entry>', s2));
    sl_.text;
  end;

  sl_.free;
  exit;

  assignfile(f, fname);
  reset(f);
  while not eof(f) do
  begin
    readln(f, s);
    if substrpos('<Ref href', s) > 0 then
    begin
      delete(s, 1, substrpos('"', s));

      s := copy(s, 1, substrpos('"', s) - 1);

      if extractfilepath(s) = '' then s := extractfilepath(fname) + separator + s;
      sl.add(s);
    end;
  end;
  closefile(f);
end;

procedure openOnline_playlist(fname: string; var sl, sl2: tstringlist);
var
  f: textfile;
  s: string;
begin
  sl.clear;
  sl2.clear;

{$IFDEF mswindows}
  if fname[1] = '/' then delete(fname, 1, 1);
{$ENDIF}
  if not fileexists(fname) then
  begin
    exit;
  end;

  assignfile(f, fname);
  reset(f);
  while not eof(f) do
  begin
    readln(f, s);
    sl.add(copy(s, 1, pos(' == ', s) - 1));
    delete(s, 1, pos(' == ', s) + 3);
    sl2.add(s);
  end;
  closefile(f);
end;

end.
