
unit alsmplayer;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  msegui,

  mseterminal,
  mseclasses,
  sysutils,
  msetimer,
  classes,
  mclasses,
  msewindowwidget,

  mseprocess,
  process,
  msefileutils,
  msetypes;

const
  ver = '0.13.2 beta';

type
  TerrorEvent = procedure(const Sender: TObject; errormsg: string) of object;
  TPauseEvent = procedure(const Sender: TObject; const apausing: boolean; msg: string) of object;
  TPlayingEvent = procedure(const Sender: TObject; position, length: integer; msg: string) of object;
  TStartPlayEvent = procedure(const Sender: TObject; videoW, videoH: integer; msg: string) of object;
  TEndOfTrackEvent = procedure(const Sender: TObject; msg: string) of object;
  TConnectingEvent = procedure(const Sender: TObject; msg: string) of object;
  TchangeVolumeEvent = procedure(const Sender: TObject; avolume: integer) of object;
  TDebugMessageEvent = procedure(const Sender: TObject; msg: AnsiString) of object;
  TTakeCdPlaylistEvent = procedure(const Sender: TObject; msg: string) of object;

  tplaymode = (__cd, __dvd, __dvb, __tv, __localfile, __inet, __webcamera, __rtmp, __null);
  tlang = (ru, en);

  tvideoresolution = record
    width: integer;
    height: integer;
  end;

  tCurrentTrackInfo = record
    filename: msestring;
    name: string;
    video: tvideoresolution;

    Title,
      Artist,
      Album,
      Year,
      Comment,
      Genre: string;

  end;

type
  tvnorm = (NTSC, NTSC_M, NTSC_M_JP, NTSC_M_KR, PAL, PAL_BG,
    PAL_H, PAL_I, PAL_DK, PAL_M, PAL_N, PAL_Nc, PAL_60, SECAM, SECAM_B,
    SECAM_G, SECAM_H, SECAM_DK, SECAM_L, SECAM_Lc);

type
  TvCannelName = (Ch_R1, Ch_R2, Ch_R3, Ch_R4, Ch_R5, Ch_R6, Ch_R7, Ch_R8, Ch_R9, Ch_R10,
    Ch_R11, Ch_R12, Ch_SR1, Ch_SR2, Ch_SR3, Ch_SR4, Ch_SR5, Ch_SR6, Ch_SR7, Ch_SR8, Ch_SR11,
    Ch_SR12, Ch_SR13, Ch_SR14, Ch_SR15, Ch_SR16, Ch_SR17, Ch_SR18, Ch_SR19, Ch_K1, Ch_K2, Ch_K3,
    Ch_K4, Ch_K5, Ch_K6, Ch_K7, Ch_K8, Ch_K9, Ch_K10, Ch_K11, Ch_K12, Ch_K21, Ch_K22, Ch_K23,
    Ch_K24, Ch_K25, Ch_K26, Ch_K27, Ch_K28, Ch_K29, Ch_K30, Ch_K31, Ch_K32, Ch_K33, Ch_K34,
    Ch_K35, Ch_K36, Ch_K37, Ch_K38, Ch_K39, Ch_K40, Ch_K41, Ch_K42, Ch_K43, Ch_K44, Ch_K45, Ch_K46,
    Ch_K47, Ch_K48, Ch_K49, Ch_K50, Ch_K51, Ch_K52, Ch_K53, Ch_K54, Ch_K55, Ch_K56, Ch_K57, Ch_K58,
    Ch_K59, Ch_K60, Ch_K61, Ch_K62, Ch_K63, Ch_K64, Ch_K65, Ch_K66, Ch_K67, Ch_K68, Ch_K69, Ch_S01,
    Ch_S02, Ch_S03, Ch_S1, Ch_S2, Ch_S3, Ch_S4, Ch_S5, Ch_S6, Ch_S7, Ch_S8, Ch_S9, Ch_S10, Ch_S11,
    Ch_S12, Ch_S13, Ch_S14, Ch_S15, Ch_S16, Ch_S17, Ch_S18, Ch_S19, Ch_S20, Ch_S21, Ch_S22, Ch_S23,
    Ch_S24, Ch_S25, Ch_S26, Ch_S27, Ch_S28, Ch_S29, Ch_S30, Ch_S31, Ch_S32, Ch_S33, Ch_S34, Ch_S35,
    Ch_S36, Ch_S37, Ch_S38, Ch_S39, Ch_S40, Ch_S41);

const
  tvCannelNameStr: array[TVCannelName] of string = (
    'R1', 'R2', 'R3', 'R4', 'R5', 'R6', 'R7', 'R8', 'R9', 'R10',
    'R11', 'R12', 'SR1', 'SR2', 'SR3', 'SR4', 'SR5', 'SR6', 'SR7', 'SR8', 'SR11',
    'SR12', 'SR13', 'SR14', 'SR15', 'SR16', 'SR17', 'SR18', 'SR19', 'K1', 'K2', 'K3',
    'K4', 'K5', 'K6', 'K7', 'K8', 'K9', 'K10', 'K11', 'K12', 'K21', 'K22', 'K23',
    'K24', 'K25', 'K26', 'K27', 'K28', 'K29', 'K30', 'K31', 'K32', 'K33', 'K34',
    'K35', 'K36', 'K37', 'K38', 'K39', 'K40', 'K41', 'K42', 'K43', 'K44', 'K45', 'K46',
    'K47', 'K48', 'K49', 'K50', 'K51', 'K52', 'K53', 'K54', 'K55', 'K56', 'K57', 'K58',
    'K59', 'K60', 'K61', 'K62', 'K63', 'K64', 'K65', 'K66', 'K67', 'K68', 'K69', 'S01',
    'S02', 'S03', 'S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8', 'S9', 'S10', 'S11',
    'S12', 'S13', 'S14', 'S15', 'S16', 'S17', 'S18', 'S19', 'S20', 'S21', 'S22', 'S23',
    'S24', 'S25', 'S26', 'S27', 'S28', 'S29', 'S30', 'S31', 'S32', 'S33', 'S34', 'S35',
    'S36', 'S37', 'S38', 'S39', 'S40', 'S41');

  TvStr: array[tvnorm] of string[20] = ('NTSC', 'NTSC-M', 'NTSC-M-JP', 'NTSC-M-KR', 'PAL', 'PAL-BG',
    'PAL-H', 'PAL-I', 'PAL-DK', 'PAL-M', 'PAL-N', 'PAL-Nc', 'PAL-60', 'SECAM', 'SECAM-B',
    'SECAM-G', 'SECAM-H', 'SECAM-DK', 'SECAM-L', 'SECAM-Lc');

type
  PTVData = ^TTVData;
  TTVData = record
    Norm: tvnorm;
    Freq: integer;
    Name: msestring;
  end;

  TTVDataList = class
  private
    FTVList: TList;
  public
    constructor Create;
    destructor Destroy; override;
    function AddItem(Item: TTVData): integer;
    procedure RemoveItem(ItemIndex: integer);
    function GetData(Idx: integer): TTVData;
    procedure SetData(IDx: integer; Data: TTvdata);
    function Count: integer;
    procedure Exchange(idx1, idx2: integer);
  end;

type
  tmplayer = class(tcomponent)
  private
    fversion: string;
    fmplayer: string;
    fprogid: integer;
    ftimer: ttimer;
    fterm: tterminal;
    ftermfifo: tterminal;
    flang: tlang;
    fparams: string;
    fcache: string;
    findexing: boolean;
    fvolume: integer;
    flengthsec: integer;
    fpositionsec: integer;
    fhavevideostream: boolean;
    fbalance: real;
    FpauseWhenScreenshot: boolean;
    fequalizer: string;
    fcurdir: string;
    flasterror: string;
    fdoublebuf: boolean;
    fdisablescreensaver: boolean;
    fusefifo: boolean;
    fusefifointernal: boolean;
    fpausing: boolean;
    fmode: tplaymode;
    fplayplaylistback: boolean;
    fwebcam_preload: string;
    fmplayer_preload: string;
    fPlayFromPositionStr: string;
    flastcommandline: string;
    ftracknum: integer;
    fplaylist: tstringlist;
    fcurrenttrack: tCurrentTrackInfo;
    FChannelList: TTVDataList;
    fplaynexttrackallow: boolean;
    frepeatplaylist: boolean;
    fvectors: boolean;
    fflip: boolean;
    fmirror: boolean;
    frotate90: boolean;
    fvolnorm: boolean;
    fkaraoke: boolean;
    fextrastereo: boolean;
    faudiooutput: string;
    fvideooutput: string;
    faudiooutputlist: tstringlist;
    fvideooutputlist: tstringlist;
    faval_audiooutputlist: tstringlist;
    faval_videooutputlist: tstringlist;
    fdisplay: tcustomwindowwidget;
    fwebcameradevice: string;
    ftvtunerdevice: string;
    fcdromdevice: string;
    fDVDusenavigation: boolean;
    fdevice_CastomDisplayID: string;
    fbrightness: integer;
    fcontrast: integer;
    fgamma: integer;
    fhue: integer;
    fsaturation: integer;
    fvideo_to_another_display: boolean;
    fadditional_video_settings: string;
    prevvolume: integer;
    prevvolumemute: integer;
    prevposition: integer;
    beginplayback: boolean;
    playnexttrack: boolean;
    fNo_stream_found: boolean;
    connecttocddevice: boolean;
    stop_it: boolean;
    f_getvideoresolutionappempt: byte;
    FwriteOutToConsole: boolean;
    fAddMplayerMessagesToDebug: boolean;
    fdebugplaying: boolean;
    fonStartplayEv: tStartPlayEvent;
    fonEndOfTrackEv: TEndOfTrackEvent;
    fonPlayingEv: TPlayingEvent;
    fonPauseEv: tpauseEvent;
    fonChangeVolumeEv: TchangeVolumeEvent;
    fonErrorEv: TerrorEvent;
    fonConnectingEv: TConnectingEvent;
    fgetdebugmessage: TDebugMessageEvent;
    ftakecdplaylistEv: TTakeCdPlaylistEvent;
    fstartdestroy: boolean;

  protected

    procedure on_procfinished(const sender: TObject);
    procedure on_receivetext(const sender: TObject; var atext: AnsiString; const errorinput: Boolean);
    procedure on_pipebroken(const sender: TObject);
    procedure on_receivetextfifo(const sender: TObject; var atext: AnsiString; const errorinput: Boolean);
    procedure on_timer(const sender: tobject);
    function getWindowId: string;
    function cmdline(source: string): string;
    procedure SetVolume(avalue: integer);
    procedure setposition(avalue: integer);
    procedure __play(source: string);
    procedure __openaudiocd(trucknum: integer);
    function termstatus: boolean;

    procedure setparam(vol: integer; var vparam: integer; capt: string; _start, _end: integer; _interval: integer);
    procedure setbrightness(const avalue: integer);
    procedure setcontrast(const avalue: integer);
    procedure setgamma(const avalue: integer);
    procedure sethue(const avalue: integer);
    procedure setsaturation(const avalue: integer);

    procedure setbalance(const avalue: real);
    procedure setequalizer(const avalue: string);

    procedure setvolnorm(const avalue: boolean);
    procedure setkaraoke(const avalue: boolean);
    procedure setextrastereo(const avalue: boolean);

    procedure dvdnav(navcommand: string);
    function lng(line: string): string;
    procedure debug(line: string);
    procedure startdebug;

    procedure sendcmd(cmd: string; showdebug: boolean);
    function createfifofile: boolean;
    procedure deletefifofile;
  public

    constructor create(aowner: tcomponent); override;
    destructor destroy; override;

    procedure play(source: string);
    procedure play(StartPositionInSeconds: integer);
    procedure play;
    procedure pause;
    procedure stop;
    procedure next;
    procedure prev;
    procedure mute;
    procedure screenshot;

    property version: string read fversion;
    property pausing: boolean read fpausing;
    property playing: boolean read termstatus;

    property length: integer read flengthsec;
    property position: integer read fpositionsec write setposition;
    procedure setpositionpersent(avalue: integer);
    property currenttrack: tCurrentTrackInfo read fcurrenttrack;
    function checkmplayer: boolean;
    function checksource(source: string): tplaymode;

    procedure OpenWebcamera;
    procedure OpenWebcamera_withpreload;
    function checkwebcamera: boolean;

    procedure OpenDVD;
    procedure dvdnavigation_up;
    procedure dvdnavigation_down;
    procedure dvdnavigation_left;
    procedure dvdnavigation_right;
    procedure dvdnavigation_menu;
    procedure dvdnavigation_select;
    procedure dvdnavigation_prev;
    procedure dvdnavigation_mouse;

    procedure OpenTV();
    procedure SetChannelByfreq(const Ch_freq: integer);
    procedure SetChannelByName(const Ch_name: TvCannelName);
    procedure SetChannelNorm(const __TVNorm: tvnorm);

    procedure ConnectToAudioCD;
    procedure openaudiocd;
    procedure openaudiocd(tracknum: integer);

    procedure SendCommandToMPlayer(cmd: string);
    procedure getpreview(const source, outdir: string; const pos_: integer);

    property audiooutputlist: tstringlist read faudiooutputlist;
    property videooutputlist: tstringlist read fvideooutputlist;

    property processID: integer read fprogid;

    procedure getavaliableoutputs;
    property aval_audiooutputlist: tstringlist read faval_audiooutputlist;
    property aval_videooutputlist: tstringlist read faval_videooutputlist;

    property havevideostream: boolean read fhavevideostream;
    property lastcommandline: string read flastcommandline;

  published
    property usefifo: boolean read fusefifo write fusefifo;
    property doublebuf: boolean read fdoublebuf write fdoublebuf;
    property disablescreensaver: boolean read fdisablescreensaver write fdisablescreensaver;

    property preload_webcam: string read fwebcam_preload write fwebcam_preload;
    property preload_mplayer: string read fmplayer_preload write fmplayer_preload;

    property mplayer: string read fmplayer write fmplayer;
    property lang: tlang read flang write flang;
    property volume: integer read fvolume write SetVolume;
    property cache: string read fcache write fcache;
    property mode: tplaymode read fmode write fmode;
    property indexing: boolean read findexing write findexing;
    property playplaylistback: boolean read fplayplaylistback write fplayplaylistback;
    property equalizer: string read fequalizer write setequalizer;
    property PlayFromPositionStr: string read fPlayFromPositionStr write fPlayFromPositionStr;

    property tracknum: integer read ftracknum write ftracknum;
    property Playlist: tstringlist read fplaylist write fplaylist;
    property ListofChanell: TTVDataList read FChannelList write FChannelList;
    property PlayNextTrackAllow: boolean read fplaynexttrackallow write fplaynexttrackallow;
    property RepeatPlaylist: boolean read frepeatplaylist write frepeatplaylist;

    property audiooutput: string read faudiooutput write faudiooutput;
    property videooutput: string read fvideooutput write fvideooutput;

    property device_webcamera: string read fwebcameradevice write fwebcameradevice;
    property device_tvtuner: string read ftvtunerdevice write ftvtunerdevice;
    property device_cdrom: string read fcdromdevice write fcdromdevice;
    property display: tcustomwindowwidget read fdisplay write fdisplay;
    property DVDusenavigation: boolean read fDVDusenavigation write fDVDusenavigation;
    property device_CastomDisplayID: string read fdevice_CastomDisplayID write fdevice_CastomDisplayID;
    property VideoToAnotherDesktop: boolean read fvideo_to_another_display write fvideo_to_another_display;

    property VideoEffect_vectors: boolean read fvectors write fvectors;
    property VideoEffect_flip: boolean read fflip write fflip;
    property VideoEffect_mirror: boolean read fmirror write fmirror;
    property VideoEffect_Rotate90: boolean read frotate90 write frotate90;

    property AudioEffect_volumeNorm: boolean read fvolnorm write setvolnorm;
    property AudioEffect_karaoke: boolean read fkaraoke write setkaraoke;
    property AudioEffect_extrastereo: boolean read fextrastereo write setextrastereo;

    property video_brightness: integer read fbrightness write setbrightness;
    property video_contrast: integer read fcontrast write setcontrast;
    property video_gamma: integer read fgamma write setgamma;
    property video_hue: integer read fhue write sethue;
    property video_saturation: integer read fsaturation write setsaturation;

    property balance: real read fbalance write setbalance;

    property debug_showplaying: boolean read fdebugplaying write fdebugplaying;
    property debug_WriteOutToConsole: boolean read fwriteOutToConsole write fwriteOutToConsole;
    property debug_AddMplayerOutputToDebug: boolean read fAddMplayerMessagesToDebug write fAddMplayerMessagesToDebug;

    property aditionalparams: string read fparams write fparams;
    property additional_videoparams: string read fadditional_video_settings write fadditional_video_settings;

    property onStartPlay: tStartPlayEvent read fonStartPlayEv write fonStartPlayEv;
    property onPlaying: TPlayingEvent read fonPlayingEv write fonPlayingEv;
    property onPause: TPauseEvent read fonpauseEv write fonpauseEv;
    property onEndOfTrack: TEndOfTrackEvent read fonEndOfTrackEv write fonendoftrackEv;
    property onError: TErrorEvent read fonErrorEv write fonErrorEv;
    property onConnecting: TConnectingEvent read fonConnectingEv write fonConnectingEv;
    property onTakeCdPlaylistEvent: TTakeCdPlaylistEvent read ftakecdplaylistEv write ftakecdplaylistEv;
    property onChangeVolume: TchangeVolumeEvent read fonChangeVolumeEv write fonChangeVolumeEv;
    property onGetDebugMessage: TDebugMessageEvent read fgetdebugmessage write fgetdebugmessage;
  end;

var
  ffifofile: string = '/tmp/alsmplayerfifofile';

implementation

var
  PAUSETIME: integer;
  fifotext: string;

function SecondsToFmtStr(seconds: longint): string;
var
  min, sec, hour: longint;
  sm, ss, sh: string;
begin
  if seconds > 0 then
  begin
    hour := seconds div (60 * 60);

    if hour = 0
      then min := seconds div 60
    else min := (seconds - 60 * 60 * hour) div 60;

    sec := seconds mod 60;

    str(min, sm);
    str(sec, ss);
    str(hour, sh);

    if min < 10 then sm := '0' + sm;
    if sec < 10 then ss := '0' + ss;
    if hour < 10 then sh := '0' + sh;

    result := sh + ':' + sm + ':' + ss;
  end
  else Result := '00:00:00';
end;

procedure tmplayer.sendcmd(cmd: string; showdebug: boolean);
begin
  if fusefifointernal then
  begin
    ftermfifo.execprog('echo "' + cmd + '" > ' + ffifofile);
    ftermfifo.waitforprocess;
    if showdebug then debug('SEND CMD VIA FIFO : "' + cmd + '"');
  end
  else
  begin
    fterm.writestrln(cmd);
    if showdebug then debug('SEND CMD DIRECTLY : "' + cmd + '"');
  end;

end;

function tmplayer.createfifofile: boolean;

  procedure errorcreatefifo;
  begin
    fusefifo := false;
    debug('CREATE FIFO FILE: error while creating fifo file:"' + ffifofile + '"');
  end;

begin
  result := true;
  deletefifofile;
  if fileexists(ffifofile) then
  begin
    errorcreatefifo; result := false;
  end;
  fifotext := '';
  ftermfifo.execprog('mkfifo ' + ffifofile);
  ftermfifo.waitforprocess;
  if trim(fifotext) > '' then
  begin
    errorcreatefifo; result := false;
  end;
  if result then debug('CREATE FIFO FILE: fifo file:"' + ffifofile + '" create');
end;

procedure tmplayer.deletefifofile;
begin
  if fusefifointernal then
    if trydeletefile(ffifofile)
      then debug('delete FIFO FILE: fifo file deleted successfully')
    else debug('delete FIFO FILE: Error while delete fifo file. May be file not exsists.');
end;

procedure tmplayer.on_receivetextfifo(const sender: TObject; var atext: AnsiString; const errorinput: Boolean);
begin
  fifotext := fifotext + atext;
end;

procedure tmplayer.startdebug;
begin
  debug('========================================================');
  debug('ALSMPLAYER VERSION ' + fversion);
  debug('NOTE: strings with <***> is output of alsmmplayer class,');
  debug('      without is output of mplayer');
  debug('========================================================');
end;

procedure tmplayer.debug(line: AnsiString);
begin
  if FwriteOutToConsole then writeln('*** (' + self.name + ') ' + line);
  if not fstartdestroy then
    if assigned(fgetdebugmessage) then fgetdebugmessage(self, '*** (' + self.name + ') ' + line);
end;

constructor tmplayer.create(aowner: tcomponent);
var
  s: string;
  i: integer;
begin
  fstartdestroy := false;
  fversion := ver;
  debug('WELCOME TO ALSMPLAYER version ' + fversion + ' (component:' + self.name + ')');
  fcurdir := getcurrentdir;
  fterm := tterminal.create(nil);
  ftermfifo := tterminal.create(nil);
  fterm.optionsprocess := [pro_input, pro_output];
  flang := ru;
  fdebugplaying := false;

  randomize;
  for i := 1 to 5 do
    ffifofile := ffifofile + inttostr(random(9));

{$IFDEF mswindows}
  fterm.optionsprocess := fterm.optionsprocess + [pro_inactive];
  ftermfifo.optionsprocess := fterm.optionsprocess + [pro_inactive];
{$ENDIF}

  fterm.onprocfinished := @on_procfinished;
  fterm.onreceivetext := @on_receivetext;
  fterm.onerrorpipebroken := @on_pipebroken;
  fterm.oninputpipebroken := @on_pipebroken;

  ftermfifo.onreceivetext := @on_receivetextfifo;

  ftimer := ttimer.create(nil);
  ftimer.ontimer := @on_timer;
  ftimer.interval := 1000000;
  ftimer.enabled := true;

  fdevice_CastomDisplayID := '0';
  fequalizer := '0:0:0:0:0:0:0:0:0:0';
  fPlayFromPositionStr := '';

{$IFDEF mswindows}
  fmplayer := 'mplayer_portable\Mplayer.exe';
{$ENDIF}
{$IFDEF linux}
  fmplayer := '/usr/bin/mplayer';
{$ENDIF}

  fvolume := 50;
  prevvolume := fvolume;
  fmode := __localfile;
  pausetime := 0;

  fplaylist := tstringlist.create;
  FChannelList := TTVDataList.Create;
  ftrackNum := -1;

  fplaynexttrackallow := true;
  frepeatplaylist := false;
  fplayplaylistback := false;

  fdoublebuf := false;
  fdisablescreensaver := true;

  fwebcam_preload := '';
  fmplayer_preload := '';

  faudiooutput := 'default';
  fvideooutput := 'default';

  faudiooutputlist := tstringlist.create;
  faudiooutputlist.add('default'); faudiooutputlist.add('alsa');
  faudiooutputlist.add('alsa5'); faudiooutputlist.add('oss');
  faudiooutputlist.add('sdl'); faudiooutputlist.add('arts');
  faudiooutputlist.add('esd'); faudiooutputlist.add('jack');
  faudiooutputlist.add('nas'); faudiooutputlist.add('coreaudio');
  faudiooutputlist.add('openal'); faudiooutputlist.add('pulse');
  faudiooutputlist.add('sgi'); faudiooutputlist.add('sun');
  faudiooutputlist.add('win32'); faudiooutputlist.add('dsound');
  faudiooutputlist.add('dart'); faudiooutputlist.add('dxr2');
  faudiooutputlist.add('ivtv'); faudiooutputlist.add('v4l2');
  faudiooutputlist.add('mpegpes');

  fvideooutputlist := tstringlist.create;
  fvideooutputlist.add('default'); fvideooutputlist.add('x11');
  fvideooutputlist.add('xover'); fvideooutputlist.add('vdpau');
  fvideooutputlist.add('xvmc'); fvideooutputlist.add('dga');
  fvideooutputlist.add('sdl'); fvideooutputlist.add('vidix');
  fvideooutputlist.add('xvidix'); fvideooutputlist.add('cvidix');
  fvideooutputlist.add('winvidix'); fvideooutputlist.add('direct3d');
  fvideooutputlist.add('directx'); fvideooutputlist.add('kva');
  fvideooutputlist.add('quartz'); fvideooutputlist.add('corevideo');
  fvideooutputlist.add('fbdev'); fvideooutputlist.add('fbdev2');
  fvideooutputlist.add('vesa'); fvideooutputlist.add('svga');
  fvideooutputlist.add('gl'); fvideooutputlist.add('gl2');
  fvideooutputlist.add('aa'); fvideooutputlist.add('caca');
  fvideooutputlist.add('bl'); fvideooutputlist.add('ggi');
  fvideooutputlist.add('directfb'); fvideooutputlist.add('dfbmga');
  fvideooutputlist.add('mga'); fvideooutputlist.add('xmga');
  fvideooutputlist.add('s3fb'); fvideooutputlist.add('wii');
  fvideooutputlist.add('3dfx'); fvideooutputlist.add('tdfxfb');
  fvideooutputlist.add('tdfx_vid'); fvideooutputlist.add('dxr2');
  fvideooutputlist.add('dxr3'); fvideooutputlist.add('ivtv');
  fvideooutputlist.add('v4l2'); fvideooutputlist.add('mpegpes');
  fvideooutputlist.add('zr'); fvideooutputlist.add('zr2');
  fvideooutputlist.add('yuv4mpeg'); fvideooutputlist.add('gif89a');
  fvideooutputlist.add('jpeg'); fvideooutputlist.add('pnm');
  fvideooutputlist.add('png'); fvideooutputlist.add('tga');
  fvideooutputlist.add('null'); fvideooutputlist.add('matrixview');
  fvideooutputlist.add('xv'); fvideooutputlist.add('gl:yuv=2:force-pbo:ati-hack');
  inherited;
end;

procedure tmplayer.getavaliableoutputs;
var
  s: string;
  p: tprocess;
  i: integer;
  sl: classes.tstringlist;
begin
  faval_videooutputlist := tstringlist.create;
  faval_videooutputlist.add('default');

  faval_audiooutputlist := tstringlist.create;
  faval_audiooutputlist.add('default');

  if checkmplayer then
  begin
    sl := classes.tstringlist.create;
    p := tprocess.create(nil);
    p.options := p.Options + [powaitonexit, pousepipes, ponoconsole];

    p.commandline := fmplayer + ' -vo help';
    p.execute;
    sl.loadfromstream(p.output);
    if sl.count > 0 then
      for i := 3 to sl.count - 1 do
      begin
        s := trim(sl[i]);
        if s = '' then continue;
        faval_videooutputlist.add(copy(s, 1, system.pos('	', s) - 1));
      end;

    sl.clear;
    p.commandline := fmplayer + ' -ao help';
    p.execute;
    sl.loadfromstream(p.output);

    if sl.count > 0 then
      for i := 3 to sl.count - 1 do
      begin
        s := trim(sl[i]);
        if s = '' then continue;
        faval_audiooutputlist.add(copy(s, 1, system.pos('	', s) - 1));
      end;
    sl.free;
    p.free;
  end;
end;

destructor tmplayer.destroy;
begin
  try
    fstartdestroy := true;
    stop;
    deletefifofile;
    debug('Exit alsmplayer (Component:' + self.name + ') . Bye!');
    fterm.free;
    ftermfifo.free;
    faval_audiooutputlist.free;
    faval_videooutputlist.free;
    fplaylist.free;
    fvideooutputlist.free;
    faudiooutputlist.free;
    ftimer.free;
  except
  end;
  inherited;
end;

function tmplayer.lng(line: string): string;
begin
  result := line;

end;

function tmplayer.termstatus: boolean;
begin
  result := fterm.running;
end;

procedure tmplayer.on_procfinished(const sender: TObject);
var
  msg: string;
begin
  fusefifointernal := fusefifo;
  deletefifofile;
  fcurrenttrack.Title := '';
  fcurrenttrack.Artist := '';
  fcurrenttrack.Album := '';
  fcurrenttrack.Year := '';
  fcurrenttrack.Comment := '';
  fcurrenttrack.Genre := '';

  case fmode of
    __cd:
      begin
        if connecttocddevice then
        begin
          connecttocddevice := false;
          if assigned(ftakecdplaylistEv) then ftakecdplaylistEv(self, 'audio CD playlist (' + inttostr(fplaylist.count) + ' track(s).');
        end
        else msg := 'End of CD track';
      end;
    __webcamera: msg := 'Webcamera is switch off';
    __tv: msg := 'TV Tuner is switch off';
    __dvd: msg := 'DVD is power off';
    __inet: if fNo_stream_found then msg := 'No stream found' else msg := 'End of stream';
  else
    begin
      if fileexists(fcurrenttrack.filename)
        then msg := 'End of track'
      else msg := 'File not found';
    end;
  end;
  debug(msg);

  if assigned(fonEndOfTrackEv) then fonEndOfTrackEv(self, lng(msg));

  debug('ON PROGFINISHED : Request for next ...');
  if not stop_it
    then
  begin
    debug('ON PROGFINISHED : Assepted');
    if fplaynexttrackallow
      then playnexttrack := true;
  end
  else debug('ON PROGFINISHED : Cancelled');

end;

procedure tmplayer.on_pipebroken(const sender: TObject);
begin
  debug('pipe is broken');
end;

procedure tmplayer.on_receivetext(const sender: TObject; var atext: AnsiString; const errorinput: Boolean);
var
  s: string;
  i, pos_: integer;
begin
  if fAddMplayerMessagesToDebug then debug(trim(atext));

  if FpauseWhenScreenshot then
  begin
    FpauseWhenScreenshot := false;
    debug('RECEIVE TEXT: Pausing (screenshot maked when pause)');
    sendcmd('pause', true);
    exit;
  end;

  if (system.pos('Starting playback', atext) > 0) or (system.pos('Начало воспроизведения...', atext) > 0)
    then
  begin
    debug('RECEIVE TEXT: Starting playback');
    fpausing := false;
    flengthsec := 0;
    case fmode of
      __webcamera:
        begin
          fcurrenttrack.video.Width := 640;
          fcurrenttrack.video.height := 480;
          debug('RECEIVE TEXT: Starting playback webcamera');
          if assigned(fonstartplayEv) then fonstartplayEv(self, fcurrenttrack.video.Width, fcurrenttrack.video.height, lng('Webcamera is switch on'));
        end;
      __dvd:
        begin
          fcurrenttrack.video.Width := 640;
          fcurrenttrack.video.height := 480;
          debug('RECEIVE TEXT: Starting playback DVD');
          if assigned(fonstartplayEv) then fonstartplayEv(self, fcurrenttrack.video.Width, fcurrenttrack.video.height, lng('DVD is power on'));
        end;
      __tv:
        begin
          fcurrenttrack.video.Width := 640;
          fcurrenttrack.video.height := 480;
          debug('RECEIVE TEXT: Starting playback TVtuner');
          if assigned(fonstartplayEv) then fonstartplayEv(self, fcurrenttrack.video.Width, fcurrenttrack.video.height, lng('TV Tuner is switch on'));
        end;
      __cd:
        begin
          fcurrenttrack.video.Width := 0;
          fcurrenttrack.video.height := 0;
          debug('RECEIVE TEXT: Starting playback CD');
          if assigned(fonstartplayEv) then fonstartplayEv(self, fcurrenttrack.video.Width, fcurrenttrack.video.height, lng('Audio CD is started'));
        end;
    else
      begin
        debug('RECEIVE TEXT: Try get video resolution ...');
        sendcmd('get_video_resolution', true);
        beginplayback := true;
      end;
    end;
  end;

  if not fhavevideostream then
    if system.pos('ANS_VIDEO_RESOLUTION=', atext) > 0 then
    begin
      debug('RECEIVE TEXT: get video resolution, try decode ... ');
      s := atext;
      delete(s, 1, system.pos('ANS_VIDEO_RESOLUTION', s) + 1);
      delete(s, 1, system.pos('=''', s) + 1);
      for i := 1 to system.length(s) do if s[i] = #10 then delete(s, i, 1);
      if system.pos('x', s) > 0
        then
      begin
        fcurrenttrack.video.Width := strtoint(copy(s, 1, system.pos('x', s) - 2));
        delete(s, 1, system.pos('x', s) + 1);
        fcurrenttrack.video.height := strtoint(copy(s, 1, system.pos('''', s) - 1));
        fhavevideostream := true;
      end
      else
      begin
        fcurrenttrack.video.Width := 0;
        fcurrenttrack.video.height := 0;
        fhavevideostream := false;
      end;
      debug('RECEIVE TEXT: video resolution : '
        + inttostr(fcurrenttrack.video.Width) + 'x'
        + inttostr(fcurrenttrack.video.height));
      beginplayback := true;
    end;

  if (system.pos('ANS_TIME_POSITION=', atext) > 0) then
  begin
    s := atext;
    delete(s, 1, system.length('ANS_TIME_POSITION=') + system.pos('ANS_TIME_POSITION=', s) - 1);
    s := copy(s, 1, system.pos('.', s) - 1);
    fpositionsec := strtoint(trim(s));
    prevposition := fpositionsec;

    if fmode = __cd
      then
    begin
      if assigned(fonPlayingEv) then fonPlayingEv(self, fpositionsec, flengthsec, lng('Audio CD is started'));
    end
    else if assigned(fonPlayingEv) then fonPlayingEv(self, fpositionsec, flengthsec, lng('Playing'));
  end;

  if system.pos('ANS_LENGTH=', atext) > 0 then
  begin
    s := atext;
    delete(s, 1, system.pos('ANS_LENGTH=', s) + system.length('ANS_LENGTH=') - 1);
    s := copy(s, 1, system.pos('.', s) - 1);
    flengthsec := strtoint(trim(s));

  end;

  if system.pos(' Title:', atext) > 0 then
  begin
    s := atext;
    delete(s, 1, system.pos(' Title:', atext) + 7);
    s := copy(s, 1, system.pos(#10, s));
    fcurrenttrack.title := trim(s);
  end;

  if system.pos(' Artist:', atext) > 0 then
  begin
    s := atext;
    delete(s, 1, system.pos(' Artist:', atext) + 8);
    s := copy(s, 1, system.pos(#10, s));
    fcurrenttrack.Artist := trim(s);
  end;

  if system.pos(' Album:', atext) > 0 then
  begin
    s := atext;
    delete(s, 1, system.pos(' Album:', atext) + 7);
    s := copy(s, 1, system.pos(#10, s));
    fcurrenttrack.Album := trim(s);
  end;

  if system.pos(' Year:', atext) > 0 then
  begin
    s := atext;
    delete(s, 1, system.pos(' Year:', atext) + 6);
    s := copy(s, 1, system.pos(#10, s));
    fcurrenttrack.Year := trim(s);
  end;

  if system.pos(' Comment:', atext) > 0 then
  begin
    s := atext;
    delete(s, 1, system.pos(' Comment:', atext) + 9);
    s := copy(s, 1, system.pos(#10, s));
    fcurrenttrack.Comment := trim(s);
  end;

  if system.pos(' Genre:', atext) > 0 then
  begin
    s := atext;
    delete(s, 1, system.pos(' Genre:', atext) + 7);
    s := copy(s, 1, system.pos(#10, s));
    fcurrenttrack.Genre := trim(s);
  end;

  if (system.pos('Found audio CD with', atext) > 0) then
  begin
    s := atext;
    delete(s, 1, system.pos('Found audio CD with', s) + system.length('Found audio CD with'));
    debug('RECEIVE TEXT: Found audio CD with ' + copy(s, 1, system.pos(' ', s) - 1) + ' track(s).');
    if connecttocddevice
      then
    begin
      fplaylist.clear;
      debug(atext);
      for i := 0 to strtoint(copy(s, 1, system.pos(' ', s) - 1)) - 1 do
      begin
        fplaylist.add(lng('CD track') + ' ' + inttostr(i + 1));
        debug(lng('CD track') + ' ' + inttostr(i + 1));
      end;
      debug('RECEIVE TEXT: Format audio CD playlist (' + inttostr(fplaylist.count) + ' track(s). )');
      ftracknum := 0;
      sendcmd('quit', true);

    end;
  end
  else
    if (system.pos('404:', atext) > 0) or (system.pos('No stream found', atext) > 0) then
    begin
      debug('*** RECEIVE TEXT: Server returned 404: File Not Found (No stream found)');
      fNo_stream_found := true;
      if assigned(fonErrorEv) then fonErrorEv(self, lng('No stream found'));
    end
    else
      if (system.pos('Connecting to server', atext) > 0) or (system.pos('Соединяюсь с сервером', atext) > 0) then
      begin
        debug('RECEIVE TEXT: Connecting to server');
        if assigned(fonConnectingEv) then fonConnectingEv(self, lng('Connecting to server ...'));
      end
      else
        if (system.pos('Connected', atext) > 0) then
        begin
          debug('RECEIVE TEXT: Connected');
          if assigned(fonConnectingEv) then fonConnectingEv(self, lng('Сonnected'));
        end
        else
          if (system.pos('Cache fill:', atext) > 0) or ((system.pos('Заполнение кэша:', atext) > 0)) then
          begin
            s := atext;
            delete(s, 1, system.pos(':', s));
            debug('RECEIVE TEXT: Cache fill:' + s);
            if assigned(fonConnectingEv) then fonConnectingEv(self, lng('Cache fill:') + s);
          end
          else
            if (system.pos('Generated index table', atext) > 0) then
            begin
              debug('RECEIVE TEXT: Generated index table');
              if assigned(fonConnectingEv) then fonConnectingEv(self, lng('Generated index table'));
            end
            else
              if (system.pos('Audio only file format detected', atext) > 0) then
              begin
                fhavevideostream := false;
                debug('*** RECEIVE TEXT: Audio only file format detected');
                if assigned(fonConnectingEv) then fonConnectingEv(self, lng('Audio only file format detected'));
              end
              else
                if (system.pos('Incomplete stream? Trying resync.', atext) > 0) then
                begin
                  debug('RECEIVE TEXT: Incomplete stream? Trying resync.');
                  if assigned(fonConnectingEv) then fonConnectingEv(self, lng('Incomplete stream? Trying resync'));
                end
                else
                  if (system.pos('Cache empty', atext) > 0) then
                  begin
                    debug('RECEIVE TEXT: Cache empty');
                    if assigned(fonConnectingEv) then fonConnectingEv(self, lng('Cache empty'));
                  end
                  else
                    if (system.pos('Cache not filling', atext) > 0) then
                    begin
                      debug('RECEIVE TEXT: Cache not filling !');
                      pausetime := 20;
                      if assigned(fonPlayingEv) then fonPlayingEv(self, fpositionsec, flengthsec, lng('Cache not filling'));
                    end;
end;

function tmplayer.checkmplayer: boolean;
begin
  if fmplayer = '' then
{$IFDEF mswindows}fmplayer := fcurdir + '\mplayer_portable\Mplayer.exe'; {$ENDIF}
{$IFDEF linux}fmplayer := '/usr/bin/mplayer'; {$ENDIF}

  if not fileexists(fmplayer)
    then
  begin
    result := false;
    flasterror := lng('Mplayer not found');
    debug('CHECK PLAYER: mplayer not found');
    if assigned(fonErrorEv) then fonErrorEv(self, lng('Mplayer not found'));
  end
  else
  begin
    result := true;
    debug('CHECK PLAYER: mplayer found : ' + fmplayer);
  end;
end;

function tmplayer.checksource(source: string): tplaymode;
begin
  result := __localfile;
  if (system.pos('mms://', source) > 0) or
    (system.pos('http://', source) > 0) or
    (system.pos('rtsp://', source) > 0) then result := __inet;

  if (system.pos('tv://', source) > 0) then result := __tv;

  if ((system.pos('tv://', source) > 0) and (system.pos('driver=v4l2', source) > 0)) or
    (source = 'webcamera') then result := __webcamera;

  if (system.pos('rtmp://', source) > 0) then result := __rtmp;

  if (system.pos('dvd://', source) > 0) or
    (system.pos('dvdnav://', source) > 0) then result := __dvd;

  if (system.pos(' cdd', source) > 0) then result := __cd;

  case result of
    __localfile: debug('CHECKSOUCE: source is local file');
    __inet: debug('CHECKSOUCE: source is internet source');
    __tv: debug('CHECKSOUCE: source is TV-tuner');
    __webcamera: debug('CHECKSOUCE: source is webcamera');
    __dvd: debug('CHECKSOUCE: source is DVD');
    __cd: debug('CHECKSOUCE: source is AUDIO CD');
  else debug('CHECKSOUCE: source is unknown!');
  end;
end;

function tmplayer.getWindowId: string;
begin
  if fvideo_to_another_display
    then
  begin
    if fdevice_CastomDisplayID > ''
      then result := '-wid ' + fdevice_CastomDisplayID
    else result := '-wid 0';
  end
  else
  try
    if assigned(fdisplay)
      then result := '-wid ' + inttostr(fdisplay.clientwinid)
    else
    begin
      flasterror := lng('Display ID error');
      if assigned(fonErrorEv) then fonErrorEv(self, lng('Display ID error'));
    end;
  except result := '';
  end;
end;

function tmplayer.cmdline(source: string): string;
var
  vo, ao: string;
  cach: string;
  pipe: string;
  indx: string;
  vf: string;
  playfrom: string;
  source_: string;
  vect: string;
  veffects: string;
  doublebuffer: string;
  screensaver: string;
  volnorm: string;
  rtmpcmd: string;
  slave_: string;
  fusefifo_: boolean;
begin
  if fvideooutputlist.indexof(fvideooutput) < 0 then fvideooutput := 'default';
  if faudiooutputlist.indexof(faudiooutput) < 0 then faudiooutput := 'default';

  if (fvideooutput = 'default') or (fvideooutput = '')
    then vo := ''
  else vo := ' -vo ' + fvideooutput;

  if (faudiooutput = 'default') or (faudiooutput = '')
    then ao := ''
  else ao := ' -ao ' + faudiooutput;

  case fmode of
    __localfile, __webcamera, __dvd: cach := '-nocache'
  else
    if (fcache > '')
      then
    begin
      if (strtoint(fcache) < 32) then fcache := '32';
      cach := ' -cache ' + fcache
    end
    else cach := '';
  end;

  if findexing then indx := ' -idx' else indx := '';
  if fmplayer_preload > '' then fmplayer_preload := fmplayer_preload + ' ';

  if fadditional_video_settings = ''
    then vf := '-vf screenshot '
  else vf := '-vf ' + fadditional_video_settings + ',screenshot';

  if fPlayFromPositionStr > ''
    then playfrom := '-ss ' + fPlayFromPositionStr
  else playfrom := '';

  if fvectors
    then vect := '-lavdopts vismv=7 '
  else vect := '';

  veffects := '';
  if fflip then veffects := veffects + '-vf-add harddup,flip ';
  if fmirror then veffects := veffects + '-vf-add mirror ';
  if frotate90 then veffects := veffects + '-vf-add rotate=0 ';

  if fdoublebuf
    then doublebuffer := '-double'
  else doublebuffer := '-nodouble';

  if fdisablescreensaver
    then screensaver := '-stop-xscreensaver'
  else screensaver := '';

  if fvolnorm
    then volnorm := 'volnorm=1,'
  else volnorm := '';

  if source > ''
    then source_ := '"' + source + '"'
  else source_ := '';

  fusefifo_ := fusefifo;

  if fmode = __rtmp
    then
  begin
{$IFDEF linux}
    rtmpcmd := 'rtmpdump -r ' + source_ + ' -o - 2>/dev/null |';
    source_ := '-';
    fusefifo_ := true;
    slave_ := '';
{$ENDIF}
{$IFDEF mswindows}
{$ENDIF}
  end
  else
  begin
    rtmpcmd := '';
    slave_ := '-slave';
  end;

  pipe := '';
  if fusefifo_
    then
  begin
    debug('CMD LINE: ready for creating fifo ...');
    if createfifofile
      then
    begin
      pipe := '-input file=' + ffifofile + ' ';
      slave_ := '';
    end
    else debug('CMD LINE: ERROR while creating fifo!');
  end;
  fusefifointernal := fusefifo_;

  f_getvideoresolutionappempt := 0;
  result := fmplayer_preload
    + rtmpcmd + ' '
    + fmplayer + ' -softvol -quiet '
    + slave_ + ' '
    + pipe
    + vect
    + '-volume ' + inttostr(fvolume) + ' '
    + getwindowId + ' '
    + vo + ' '
    + ao + ' '
    + doublebuffer + ' '
{$IFDEF linux}
  + screensaver + ' '
{$ENDIF}
  + indx + ' '
    + cach + ' '
    + '-framedrop '
    + '-af ' + volnorm + 'equalizer=' + fequalizer + ' '
    + '-brightness ' + inttostr(fbrightness) + ' '
    + '-contrast ' + inttostr(fcontrast) + ' '
{$IFDEF linux}

{$ENDIF}
  + '-hue ' + inttostr(fhue) + ' '
    + '-saturation ' + inttostr(fsaturation) + ' '
    + vf + ' '
    + playfrom + ' '
    + veffects
    + fparams + ' '
    + source_;
end;

procedure tmplayer.play(source: string);
begin
  stop_it := true;
  debug('play(source) : stop_it := true');
  fPlayFromPositionStr := '0';

  __play(source);
end;

procedure tmplayer.play(StartPositionInSeconds: integer);
begin
  stop_it := false;
  debug('play(StartPositionInSeconds) : stop_it := false');
  fPlayFromPositionStr := SecondsToFmtStr(StartPositionInSeconds);
  if fplaylist.count = 0
    then
  begin
    flasterror := lng('Playlist is empty');
    if assigned(fonErrorEv) then fonErrorEv(self, lng('Playlist is empty'));
    debug('PLAY: Playlist is empty');
    exit;
  end;
  if ftracknum < 0 then ftracknum := 0;
  if ftracknum > fplaylist.count - 1 then ftracknum := fplaylist.count - 1;

  __play(fplaylist[ftracknum]);
end;

procedure tmplayer.play;
begin
  stop_it := false;
  debug('play : stop_it := false');
  fPlayFromPositionStr := '0';
  if fplaylist.count = 0
    then
  begin
    flasterror := lng('Playlist is empty');
    if assigned(fonErrorEv) then fonErrorEv(self, lng('Playlist is empty'));
    debug('PLAY: Playlist is empty');
    exit;
  end;
  if ftracknum < 0 then ftracknum := 0;
  if ftracknum > fplaylist.count - 1 then ftracknum := fplaylist.count - 1;

  __play(fplaylist[ftracknum]);
end;

procedure tmplayer.__play(source: string);
var
  s: string;
  b: boolean;
begin
  startdebug;
  debug('PLAY ***');
  b := stop_it;
  stop;
  stop_it := b;
  if not checkmplayer then exit;
  fcurrenttrack.filename := source;
  fcurrenttrack.name := extractfilename(source);
  fmode := checksource(source);

  fhavevideostream := false;
  fNo_stream_found := false;
  s := cmdline(source);

  if fmode = __rtmp
    then {$IFDEF linux}
    if not findfile('rtmpdump', ['/bin', '/sbin', '/usr/bin', 'usr/sbin'])
{$ENDIF}
{$IFDEF windows}
    if not findfile('rtmpdump.exe', [extractfilepath(fmplayer)])
{$ENDIF}
    then
    begin
      if assigned(fonErrorEv) then fonErrorEv(self, lng('rtmpdump(needed for rtmp chanels) utility not found'));
      exit;
    end;

    debug('PLAY: ' + s);
    flastcommandline := s;
    fterm.execprog(s);
    fprogid := fterm.prochandle;
    flasterror := '';
    if assigned(fonConnectingEv) then fonConnectingEv(self, lng('Connecting ...'));
end;

procedure tmplayer.pause;
begin
  if not fterm.running then exit;
  if fpausing then
  begin
    pausetime := 0;
    fpausing := false;
    if prevvolume <> fvolume then setvolume(fvolume);
    if fmode = __localfile
      then
    begin
      if prevposition <> fpositionsec
        then setposition(fpositionsec)
      else sendcmd('pause', true);
    end
    else
    begin
      sendcmd('pause', true);
    end;
    debug('PAUSE: off');
    if assigned(fonPauseEv) then fonPauseEv(self, false, Lng('Playing'));
    exit;
  end;
  fpausing := true;
  debug('PAUSE: on');

  sendcmd('pause', true);

  if assigned(fonPauseEv) then fonPauseEv(self, true, lng('Pause'));
end;

procedure tmplayer.stop;
begin
  fpausing := true;
  pausetime := 0;
  stop_it := true;
  debug('stop : stop_it := true');
  if fterm.running
    then
  begin
    debug('STOP');

    sendcmd('quit', true);

    fterm.waitforprocess;
    if fmode = __inet then
    begin
      sleep(200);
      if fterm.running then
      begin
        fterm.killprocess();
        debug('STOP: send kill signal');
        sleep(1000);
        if fterm.running then debug('STOP: can`t stop process, maybe zomby');
        if assigned(fonEndOfTrackEv) then fonEndOfTrackEv(self, lng('End of track'));
      end;
    end;
  end;
  fcurrenttrack.video.Width := 0;
  fcurrenttrack.video.height := 0;
end;

procedure tmplayer.next;
var
  b, bs: boolean;
begin
  b := false;
  if not fplayplaylistback
    then
  begin
    if ftrackNum < fplaylist.count - 1 then b := true;
  end
  else
  begin
    if ftrackNum > 0 then b := true;
  end;

  if b
    then
  begin
    if fplayplaylistback
      then dec(ftracknum)
    else inc(ftrackNum);
    debug('NEXT: Next track is ' + inttostr(ftrackNum));
    if fmode = __cd then openaudiocd(ftrackNum + 1) else __play(fplaylist[ftracknum]);
  end
  else
  begin
    bs := stop_it;
    stop;
    stop_it := bs;
    debug('NEXT: stop_it := ' + booltostr(bs));
    debug('NEXT: End of playlist');
    if fmode = __cd
      then
    begin
      flasterror := lng('End of audio CD');
      if assigned(fonErrorEv) then fonErrorEv(self, lng('End of audio CD'));
    end
    else
    begin
      flasterror := lng('End of playlist');
      if assigned(fonErrorEv) then fonErrorEv(self, lng('End of playlist'));
    end;

    if not frepeatplaylist
      then
    begin
      debug('NEXT: Repeat playlist not allowed');
    end
    else
    begin
      if fplayplaylistback
        then ftracknum := fplaylist.count - 1
      else ftrackNum := 0;
      debug('NEXT: Repeat playlist. Next track is ' + inttostr(ftrackNum));
      if fmode = __cd then openaudiocd(ftrackNum + 1) else __play(fplaylist[ftracknum]);
    end;
  end;
end;

procedure tmplayer.prev;
var
  b: boolean;
begin
  b := false;
  if fplayplaylistback
    then
  begin
    if ftrackNum < fplaylist.count - 1 then b := true;
  end
  else
  begin
    if ftrackNum > 0 then b := true;
  end;

  if b
    then
  begin
    if fplayplaylistback
      then inc(ftrackNum)
    else dec(ftrackNum);

    debug('PREV: Prev track is ' + inttostr(ftrackNum));
    if fmode = __cd then openaudiocd(ftrackNum + 1) else __play(fplaylist[ftracknum]);
  end;
end;

procedure tmplayer.setvolume(avalue: integer);
begin
  if avalue < 0 then
  begin
    debug('VOLUME: Autocorrect volume from "' + inttostr(avalue) + '" to "0"');
    avalue := 0;
  end;

  if avalue > 100 then
  begin
    debug('VOLUME: Autocorrect volume from "' + inttostr(avalue) + '" to "100"');
    avalue := 100;
  end;

  fvolume := avalue;
  if fterm.running and not fpausing then
  begin
    sendcmd('volume ' + inttostr(avalue) + ' 1', true);
    prevvolume := fvolume;
  end;
  debug('VOLUME: ' + inttostr(fvolume));
  if assigned(fonchangevolumeEv) then fonchangevolumeEv(self, fvolume);
end;

procedure tmplayer.mute;
begin
  if fvolume <> 0
    then
  begin
    prevvolumemute := fvolume;
    fvolume := 0;
    debug('MUTE: on');
  end
  else
  begin
    fvolume := prevvolumemute;
    debug('MUTE: off');
  end;
  setvolume(fvolume);
end;

procedure tmplayer.setposition(avalue: integer);
begin
  ftimer.enabled := false;

  if avalue < 0 then
  begin
    debug('SETPOSITION: Autocorrect position from "' + inttostr(avalue) + '" to "0"');
    avalue := 0;
  end;

  if avalue > flengthsec then
  begin
    debug('SETPOSITION: Autocorrect position from "' + inttostr(avalue) + '" to "' + inttostr(flengthsec) + '"');
    avalue := flengthsec;
  end;

  fpositionsec := avalue;
  if fterm.running and not fpausing then
  begin
    debug('SETPOSITION: ' + inttostr(fpositionsec));
    sendcmd('seek ' + inttostr(avalue) + ' 2', true);
  end;
  ftimer.enabled := true;
end;

procedure tmplayer.setpositionpersent(avalue: integer);
begin
  if avalue < 0 then
  begin
    debug('SETPOSITIONPERSENT: Autocorrect position from "' + inttostr(avalue) + '" to "0"');
    avalue := 0;
  end;

  if avalue > 100 then
  begin
    debug('SETPOSITIONPERSENT: Autocorrect position from "' + inttostr(avalue) + '" to "100"');
    avalue := 100;
  end;
  fpositionsec := avalue;
  if fterm.running and not fpausing then
  begin
    debug('SETPOSITIONPERSENT: ' + inttostr(fpositionsec));
    sendcmd('seek ' + inttostr(avalue) + ' 1', true);
  end;
end;

procedure tmplayer.setparam(vol: integer; var vparam: integer; capt: string; _start, _end: integer; _interval: integer);
begin
  if vol < _start then
  begin
    debug('' + uppercase(capt) + ': Autocorrect from "' + inttostr(vol) + '" to "' + inttostr(_start) + '"');
    vol := _start;
  end;

  if vol > _end then
  begin
    debug('' + uppercase(capt) + ': Autocorrect from "' + inttostr(vol) + '" to "' + inttostr(_end) + '"');
    vol := _end;
  end;

  if fterm.running then
  begin
    debug('' + uppercase(capt) + ': pausing_keep_force ' + inttostr(vparam));

    sendcmd('pausing_keep_force ' + capt + ' ' + inttostr(vol) + ' ' + inttostr(_interval), true);
  end;
  vparam := vol;
end;

procedure tmplayer.setbalance(const avalue: real);
var
  vol: real; s: string; i: integer;
begin
  vol := avalue;
  if vol < -1 then
  begin
    debug('BALANCE : Autocorrect from "' + FLOATtostr(vol) + '" to - 1"');
    vol := -1;
  end;

  if vol > 1 then
  begin
    debug('BALANCE : Autocorrect from "' + floattostr(vol) + '" to 1"');
    vol := 1;
  end;

  if fterm.running then
  begin
    s := floattostr(vol);
    if s[1] = '-'
      then s := copy(s, 1, 4)
    else s := copy(s, 1, 3);
    for i := 1 to system.length(s) do
      if s[i] = ',' then s[i] := '.';

    debug('BALANCE : ' + s);

    sendcmd('balance ' + s + ' 1', true);
    for i := 1 to system.length(s) do
      if s[i] = '.' then s[i] := ',';
    vol := strtofloat(s);
  end;
  fbalance := vol;
end;

procedure tmplayer.setbrightness(const avalue: integer);
begin
  setparam(avalue, fbrightness, 'brightness', -100, 100, 1);
end;

procedure tmplayer.setcontrast(const avalue: integer);
begin
  setparam(avalue, fcontrast, 'contrast', -100, 100, 1);
end;

procedure tmplayer.setgamma(const avalue: integer);
begin
  setparam(avalue, fgamma, 'gamma', -100, 100, 1);
end;

procedure tmplayer.sethue(const avalue: integer);
begin
  setparam(avalue, fhue, 'hue', -100, 100, 1);
end;

procedure tmplayer.setsaturation(const avalue: integer);
begin
  setparam(avalue, fsaturation, 'saturation', -100, 100, 1);
end;

procedure tmplayer.screenshot;
begin
  if fterm.running then
  begin
    debug('SCREENSHOT');
    sendcmd('screenshot 0', true);

    if fpausing then FpauseWhenScreenshot := true;
  end
  else
  begin
    flasterror := lng('Mplayer not running');
    if assigned(fonErrorEv) then fonErrorEv(self, lng('Mplayer not running'));
  end;
end;

function tmplayer.checkwebcamera: boolean;
begin
  result := true;
{$IFDEF linux}
  if fwebcameradevice = '' then
  begin
    debug('CHECK WEBCAMERA ***: use ONLY simple device names, for example "video0"');
    flasterror := lng('Webcamera device not selected');
    if assigned(fonErrorEv) then fonErrorEv(self, lng('Webcamera device not selected'));
    result := false;
  end
  else
  begin
    if not fileexists('/dev/' + fwebcameradevice) then
    begin
      debug('CHECK WEBCAMERA ***: use ONLY simple device names, for example "video0"');
      flasterror := lng('Webcamera device not exists');
      if assigned(fonErrorEv) then fonErrorEv(self, lng('Webcamera device not exists'));
      result := false;
    end;
  end;
{$ENDIF}
end;

procedure tmplayer.openwebcamera;
var
  cmd: string;
begin
  startdebug;
  debug('WEBCAMERA ***');
  if (not checkmplayer) or (not checkwebcamera) then exit;

  fmode := __webcamera;
  stop;
  fcurrenttrack.filename := lng('Webcamera');
  fcurrenttrack.name := lng('Webcamera');
  fhavevideostream := true;
  flasterror := '';
  if assigned(fonConnectingEv) then fonConnectingEv(self, lng('Connecting ...'));

{$IFDEF linux}
  cmd := cmdline('') + ' tv:// -tv driver=v4l2:width=640:height=480:device=/dev/'
    + fwebcameradevice;
  cmd := fwebcam_preload + ' ' + cmd;
{$ENDIF}
{$IFDEF mswindows}
  cmd := cmdline('') + ' tv://';

{$ENDIF}

  debug('WEBCAMERA: ' + cmd);
  flastcommandline := trim(cmd);
  fterm.execprog(trim(cmd));
  fprogid := fterm.prochandle;
end;

procedure tmplayer.openwebcamera_withpreload;
var
  cmd: string;
begin
  startdebug;
  debug('WEBCAMERA (with preload) ***');
  if (not checkmplayer) or (not checkwebcamera) then exit;

  fmode := __webcamera;
  stop;
  fcurrenttrack.filename := lng('Webcamera');
  fcurrenttrack.name := lng('Webcamera');
  fhavevideostream := true;
  flasterror := '';
  if assigned(fonConnectingEv) then fonConnectingEv(self, lng('Connecting ...'));
  cmd := ' env LD_PRELOAD=/usr/lib/libv4l/v4l2convert.so '
    + cmdline('') + ' tv:// -tv driver=v4l2:width=640:height=480:device=/dev/'
    + fwebcameradevice;
  debug('WEBCAMERA (with preload): ' + cmd);
  flastcommandline := trim(cmd);
  fterm.execprog(trim(cmd));
  fprogid := fterm.prochandle;
end;

procedure tmplayer.ConnectToAudioCD;
begin
  startdebug;
  fmode := __cd;
  debug('AUDIO CD: Connecting to CD device ...');
  connecttocddevice := true;
  __openaudiocd(0);
end;

procedure tmplayer.openaudiocd;
begin
  startdebug;

  __openaudiocd(0);
end;

procedure tmplayer.openaudiocd(tracknum: integer);
begin
  startdebug;

  __openaudiocd(tracknum);
end;

procedure tmplayer.__openaudiocd(trucknum: integer);
var
  cmd, dev, tr: string;
  b: boolean;
begin
  debug('AUDIO CD ***');
  b := stop_it;
  stop;
  stop_it := b;
  if not checkmplayer then exit;
{$IFDEF linux}
  if fcdromdevice = '' then
  begin
    flasterror := lng('DVD(cdrom) device not selected');
    if assigned(fonErrorEv) then fonErrorEv(self, lng('DVD(cdrom) device not selected'));
    exit;
  end;
{$ENDIF}
  fmode := __cd;
  fcurrenttrack.filename := lng('AudioCD');
  fcurrenttrack.name := lng('AudioCD');
  fhavevideostream := false;
  flasterror := '';
  if connecttocddevice
    then
  begin
    if assigned(fonConnectingEv) then fonConnectingEv(self, lng('Connecting to CD device ...'));
  end
  else if assigned(fonConnectingEv) then fonConnectingEv(self, lng('Connecting ...'));

  if trucknum = 0 then tr := '' else tr := inttostr(trucknum);
  if (fcdromdevice = 'default') or (fcdromdevice = '')
    then dev := ''
  else dev := ' -cdrom-device /dev/' + fcdromdevice;
  cmd := cmdline('') + ' cdda://' + tr + dev;
  stop_it := false;
  debug('__openaudiocd(trucknum) : stop_it := false');
  debug('AUDIO CD: ' + cmd);
  flastcommandline := cmd;
  fterm.execprog(cmd);
  fprogid := fterm.prochandle;
end;

procedure tmplayer.OpenDVD;
var
  cmd, dev: string;
begin
  startdebug;
  debug('DVD ***');
  if not checkmplayer then exit;
{$IFDEF linux}
  if system.pos('-dvd-device', fparams) > 0
    then
  else
  begin
    if fcdromdevice = '' then
    begin
      flasterror := lng('DVD(cdrom) device not selected');
      if assigned(fonErrorEv) then fonErrorEv(self, lng('DVD(cdrom) device not selected'));
      exit;
    end;
    if not fileexists(fcdromdevice) then
    begin
      flasterror := lng('DVD(cdrom) device not exists');
      if assigned(fonErrorEv) then fonErrorEv(self, lng('DVD(cdrom) device not exists'));
      exit;
    end;
  end;
{$ENDIF}
  stop;
  fmode := __dvd;
  fcurrenttrack.filename := lng('DVD');
  fcurrenttrack.name := lng('DVD');
  fhavevideostream := true;
  flasterror := '';
  if assigned(fonConnectingEv) then fonConnectingEv(self, lng('Connecting ...'));

  if (fcdromdevice = 'default') or (fcdromdevice = '')
    then dev := ''
  else dev := ' -dvd-device /dev/' + fcdromdevice;

  if fDVDusenavigation
    then cmd := cmdline('') + ' dvdnav://' + dev
  else cmd := cmdline('') + ' dvd://' + dev;
  stop_it := false;
  debug('OpenDVD : stop_it := false');
  debug('DVD : ' + cmd);
  flastcommandline := cmd;
  fterm.execprog(cmd);
  fprogid := fterm.prochandle;
end;

procedure tmplayer.dvdnav(navcommand: string);
begin
  if fterm.running then
  begin
    debug('DVD MENU NAVIGATION: ' + navcommand);
    sendcmd('dvdnav ' + navcommand, true);
  end;
end;

procedure tmplayer.dvdnavigation_up;
begin
  dvdnav('up');
end;

procedure tmplayer.dvdnavigation_down;
begin
  dvdnav('down');
end;

procedure tmplayer.dvdnavigation_left;
begin
  dvdnav('left');
end;

procedure tmplayer.dvdnavigation_right;
begin
  dvdnav('right');
end;

procedure tmplayer.dvdnavigation_menu;
begin
  dvdnav('menu');
end;

procedure tmplayer.dvdnavigation_select;
begin
  dvdnav('select');
end;

procedure tmplayer.dvdnavigation_prev;
begin
  dvdnav('prev');
end;

procedure tmplayer.dvdnavigation_mouse;
begin
  dvdnav('mouse');
end;

procedure tmplayer.SendCommandToMPlayer(cmd: string);
begin
  if fterm.running then
  begin
    sendcmd(cmd, true);
    debug('SENDING USER COMMAND TO MPLAYER :' + cmd);
  end;
end;

procedure tmplayer.OpenTV();
var
  cmd: string;
begin
  startdebug;
  debug('TV tuner ***');
  if not checkmplayer then exit;
{$IFDEF linux}
  if ftvtunerdevice = '' then
  begin
    flasterror := lng('TVtuner device not selected');
    if assigned(fonErrorEv) then fonErrorEv(self, lng('TVtuner device not selected'));
    exit;
  end;
  if not fileexists('/dev/' + ftvtunerdevice) then
  begin
    flasterror := lng('TVtuner device not exists');
    if assigned(fonErrorEv) then fonErrorEv(self, lng('TVtuner device not exists'));
    exit;
  end;
{$ENDIF}
  stop;
  fcurrenttrack.filename := lng('TV Tuner');
  fcurrenttrack.name := lng('TV Tuner');
  flasterror := '';
  if assigned(fonConnectingEv) then fonConnectingEv(self, lng('Connecting ...'));
  fmode := __tv;
{$IFDEF linux}
  cmd := cmdline('') + ' tv:// -tv device=/dev/' + ftvtunerdevice;
{$ENDIF}
{$IFDEF mswindows}
  cmd := cmdline('') + ' tv://';
{$ENDIF}
  fhavevideostream := true;
  stop_it := false;
  debug('OpenTV() : stop_it := false');
  debug('TV tuner:' + cmd);
  flastcommandline := cmd;
  fterm.execprog(cmd);
  fprogid := fterm.prochandle;
end;

function TTVDataList.AddItem(Item: TTVData): integer;
var
  p: PTVData;
begin
  New(p);
  p^ := Item;
  Result := FTVList.Add(p);
end;

function TTVDataList.Count: integer;
begin
  Result := FTVList.Count;
end;

constructor TTVDataList.Create;
begin
  inherited Create;
  FTVList := TList.Create;
end;

destructor TTVDataList.Destroy;
begin
  FTVList.Free;
  inherited Destroy;
end;

procedure TTVDataList.Exchange(idx1, idx2: integer);
begin
  FTVList.Exchange(idx1, idx2);
end;

function TTVDataList.GetData(Idx: integer): TTVData;
begin
  FillChar(Result, SizeOf(TTVData), #0);
  Result := TTVData(FTVList.Items[Idx]^);
end;

procedure TTVDataList.SetData(IDx: integer; Data: TTvdata);
begin
  TTVData(FTVList.Items[Idx]^) := Data;
end;

procedure TTVDataList.RemoveItem(ItemIndex: integer);
begin
  dispose(Ptvdata(FTVList[ItemIndex]));

  FTVList.delete(ItemIndex);
end;

procedure tmplayer.SetChannelByfreq(const Ch_freq: integer);
begin
  if fterm.running then sendcmd('tv_set_freq ' + IntToStr(Ch_freq), true);
end;

procedure tmplayer.SetChannelByName(const Ch_name: TvCannelName);
begin
  if fterm.running then sendcmd('tv_set_channel ' + tvCannelNameStr[Ch_name], true);
end;

procedure tmplayer.SetChannelNorm(const __TVNorm: tvnorm);
begin
  if fterm.running then sendcmd('tv_set_norm ' + TvStr[__TVNorm], true);
end;

procedure tmplayer.setequalizer(const avalue: string);
begin
  fequalizer := avalue;
  if fterm.running then sendcmd('af_eq_set_bands ' + fequalizer, true);
end;

procedure tmplayer.setvolnorm(const avalue: boolean);
begin
  fvolnorm := avalue;
  if fterm.running then
    if avalue
      then sendcmd('af_add volnorm', true)
    else sendcmd('af_del volnorm', true);
end;

procedure tmplayer.setkaraoke(const avalue: boolean);
begin
  fkaraoke := avalue;
  if fterm.running then
    if avalue
      then sendcmd('af_add karaoke', true)
    else sendcmd('af_del karaoke', true);
end;

procedure tmplayer.setextrastereo(const avalue: boolean);
begin
  fextrastereo := avalue;
  if fterm.running then
    if avalue
      then sendcmd('af_add extrastereo', true)
    else sendcmd('af_del extrastereo', true);
end;

procedure tmplayer.getpreview(const source, outdir: string; const pos_: integer);
var
  cmd: string;
begin
  startdebug;
  debug('PREVIEW ***');
  stop;
  if not checkmplayer then exit;
  stop_it := false;
  debug('getpreview(const source ...) : stop_it := false');
  cmd := fmplayer + ' -nosound -vo png '
    + ' -frames 1 -ss ' + SecondsToFmtStr(pos_)
    + ' -zoom "' + source + '"';
  debug('RREVIEW: ' + cmd);
  fterm.execprog(cmd);
  fterm.waitforprocess;
end;

procedure tmplayer.on_timer(const sender: tobject);
begin
  if fmode = __webcamera then exit;
  if playnexttrack then
  begin
    debug('TIMER : Call next');
    playnexttrack := false;
    next;
    exit;
  end;

  if beginplayback then
  begin
    beginplayback := false;
    if fmode = __cd
      then
    begin
      if assigned(fonstartplayEv) then fonstartplayEv(self, fcurrenttrack.video.Width, fcurrenttrack.video.height, Lng('Audio CD is started'));
    end
    else if assigned(fonstartplayEv) then fonstartplayEv(self, fcurrenttrack.video.Width, fcurrenttrack.video.height, Lng('Playing'));
    exit;
  end;

  if pausetime > 0 then
  begin
    if pausetime = 20
      then
    begin
      debug('TIMER : try pause for caching ...');
      sendcmd('pause', true);
    end
    else
    begin
      debug('TIMER : Pause for caching, please, wait ... ' + inttostr(pausetime));
      if assigned(fonErrorEv) then fonErrorEv(self, lng('Pause for caching, please, wait ... ') + inttostr(pausetime));
    end;

    dec(pausetime);

    if pausetime = 0 then
    begin
      sendcmd('pause', true);
      fpausing := false;
    end;
    exit;
  end;

  if fterm.running and not fpausing then
  begin
    if (fmode <> __inet) and (fmode <> __rtmp) then
    begin
      sendcmd('get_time_length', fdebugplaying);
      sendcmd('get_time_pos', fdebugplaying);
    end;
    if not fhavevideostream then
    begin
      if f_getvideoresolutionappempt < 2
        then
      begin
        sendcmd('get_video_resolution (' + inttostr(f_getvideoresolutionappempt + 1) + ' of 2)', true);
        inc(f_getvideoresolutionappempt);
      end;
    end;
    if assigned(fonPlayingEv) then
    begin
      if fmode = __cd
        then
      begin
        if assigned(fonPlayingEv) then fonPlayingEv(self, fpositionsec, flengthsec, lng('Audio CD is started'));
      end
      else if assigned(fonPlayingEv) then fonPlayingEv(self, fpositionsec, flengthsec, lng('Playing'));
    end;

  end;
end;

end.
