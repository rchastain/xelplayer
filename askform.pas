
unit askform;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  mseforms,
  msesimplewidgets,
  msewidgets,
  msedataedits,
  mseedit,
  mseifiglob,
  msestrings,
  msetypes,
  msebitmap,
  msedatanodes,
  msefiledialog,
  msegrids,
  mselistbrowser,
  msesys,
  msetimer,
  msedock;

type
  taskfo = class(tmseform)
    b_yes: trichbutton;
    trichbutton2: trichbutton;
    se: tstringedit;
    ddd: tdirdropdownedit;
    l_: tlabel;
    timer_move: ttimer;
    trichbutton1: trichbutton;
    timer_resize: ttimer;
    tdockhandle1: tdockhandle;
    procedure on_move(const sender: TObject);
    procedure on_resize(const sender: TObject);
    procedure on_close(const sender: TObject);
    procedure on_onchildmouseevent(const sender: twidget; var ainfo: mouseeventinfoty);
  end;

var
  askfo: taskfo;
  moveXX, moveYY: integer;

implementation

uses
  askform_mfm;

procedure taskfo.on_move(const sender: TObject);
begin
  left := gui_getpointerpos.x - moveXX;
  top := gui_getpointerpos.y - moveYY;
end;

procedure taskfo.on_resize(const sender: TObject);
begin
  width := gui_getpointerpos.x - moveXX;
  height := gui_getpointerpos.y - moveYY;
end;

procedure taskfo.on_close(const sender: TObject);
begin
  close;
end;

procedure taskfo.on_onchildmouseevent(const sender: twidget; var ainfo: mouseeventinfoty);
begin
  if (ainfo.eventkind = EK_BUTTONPRESS) then
  begin
    bringtofront;
    if (tdockhandle1 = sender) then
    begin
      moveXX := gui_getpointerpos.x - width;
      moveYY := gui_getpointerpos.y - height;
      timer_resize.enabled := true;
    end else
      if (askfo = sender) or (l_ = sender) then
      begin
        moveXX := gui_getpointerpos.x - left;
        moveYY := gui_getpointerpos.y - top;
        timer_move.enabled := true;
      end;
  end;
  if (ainfo.eventkind = EK_BUTTONRELEASE) then
  begin
    if timer_move.enabled then timer_move.enabled := false;
    if timer_resize.enabled then timer_resize.enabled := false;
  end;
end;

end.
