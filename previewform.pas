unit previewform;
{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}
interface
uses
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  mseforms,
  mseimage,
  msesimplewidgets,
  msewidgets,
  msegraphedits,
  mseifiglob,
  msetypes,
  msesplitter,
  msetimer;
type
  tpreviewfo = class(tmseform)
    im1: timage;
    im2: timage;
    im3: timage;
    im4: timage;
    im0: timage;
    pb: tprogressbar;
    im6: timage;
    im5: timage;
    s_top: tspacer;
    l_: tlabel;
    trichbutton1: trichbutton;
    timer_move: ttimer;
    tlabel2: tlabel;
    s_full: tspacer;
    im_full: timage;
    procedure on_close(const sender: TObject);
    procedure on_move(const sender: TObject);
    procedure on_childmouseevens(const sender: twidget;
      var ainfo: mouseeventinfoty);
  end;
var
  previewfo: tpreviewfo;
  moveXX, moveYY: integer;

implementation
uses
  previewform_mfm;

procedure tpreviewfo.on_close(const sender: TObject);
begin
  close;
end;

procedure tpreviewfo.on_move(const sender: TObject);
begin
  left := gui_getpointerpos.x - moveXX;
  top := gui_getpointerpos.y - moveYY;
end;

procedure tpreviewfo.on_childmouseevens(const sender: twidget;
  var ainfo: mouseeventinfoty);
begin
  if (ainfo.eventkind = EK_BUTTONPRESS) and (ss_double in ainfo.shiftstate) then
  begin
    if (sender = s_full) or (sender = im_full)
      then s_full.visible := false
    else
      if (sender.classname = 'timage')
        then
      begin
        im_full.bitmap.clear;
        s_full.visible := true;
        im_full.update;
        s_full.update;
        im_full.bitmap := (sender as timage).bitmap;
      end;

  end
  else
    if (ainfo.eventkind = EK_BUTTONPRESS) then
    begin
      bringtofront;
      if (s_top = sender) or (previewfo = sender)
        or (l_ = sender) or (pb = sender)
        or (sender.classname = 'timage') then
      begin
        moveXX := gui_getpointerpos.x - left;
        moveYY := gui_getpointerpos.y - top;
        timer_move.enabled := true;
      end;
    end
    else
      if (ainfo.eventkind = EK_BUTTONRELEASE) then
      begin
        if timer_move.enabled then timer_move.enabled := false;
      end;
end;

end.
