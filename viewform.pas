unit viewform;
{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}
interface
uses
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  mseforms,
  msesplitter,
  msesimplewidgets,
  msewidgets,
  msetimer,
  msedock,
  msewindowwidget,
  mseimage,
  msegraphedits,
  mseifiglob,
  msetypes;
type
  tviewfo = class(tmseform)
    s_top: tspacer;
    trichbutton1: trichbutton;
    timer_move: ttimer;
    timer_resize: ttimer;
    tdockhandle1: tdockhandle;
    s_video: tspacer;
    ww_video: twindowwidget;
    be_autoclose: tbooleanedit;
    procedure on_resize(const sender: TObject);
    procedure on_move(const sender: TObject);
    procedure on_clientmouseevent(const sender: twidget;
      var ainfo: mouseeventinfoty);
    procedure on_resizeform(const sender: TObject);
    procedure on_close(const sender: TObject);
  end;
var
  viewfo: tviewfo;
  moveXX, moveYY: integer;

implementation
uses
  viewform_mfm,
  main;

procedure tviewfo.on_resize(const sender: TObject);
begin
  width := gui_getpointerpos.x - moveXX;
  height := gui_getpointerpos.y - moveYY;
end;

procedure tviewfo.on_move(const sender: TObject);
begin
  left := gui_getpointerpos.x - moveXX;
  top := gui_getpointerpos.y - moveYY;
end;

procedure tviewfo.on_clientmouseevent(const sender: twidget;
  var ainfo: mouseeventinfoty);
begin
  if (ainfo.eventkind = EK_BUTTONPRESS) then
  begin
    bringtofront;
    if (s_top = sender) or (sender = s_video) or (sender = ww_video)
      or (sender = be_autoclose) or (sender = viewfo) then
    begin
      moveXX := gui_getpointerpos.x - left;
      moveYY := gui_getpointerpos.y - top;
      timer_move.enabled := true;
    end;
    if (tdockhandle1 = sender) then
    begin
      moveXX := gui_getpointerpos.x - width;
      moveYY := gui_getpointerpos.y - height;
      timer_resize.enabled := true;
    end;
  end;
  if (ainfo.eventkind = EK_BUTTONRELEASE) then
  begin
    if timer_move.enabled then timer_move.enabled := false;
    if timer_resize.enabled then timer_resize.enabled := false;
  end;
end;

procedure tviewfo.on_resizeform(const sender: TObject);
begin
  mainfo.on_resizevideo(sender);
end;

procedure tviewfo.on_close(const sender: TObject);
begin
  close;
end;

end.
