
unit bottom;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  mseforms;

type
  tbottomfo = class(tmseform)
    procedure on_childmouseevents(const sender: twidget;
      var ainfo: mouseeventinfoty);
  end;

var
  bottomfo: tbottomfo;

implementation

uses
  bottom_mfm,
  main;

procedure tbottomfo.on_childmouseevents(const sender: twidget; var ainfo: mouseeventinfoty);
begin
  if (ainfo.eventkind = EK_BUTTONPRESS) then
  begin
    bringtofront;
    if sender = mainfo.sl_volume then
      mainfo.sl_volume.value := (gui_getpointerpos.x - mainfo.s_bottom_right.left - mainfo.sl_volume.left + mainfo.s_bottom.frame.framewidth) / mainfo.sl_volume.width
    else
      if sender = mainfo.sl_position then
        mainfo.sl_position.value := (gui_getpointerpos.x - mainfo.sl_position.left + mainfo.s_bottom.frame.framewidth) / mainfo.sl_position.width;
  end;
end;

end.
