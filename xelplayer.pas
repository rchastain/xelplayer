program xelplayer;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}
{$IFDEF FPC}
{$IFDEF mswindows}{$APPTYPE gui}{$ENDIF}
{$ENDIF}

uses
{$IFDEF FPC}{$IFDEF linux}
  cthreads,
{$ENDIF}{$ENDIF}
  msegui,
  mseforms,
  main,
  loadform,
  inifiles,
{$IFDEF linux}
  process,
  classes,
{$ENDIF}
  sysutils,
  msesysintf,
  log,
  xelutils;

{$IFDEF windows}
const
  clockfile = '/.Almin-Soft/xelplayer/xelplayer.lock';
{$ENDIF}
var
{$IFDEF linux}
  lprocess: tprocess;
  llist: tstringlist;
{$ENDIF}
  lhomedir: string;
  lfile: textfile;
  linifile: tinifile;
  luseonecopy: boolean;
  i, c: integer;

begin
  lhomedir := string(sys_getuserhomedir);
{$IFDEF windows}
  if length(lhomedir) > 0 then if lhomedir[1] = '/' then delete(lhomedir, 1, 1);
{$ENDIF}
  forcedirectories(lhomedir + '/.Almin-Soft/xelplayer');

  if paramcount > 0 then
  begin
    assignfile(lfile, lhomedir + '/.Almin-Soft/xelplayer/xelplayer.pl');
    rewrite(lfile);
    for i := 1 to paramcount do
      writeln(lfile, paramstr(i));
    closefile(lfile);
  end;

  linifile := tinifile.create(dirtowin(extractfilepath(string(sys_getapplicationpath))) + 'xelplayer15.conf');
  luseonecopy := linifile.readbool('xelplayer', 'useonecopy', true);
  linifile.free;

  if luseonecopy then
  begin
{$IFDEF linux}
    llist := tstringlist.create;
    lprocess := tprocess.create(nil);
    lprocess.executable := 'ps';
    lprocess.parameters.add('-C');
    lprocess.parameters.add('xelplayer');
    lprocess.options := lprocess.options + [powaitonexit, pousepipes];
    lprocess.execute;
    llist.loadfromstream(lprocess.output);
    c := 0;
    if llist.count > 0 then
      for i := 0 to llist.count - 1 do
        if system.pos('xelplayer', llist[i]) > 0 then
          inc(c);
    if c > 1 then
      exit;
    lprocess.free;
    llist.free;
{$ENDIF}
{$IFDEF windows}
    if fileexists(lhomedir + clockfile) then
      exit;
    assignfile(lfile, lhomedir + clockfile);
    rewrite(lfile);
    closefile(lfile);
{$ENDIF}
  end;
  loadfo := tloadfo.create(application);
  loadfo.show;
  loadfo.update;
  application.createform(tmainfo, mainfo);
  application.run;
{$IFDEF windows}
  if fileexists(lhomedir + clockfile) then
    deletefile(lhomedir + clockfile);
{$ENDIF}
end.
