
unit aboutform;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  mseforms,
  msesplitter,
  msesimplewidgets,
  msewidgets,
  msetimer;

type
  taboutfo = class(tmseform)
    s_top: tspacer;
    trichbutton1: trichbutton;
    timer_move: ttimer;
    tscrollbox1: tscrollbox;
    tlabel2: tlabel;
    tlabel3: tlabel;
    l_: tlabel;
    procedure on_close(const sender: TObject);
    procedure on_move(const sender: TObject);
    procedure on_childmouseevens(const sender: twidget; var ainfo: mouseeventinfoty);
  end;

var
  aboutfo: taboutfo;
  moveXX, moveYY: integer;

implementation

uses
  aboutform_mfm;

procedure taboutfo.on_close(const sender: TObject);
begin
  close;
end;

procedure taboutfo.on_move(const sender: TObject);
begin
  left := gui_getpointerpos.x - moveXX;
  top := gui_getpointerpos.y - moveYY;
end;

procedure taboutfo.on_childmouseevens(const sender: twidget; var ainfo: mouseeventinfoty);
begin
  if (ainfo.eventkind = EK_BUTTONPRESS) then
  begin
    bringtofront;
    if (s_top = sender)
      or (sender = aboutfo)
      or (sender = tlabel2)
      or (sender = l_) then
    begin
      moveXX := gui_getpointerpos.x - left;
      moveYY := gui_getpointerpos.y - top;
      timer_move.enabled := true;
    end;
  end;
  if (ainfo.eventkind = EK_BUTTONRELEASE) then
  begin
    if timer_move.enabled then timer_move.enabled := false;
  end;
end;

end.
