
unit playlist;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  mseforms,
  msesplitter,
  msesimplewidgets,
  msewidgets,
  msetimer,
  msedock,
  msesysdnd,
  msedrag,
  msedragglob,
  msemime,
  msearrayutils,
  msetypes;

type
  tplaylistfo = class(tdockform)
    s_top: tspacer;
    trichbutton1: trichbutton;
    timer_move: ttimer;
    timer_resize: ttimer;
    tdockhandle1: tdockhandle;
    b_showmedia: trichbutton;
    procedure on_close(const sender: TObject);
    procedure on_clientmouseevent(const sender: twidget; var ainfo: mouseeventinfoty);
    procedure on_resize(const sender: TObject);
    procedure on_move(const sender: TObject);
    procedure on_show_media(const sender: TObject);
    procedure on_resizeform(const sender: TObject);
    procedure on_beforedragdrop(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var processed: Boolean);
    procedure on_beforedragover(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var accept: Boolean; var processed: Boolean);
    procedure on_afterdragbegin(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var processed: Boolean);
  end;

var
  playlistfo: tplaylistfo;
  moveXX, moveYY: integer;

implementation

uses
  playlist_mfm,
  main,
  sysutils,
  msestrings,
  xelutils;

procedure tplaylistfo.on_close(const sender: TObject);
begin
  close;
end;

procedure tplaylistfo.on_clientmouseevent(const sender: twidget;
  var ainfo: mouseeventinfoty);
begin
  if (ainfo.eventkind = EK_BUTTONPRESS) then
  begin
    bringtofront;
    if (s_top = sender) or (mainfo.s_left = sender) then
    begin
      moveXX := gui_getpointerpos.x - left;
      moveYY := gui_getpointerpos.y - top;
      timer_move.enabled := true;
    end;
    if (tdockhandle1 = sender) then
    begin
      moveXX := gui_getpointerpos.x - width;
      moveYY := gui_getpointerpos.y - height;
      timer_resize.enabled := true;
    end;
  end;
  if (ainfo.eventkind = EK_BUTTONRELEASE) then
  begin
    if timer_move.enabled then timer_move.enabled := false;
    if timer_resize.enabled then timer_resize.enabled := false;
  end;
end;

procedure tplaylistfo.on_resize(const sender: TObject);
begin
  width := gui_getpointerpos.x - moveXX;
  height := gui_getpointerpos.y - moveYY;
end;

procedure tplaylistfo.on_move(const sender: TObject);
begin
  left := gui_getpointerpos.x - moveXX;
  top := gui_getpointerpos.y - moveYY;
end;

procedure tplaylistfo.on_show_media(const sender: TObject);
begin

  if b_showmedia.tag = 0
    then
  begin
    playlistfo.width := playlistfo.width - (playlistfo.width div 3);
    mainfo.spl1.linkright := nil;
    mainfo.s_left.visible := false;
    mainfo.spl1.visible := false;
    mainfo.s_main.visible := false;
    mainfo.s_playlist.anchors := [an_top, an_bottom];
    mainfo.s_playlist.bringtofront;
    b_showmedia.tag := 1;
  end
  else
  begin
    mainfo.s_left.visible := true;
    mainfo.spl1.visible := true;
    mainfo.s_main.visible := true;
    mainfo.s_playlist.anchors := [an_top, an_bottom, an_left, an_right];

    mainfo.spl1.linkright := mainfo.s_playlist;

    if mainfo.spl1.anchors = [an_left] then
      mainfo.spl1.left := mainfo.s_left.width
    else
      main.spl1_left := mainfo.spl1.left div playlistfo.width;

    mainfo.s_playlist.sendtoback;
    playlistfo.width := playlistfo.width + (playlistfo.width div 2);
    b_showmedia.tag := 0;
  end;

  if sender = mainfo.b_library then
  begin
    bringtofront;
    show;
  end;
end;

procedure tplaylistfo.on_resizeform(const sender: TObject);
begin
  if mainfo.s_middle.parentwidget = playlistfo.container
    then
  begin
    if mainfo.spl1.anchors = [an_right] then
      mainfo.spl1.left := round(main.spl1_left * width)
    else
      mainfo.spl1.left := mainfo.s_left.width;
  end;
end;

procedure tplaylistfo.on_beforedragdrop(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var processed: Boolean);
var
  ar1: msestringarty;
  po1: pdropfilesty;
  pc1, pc2: pchar;
  pw1, pw2: pmsechar;
  pend: pointer;
  i: integer;
  fs: tsearchrec;
  s: string;
  smse: msestring;

begin
  if adragobject is tmimedragobject then
  begin
    with tmimedragobject(adragobject) do
    begin
      case wantedformatindex of
        0:
          ar1 := breaklines(msestring(trim(data)));
        1:
          begin
            if length(data) > sizeof(dropfilesty) then
            begin
              po1 := pointer(data);
              pend := pointer(po1) + length(data);
              if po1^.fwide then
              begin
                pw1 := pointer(po1) + po1^.pfiles;
                while pw1 < pend do
                begin
                  pw2 := pw1;
                  while (pw1 < pend) and (pw1^ <> #0) do
                  begin
                    inc(pw1);
                  end;
                  additem(ar1, psubstr(pw2, pw1));
                  if pw1^ <> #0 then
                    break;
                  inc(pw1);
                  if pw1^ = #0 then
                    break;
                end;
              end else
              begin
                pc1 := pointer(po1) + po1^.pfiles;
                while pc1 < pend do
                begin
                  pc2 := pc1;
                  while (pc1 < pend) and (pc1^ <> #0) do
                  begin
                    inc(pc1);
                  end;
                  additem(ar1, psubstr(pc2, pc1));
                  if pc1^ <> #0 then
                    break;
                  inc(pc1);
                  if pc1^ <> #0 then
                    break;
                end;
              end;
            end;
          end;
      end;
      if high(ar1) > -1 then
        for i := 0 to high(ar1) do
        begin
          s := urldecode(ar1[i]);

          if system.pos('file://', s) = 1 then delete(s, 1, 7);
          if findfirst(s, faanyfile, fs) = 0 then
            repeat
              if (fs.name = '') or (fs.name = '.') or (fs.name = '..') then continue;
              if fs.Attr and faDirectory <> 0
                then mainfo.adddir(s)
              else mainfo.addfile(extractfilename(s), s, '');
            until findnext(fs) <> 0;
          findclose(fs);
        end;
    end;
    processed := true;
  end;
end;

procedure tplaylistfo.on_beforedragover(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var accept: Boolean; var processed: Boolean);
begin
  if adragobject is tmimedragobject then
  begin
    with tmimedragobject(adragobject) do
    begin
      actions := [dnda_copy];
      if checkformat(msestringarty(knownformats)) then
      begin
        try
          if (widgetatpos(apos) = mainfo.wg_playlist) or (widgetatpos(apos).parentwidget.name = 'wg_playlist') then accept := true;
          processed := true;
        except
        end;
      end;
    end;
  end;
end;

procedure tplaylistfo.on_afterdragbegin(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var processed: Boolean);
begin
  processed := true;
end;

end.
