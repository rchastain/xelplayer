unit loadform;
{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}
interface
uses
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  mseforms,
  msesimplewidgets,
  msewidgets;
type
  tloadfo = class(tmseform)
    tlabel2: tlabel;
    l_: tlabel;
    procedure on_create(const sender: TObject);
  end;
var
  loadfo: tloadfo;
implementation
uses
  loadform_mfm;

procedure tloadfo.on_create(const sender: TObject);
begin
end;

end.
