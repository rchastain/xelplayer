
unit main;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  mseforms,
  msesplitter,
  msesimplewidgets,
  msewidgets,
  msetimer,
  msedataedits,
  mseedit,
  msegrids,

  msestrings,
  msetypes,
  msewidgetgrid,

  msestream,
  msetabs,
  msegraphedits,
  sysutils,
  msebitmap,
  msedatanodes,
  msefiledialog,
  mselistbrowser,
  msesys,
  msedataimage,
  classes,
  alsmplayer,
  msewindowwidget,
  mseimage,

  inifiles,
  msefilechange,
  msedock,

  msedrag,
  msedragglob,
  msemime,
  msearrayutils,
  mp3file;

const
  knownformats: array[0..1] of msestring = ('text/uri-list', 'CF_HDROP');

type
  dropfilesty = record
    pfiles: dword;
    pt: pointty;
    fnc: longbool;
    fwide: longbool;
  end;
  pdropfilesty = ^dropfilesty;

type
  tmainfo = class(tdockform)
    s_top: tspacer;
    s_bottom: tspacer;
    s_middle: tspacer;
    button_windowstyle: trichbutton;
    b_fullscreen: trichbutton;
    s_main: tspacer;
    s_playlist: tspacer;
    spl1: tsplitter;
    timer_resize: ttimer;
    timer_move: ttimer;
    button_close: trichbutton;
    button_minimize: trichbutton;
    sl_position: tslider;
    b_volume: trichbutton;
    pb_volume: tprogressbar;
    sl_volume: tslider;
    tw_playlist: ttabwidget;
    tp_pl: ttabpage;
    ttabpage12: ttabpage;
    wg_playlist: twidgetgrid;
    se_name: tstringedit;
    se_url: tstringedit;
    se_emblem: tstringedit;
    S_TUNER: tspacer;
    st: tstringgrid;
    tbutton2: tbutton;
    tbutton6: tbutton;
    tbutton5: tbutton;
    e_tvname: tstringedit;
    e_normtv: tdropdownlistedit;
    e_freqtv: tintegeredit;
    ttabpage11: ttabpage;
    button_fastmotion: trichbutton;
    fr_panels: tframecomp;
    tspacer1: tspacer;
    tw_main: ttabwidget;
    ttabpage3: ttabpage;
    ttabpage7: ttabpage;
    dd_dir: tdirdropdownedit;
    flv: tfilelistview;
    ttabpage13: ttabpage;
    ttabpage4: ttabpage;
    tp_radio: ttabpage;
    tp_tv: ttabpage;
    tp_webcam: ttabpage;
    tp_dvd: ttabpage;
    tp_tuner: ttabpage;
    spl2: tsplitter;
    tspacer3: tspacer;
    tw_addons: ttabwidget;
    ttabpage14: ttabpage;
    tscrollbox2: tscrollbox;
    l_trackinfo: tlabel;
    trichbutton25: trichbutton;
    frame_edits: tframecomp;
    frame_buttons: tframecomp;
    wg_last: twidgetgrid;
    se_name_last: tstringedit;
    se_url_last: tstringedit;
    se_emblem_last: tstringedit;
    button_maximize: trichbutton;
    trichbutton11: trichbutton;
    trichbutton26: trichbutton;
    trichbutton12: trichbutton;
    wg_myplaylists: twidgetgrid;
    se_myplaylist: tstringedit;
    b_reloadplaylists: trichbutton;
    trichbutton13: trichbutton;
    trichbutton16: trichbutton;
    b_add_myplaylist_To_Playlist: trichbutton;
    b_open_from_myplaylists: trichbutton;
    trichbutton18: trichbutton;
    trichbutton19: trichbutton;
    trichbutton20: trichbutton;
    trichbutton21: trichbutton;
    wg_radio: twidgetgrid;
    wg_tv: twidgetgrid;
    se_name_tv: tstringedit;
    se_name_radio: tstringedit;
    di_emblem_tv: tdataimage;
    di_emblem_radio: tdataimage;
    se_site_tv: tstringedit;
    se_url_tv: tstringedit;
    se_emblem_tv: tstringedit;
    se_url_radio: tstringedit;
    se_site_radio: tstringedit;
    se_emblem_radio: tstringedit;
    trichbutton22: trichbutton;
    trichbutton24: trichbutton;
    b_edit_tv: trichbutton;
    trichbutton28: trichbutton;
    trichbutton29: trichbutton;
    b_edit_radio: trichbutton;
    b_add_from_radiolist: trichbutton;
    b_open_from_radiolist: trichbutton;
    b_add_from_tvlist: trichbutton;
    b_open_from_tvlist: trichbutton;
    s_bottom_pos: tspacer;
    s_bottom_right: tspacer;
    b_menu: trichbutton;
    pm_pl: tpopupmenu;
    FD: tfiledialog;
    s_btn_center: tspacer;
    s_btn_player: tspacer;
    b_next: trichbutton;
    b_stop: trichbutton;
    b_play: trichbutton;
    b_prev: trichbutton;
    s_btn_dvd: tspacer;
    trichbutton37: trichbutton;
    trichbutton38: trichbutton;
    trichbutton39: trichbutton;
    trichbutton40: trichbutton;
    trichbutton41: trichbutton;
    trichbutton44: trichbutton;
    trichbutton45: trichbutton;
    b_playDVD: trichbutton;
    trichbutton8: trichbutton;
    button_showsettings: trichbutton;
    edit_: tedit;
    pm_flv: tpopupmenu;
    pm_last: tpopupmenu;
    pm_mypls: tpopupmenu;
    pm_chanels: tpopupmenu;
    pm_main: tpopupmenu;
    button_screenshot: trichbutton;
    player: tmplayer;
    player2: tmplayer;
    se_cur: tstringedit;
    l_pos: tlabel;
    s_video: tspacer;
    ww_video: twindowwidget;
    b_animate: trichbutton;
    im_emblem: timage;
    s_left: tspacer;
    sb_left: tscrollbox;
    s_l_1: tspacer;
    bl3: trichbutton;
    bl2: trichbutton;
    bl1: trichbutton;
    bl0: trichbutton;
    s_l_2: tspacer;
    bl4: trichbutton;
    bl5: trichbutton;
    b_l_2: trichbutton;
    b_l_1: trichbutton;
    tspacer2: tspacer;
    trichbutton9: trichbutton;
    b_settings: trichbutton;
    ttabpage1: ttabpage;
    tw_sets: ttabwidget;
    ttabpage16: ttabpage;
    ttabpage17: ttabpage;
    ttabpage8: ttabpage;
    im_animate: timagelist;
    main_timer: ttimer;
    tspacer4: tspacer;
    b_playlist: trichbutton;
    b_library: trichbutton;
    trichbutton34: trichbutton;
    tdockhandle1: tdockhandle;
    ddd_dvdpath: tdirdropdownedit;
    ttabpage2: ttabpage;
    ddd_webcam: tdropdownlistedit;
    b_update_devices: trichbutton;
    l_tvtuner_capt: tlabel;
    l_dvd_capt: tlabel;
    l_webcam_capt: tlabel;
    f_top: tfacecomp;
    fr_form: tframecomp;
    f_scrollH: tfacecomp;
    f_scrollV: tfacecomp;
    f_buttons: tfacecomp;
    f_topbuttons: tfacecomp;
    fr_mainbuttons: tframecomp;
    f_mainbuttons: tfacecomp;
    fr_progress: tframecomp;
    pb_position: tprogressbar;
    player_preview: tmplayer;
    im_buttons: timagelist;
    s_bottom_top: tspacer;
    l_video_inf: tlabel;
    tspacer7: tspacer;
    b_add_from_flv: trichbutton;
    b_open_from_flv: trichbutton;
    trichbutton15: trichbutton;
    trichbutton14: trichbutton;
    b_reread_flv: trichbutton;
    trichbutton23: trichbutton;
    b_home: trichbutton;
    f_bottom: tfacecomp;
    fr_topbuttoms: tframecomp;
    timer_animate: ttimer;
    f_tabbutton: tfacecomp;
    b_close2: trichbutton;
    fr_bottombutton: tframecomp;
    f_bottombutton: tfacecomp;
    ddd_audioCDdevice: tdropdownlistedit;
    b_set_dvd_device: tbooleaneditradio;
    trichbutton5: trichbutton;
    tbooleaneditradio2: tbooleaneditradio;
    s_pl_top: tspacer;
    b_forward: trichbutton;
    trichbutton30: trichbutton;
    trichbutton6: trichbutton;
    b_clear: trichbutton;
    im_fileicons: timagelist;
    b_menuflv: trichbutton;
    filechangenotifyer: tfilechangenotifyer;
    wg_fl: twidgetgrid;
    se_fname: tstringedit;
    di_fl: tdataicon;
    se_furl: tstringedit;
    tlabel3: tlabel;
    player_info: tmplayer;
    fr_menu: tframecomp;
    b_add_from_lastplayed: trichbutton;
    b_l_3: trichbutton;
    s_l_3: tspacer;
    bl8: trichbutton;
    bl7: trichbutton;
    bl6: trichbutton;
    twindowwidget1: twindowwidget;
    tscrollbox1: tscrollbox;
    ber3: tbooleaneditradio;
    ber2: tbooleaneditradio;
    ber1: tbooleaneditradio;
    tlabel1: tlabel;
    trichbutton4: trichbutton;
    pb_gamma: tprogressbar;
    pb_hue: tprogressbar;
    pb_contrast: tprogressbar;
    pb_saturation: tprogressbar;
    pb_brightness: tprogressbar;
    sl_brightness: tslider;
    sl_saturation: tslider;
    sl_contrast: tslider;
    sl_HUE: tslider;
    sl_gamma: tslider;
    tscrollbox3: tscrollbox;
    pb_balance: tprogressbar;
    trichbutton71: trichbutton;
    tspacer5: tspacer;
    ber_audio0: tbooleaneditradio;
    ber_audio1: tbooleaneditradio;
    sl_balance: tslider;
    trichbutton35: trichbutton;
    sl_eq10: tslider;
    sl_eq9: tslider;
    sl_eq8: tslider;
    sl_eq7: tslider;
    sl_eq6: tslider;
    sl_eq5: tslider;
    sl_eq4: tslider;
    sl_eq3: tslider;
    sl_eq2: tslider;
    sl_eq1: tslider;
    tscrollbox4: tscrollbox;
    tbooleanedit1: tbooleanedit;
    tbooleanedit2: tbooleanedit;
    tbooleanedit3: tbooleanedit;
    tbooleanedit4: tbooleanedit;
    b_mirror: tbooleanedit;
    b_flip: tbooleanedit;
    b_rotate90: tbooleanedit;
    tlabel2: tlabel;
    bl9: trichbutton;
    tp_audiocd_pl: ttabpage;
    ddd_audioCDdevice2: tdropdownlistedit;
    trichbutton2: trichbutton;
    l_audiocd_capt: tlabel;
    tlabel5: tlabel;
    tp_audiocd: ttabpage;
    sg_cdplaylist: tstringgrid;
    s_audio_top: tspacer;
    b_repeat: trichbutton;
    procedure on_createform(const sender: TObject);
    procedure on_createdform(const sender: TObject);
    procedure on_showform(const sender: TObject);
    procedure on_resizevideo(const sender: TObject);
    procedure resizevideo(screenwidth, screenheight: integer);
    procedure on_resizeform(const sender: TObject);
    procedure on_childmouseevents(const sender: twidget; var ainfo: mouseeventinfoty);
    procedure on_show_about(const sender: TObject);
    procedure button_close_onexecute(const sender: TObject);
    procedure on_terminatequery(var terminate: Boolean);
    procedure button_minimize_onexecute(const sender: TObject);
    procedure button_maximize_onexecute(const sender: TObject);
    procedure on_timer_move(const sender: TObject);
    procedure on_timer_resize(const sender: TObject);
    procedure autocorrect_middle;
    procedure autocorrect_spl1_position(const sender: TObject);
    procedure showpanels;
    procedure button_windowstyle_onexecute(const sender: TObject);
    procedure on_fullscreen(const sender: TObject);
    procedure on_showlibrary(const sender: TObject);
    procedure on_showplaylist(const sender: TObject);
    procedure button_fastmotion_onexecute(const sender: TObject);
    procedure on_navigate_media(const sender: TObject);
    procedure on_navigate2(const sender: TObject);
    procedure button_showsettings_onexecute(const sender: TObject);
    procedure loadsettings;
    procedure savesettings;
    procedure on_hide_addons(const sender: TObject);
    procedure on_showtrackinfo(const sender: TObject);
    function show_ask(const sender: tobject; capt, fname: string; ddd_visible, se_visible, yes_visible: boolean): modalresultty;
    procedure rename_file(fold: string; updateplaylist, update_flv: boolean);
    procedure copyfiles(sl, sl2: tstringlist; copy_: boolean);
    procedure addfile(fcapt, url, emblem: string);
    procedure adddir(path: string);
    procedure on_set_directory_finav(const sender: TObject; var avalue: msestring; var accept: Boolean);
    procedure on_go_home(const sender: TObject);
    procedure on_go_up(const sender: TObject);
    procedure on_rename_file_in_flv(const sender: TObject);
    procedure on_deletefile_flv(const sender: TObject);
    procedure on_reread_flv(const sender: TObject);
    procedure on_filenavigate(const sender: tcustomlistview; const index: integer; var info: celleventinfoty);
    procedure on_addfilefromdisk(const sender: TObject);
    procedure on_add_from_lastplayed(const sender: TObject);
    procedure on_add_file_from_last_tracks(const sender: TObject; var info: celleventinfoty);
    procedure on_clear_list_of_last_played(const sender: TObject);
    procedure on_delete_from_last_played(const sender: TObject);
    procedure on_loadplaylists_list(const sender: TObject);
    procedure save_in_my_playlists(const wg: twidgetgrid; const se: tstringedit; fname: string);
    procedure on_rename_my_playlist(const sender: TObject);
    procedure on_delete_from_my_playlists(const sender: TObject);
    procedure on_add_myplaylist_To_Playlist(const sender: TObject);
    procedure on_dblclick_myplaylists(const sender: TObject; var info: celleventinfoty);
    procedure on_save_in_my_playlists(const sender: TObject);
    procedure on_rewrite_my_playlist(const sender: TObject);
    procedure on_LOADRADIOtv(const sender: TObject);
    procedure loadradiotv(fname, prev: string; wg: twidgetgrid);
    procedure on_delete_chanel(const sender: TObject);
    procedure on_add_radio_chanel(const sender: TObject);
    procedure on_edit_chanel(const sender: TObject);
    procedure on_save_chanel(const sender: TObject);
    procedure on_add_radioTVchanel_to_playlist(const sender: TObject);
    procedure on_radioTVlist_cell_event(const sender: TObject; var info: celleventinfoty);
    procedure ON_SHOW_PL_POPUP(const sender: TObject);
    procedure findcurrentruck(_restore: boolean);
    procedure on_addfiles(const sender: TObject);
    procedure on_add_url(const sender: TObject);
    procedure on_openplaylist(const sender: TObject);
    procedure on_open_directory(const sender: TObject);
    procedure on_copyfile(const sender: TObject);
    procedure on_saveinMyPlayLists(const sender: TObject);
    procedure on_save_as_radio_or_tv_chanel(const sender: TObject);
    procedure on_save_urls_to_clipboard(const sender: TObject);
    procedure on_save_as__(const sender: TObject);
    procedure on_save_files_to__(const sender: TObject);
    procedure on_movefiles_to__(const sender: TObject);
    procedure on_rename_file_in_playlist(const sender: TObject);
    procedure on_deleteformplaylist_pm(const sender: TObject);
    procedure on_topTruckinPlaylist(const sender: TObject);
    procedure on_upTrackInPlaylist(const sender: TObject);
    procedure on_downTrackInPlaylist(const sender: TObject);
    procedure on_bottomTrauckinPlaylist(const sender: TObject);
    procedure on_shuffle(const sender: TObject);
    procedure on_sort_playlist(const sender: TObject);
    procedure on_clearplaylist(const sender: TObject);
    procedure on_dblclick_on_playlist(const sender: TObject; var info: celleventinfoty);
    procedure on_changeVolumeEvent(const Sender: TObject; avolume: integer);
    procedure onConnectingEvent(const Sender: TObject; msg: AnsiString);
    procedure on_EndOfTrackEvent(const Sender: TObject; msg: AnsiString);
    procedure on_ErrorEvent(const Sender: TObject; errormsg: AnsiString);
    procedure on_GetDebugMessage(const Sender: TObject; msg: AnsiString);
    procedure on_PauseEvent(const Sender: TObject; const apausing: Boolean; msg: AnsiString);
    procedure on_PlayingEvent(const Sender: TObject; position: integer; length: integer; msg: AnsiString);
    procedure on_StartPlayEvent(const Sender: TObject; videoW: integer; videoH: integer; msg: AnsiString);
    procedure on_playingevent2(const Sender: TObject; position: integer; length: integer; msg: AnsiString);
    procedure on_startplayevent2(const Sender: TObject; videoW: integer; videoH: integer; msg: AnsiString);
    procedure on_prev(const sender: TObject);
    procedure on_play(const sender: TObject);
    procedure on_stop(const sender: TObject);
    procedure on_next(const sender: TObject);

    procedure on_load_audio_1(const sender: TObject);
    procedure on_set_audio_1(const sender: TObject);
    procedure on_set_audio_0(const sender: TObject);

    procedure on_change_position(const sender: TObject);
    procedure ON_SETPOSITION(const sender: TObject; var avalue: realty; var accept: Boolean);
    procedure ON_SETVOLUME(const sender: TObject; var avalue: realty; var accept: Boolean);
    procedure ON_MUTE(const sender: TObject);
    procedure on_showVsetform(const sender: TObject);
    procedure on_setbrightness(const sender: TObject; var avalue: realty; var accept: Boolean);
    procedure on_setsaturation(const sender: TObject; var avalue: realty; var accept: Boolean);
    procedure on_setcontrast(const sender: TObject; var avalue: realty; var accept: Boolean);
    procedure on_sethue(const sender: TObject; var avalue: realty; var accept: Boolean);
    procedure on_setgamma(const sender: TObject; var avalue: realty; var accept: Boolean);
    procedure on_setbalance(const sender: TObject; var avalue: realty; var accept: Boolean);

    procedure on_reset_video_options(const sender: TObject);
    procedure on_set_resolution(const sender: TObject);

    procedure on_set_equalizer(const sender: TObject);
    procedure on_set_equalizer_row(const sender: TObject; var avalue: realty; var accept: Boolean);
    procedure on_cancel_equalizer(const sender: TObject);
    procedure on_set_audioN(const sender: TObject; var avalue: Boolean; var accept: Boolean);
    procedure on_set_volnorm(const sender: TObject; var avalue: Boolean; var accept: Boolean);
    procedure on_set_karaoke(const sender: TObject; var avalue: Boolean; var accept: Boolean);
    procedure on_set_extrastereo(const sender: TObject; var avalue: Boolean; var accept: Boolean);
    procedure on_set_vectors(const sender: TObject; var avalue: Boolean; var accept: Boolean);
    procedure on_set_videoeffect(const sender: TObject; var avalue: Boolean; var accept: Boolean);
    procedure restart_play;
    procedure on_maintimer(const sender: TObject);
    procedure on_open_stop_dvd(const sender: TObject);
    procedure on_dvd_menu(const sender: TObject);
    procedure on_dvd_left(const sender: TObject);
    procedure on_dvd_right(const sender: TObject);
    procedure on_dvd_up(const sender: TObject);
    procedure on_dvd_down(const sender: TObject);
    procedure on_dvd_select(const sender: TObject);
    procedure on_dvdpause(const sender: TObject);
    procedure on_set_dvd_iso_path(const sender: TObject; var avalue: Boolean; var accept: Boolean);

    procedure loadtvtuner;
    procedure on_dblclickOnTVlist(const sender: TObject; var info: celleventinfoty);
    procedure on_addchanell(const sender: TObject);
    procedure on_deletechanell(const sender: TObject);
    procedure on_setchanneltv(const sender: TObject);
    procedure SaveChannel();
    procedure on_set_webcam2(const sender: TObject; var avalue: msestring; var accept: Boolean);
    procedure on_updatedivices2(const sender: TObject);
    procedure on_setplayplaylist_up_down(const sender: TObject);
    procedure on_setrepeatplaylist(const sender: TObject);
    procedure on_getpreview_pl(const sender: TObject);
    procedure getpreview(fname: string);
    procedure on_showdubble(const sender: TObject);
    procedure on_deactivateform(const sender: TObject);

    procedure loadshemas;
    procedure loadicons;
    procedure on_timer_animate(const sender: TObject);
    procedure button_screenshot_onexecute(const sender: TObject);
    procedure on_set_dvd_device(const sender: TObject; var avalue: Boolean; var accept: Boolean);
    procedure on_set_cdrom_device3(const sender: TObject; var avalue: msestring; var accept: Boolean);
    procedure on_keyup_form(const sender: twidget; var ainfo: keyeventinfoty);
    procedure on_mousewheelform(const sender: twidget; var ainfo: mousewheeleventinfoty);
    procedure on_get_icon(const sender: TObject; const ainfo: fileinfoty; var imagelist: timagelist; var imagenr: integer);
    procedure on_show_flv_popup(const sender: TObject);
    procedure on_show_flv_menu(const sender: tcustommenu);
    procedure on_open_dir_pm(const sender: TObject);

    procedure on_last_played_change(const sender: tfilechangenotifyer; const info: filechangeinfoty);
    procedure loadlast(dir: string);
    procedure on_createitemflv(const sender: tcustomitemlist; var item: tlistedititem);
    procedure on_ckeckfile(const sender: TObject; const streaminfo: dirstreaminfoty; const fileinfo: fileinfoty; var accept: Boolean);
    procedure on_listread_flv(const sender: TObject);

    procedure readlist(dir: string);
    procedure on_fl_event(const sender: TObject; var info: celleventinfoty);
    procedure on_getpreview_flv(const sender: TObject);
    procedure on_getpreview_last(const sender: TObject);
    procedure on_player_preview_startplay(const Sender: TObject; videoW: integer; videoH: integer; msg: AnsiString);
    procedure on_player_info_tartplay_event(const Sender: TObject; videoW: integer; videoH: integer; msg: AnsiString);
    procedure on_beforedragbegin_pl(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var processed: Boolean);
    procedure on_beforedragdrop_pl(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var processed: Boolean);
    procedure on_beforedragover_pl(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var accept: Boolean; var processed: Boolean);
    procedure on_dragbegin(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var processed: Boolean);
    procedure on_dragdrop(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var processed: Boolean);
    procedure on_dragover(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var accept: Boolean; var processed: Boolean);
    function get_mainwindow: tcustommseform;
    procedure on_dblclick_audiocd(const sender: TObject; var info: celleventinfoty);
    procedure on_getaudiocdplaylist(const Sender: TObject; msg: AnsiString);
  end;

var
  mainfo: tmainfo;
  _formt, _forml, _formw, _formh: integer;
  _formws, _formhs: integer;
  moveXX, moveYY: integer;
  __showsetsfofirst: boolean = true;
  __showset2fofirst: boolean = true;
  __showaboutfofirst: boolean = true;
  __showfirstresize: boolean = true;
  __continueplay: boolean = false;
  __continueplaypos: integer;
  __lastviewstyle: byte;
  apppath, conffile, playlistspath, langspath, radiotvpath, shemepath, shemeimagepath: string;
  spl1_left: real;
  _cursor: string;
  audioN: byte;
  n_animate: byte = 0;
  mainfoframewidth: integer;
  CFGChannelsName: string;
  SC: Tinifile;
  CountSelectionGrid: integer;
  B_USEONE: BOOLEAN;
  filemagic: string;
  svideoanchor: anchorsty;
  mp3f: TMP3File;

implementation

uses
  main_mfm,
  top,
  bottom,
  playlist,
  settings,
  askform,
  msefileutils,
  msesysintf,
  viewform,
  playlists,
  functions,
  chanelform,
  copyform,
  mseact,
  aboutform,
  process,
  settings2,
  msegridsglob,
  previewform,
  fastmotion,
  loadform,
  editor,
  msekeyboard,
{$IFDEF linux}

  unix,

{$ENDIF}

  mseformatpngread,
  mseformatjpgread,
  mseformatxpmread,
  fileinfo,
  mseformatbmpicoread,
  mseformatpnmread,
  mseformattgaread,

  log,
  xelutils;

procedure tmainfo.on_createform(const sender: TObject);
begin
  LLog.Add(format('tmainfo.on_createform() [player.version = %s]', [player.version]));

  __showsetsfofirst := false;

  loadfo.l_.caption := 'Loading forms...';
  loadfo.update;

  application.createform(tviewfo, viewfo);
  application.createform(ttopfo, topfo);
  application.createform(tbottomfo, bottomfo);
  application.createform(tplaylistfo, playlistfo);
  application.createform(tsetsfo, setsfo);
  application.createform(tset2fo, set2fo);
  application.createform(taskfo, askfo);
  application.createform(tchanelfo, chanelfo);
  application.createform(tcopyfo, copyfo);
  application.createform(taboutfo, aboutfo);
  application.createform(tpreviewfo, previewfo);
  application.createform(tdubblefo, dubblefo);
  application.createform(teditshemefo, editshemefo);
  application.createform(tfileinfofo, fileinfofo);

{$IFDEF linux}
  loadfo.l_.caption := 'Setting form options...';
  loadfo.update;

  playlistfo.optionswindow := [wo_popupmenu, wo_noframe, wo_notaskbar, wo_sysdnd];
  topfo.optionswindow := [wo_popup, wo_notaskbar];
  setsfo.optionswindow := [wo_popupmenu, wo_noframe, wo_notaskbar];
  set2fo.optionswindow := [wo_popup, wo_notaskbar];
  askfo.optionswindow := [wo_popup, wo_notaskbar];
  bottomfo.optionswindow := [wo_popup, wo_notaskbar];
  chanelfo.optionswindow := [wo_popup, wo_notaskbar];
  copyfo.optionswindow := [wo_popup, wo_notaskbar];
  aboutfo.optionswindow := [wo_popup, wo_notaskbar];
  previewfo.optionswindow := [wo_popup, wo_notaskbar];
  dubblefo.optionswindow := [wo_popup, wo_notaskbar];
  viewfo.optionswindow := [wo_popupmenu, wo_noframe, wo_notaskbar];
  editshemefo.optionswindow := [wo_dialog, wo_noframe, wo_notaskbar];
  fileinfofo.optionswindow := [wo_popup, wo_notaskbar];
{$ENDIF}

  loadfo.l_.caption := 'Setting component options...';
  loadfo.update;

  tw_main.height := tw_main.height + tw_main.tab_size + 2;
  tw_addons.height := tw_addons.height + tw_addons.tab_size + 2;
  tw_playlist.height := tw_playlist.height + tw_playlist.tab_size + 2;
  topfo.height := s_top.height;

{$IFDEF linux}
  _cursor := '♫';
{$ENDIF}
{$IFDEF mswindows}
  _cursor := '>';
{$ENDIF}

  button_fastmotion.parentwidget := dubblefo;
  dubblefo.width := button_fastmotion.width;
  dubblefo.height := button_fastmotion.height;
  button_fastmotion.top := 0;
  button_fastmotion.left := 0;

  tw_sets.parentwidget := set2fo.container;

  tw_sets.anchors := [an_top, an_bottom];

  loadfo.l_.caption := 'Setting configuration...';
  loadfo.update;

  player.volume := 50;
  sl_volume.value := player.volume / 100;
  pb_volume.value := player.volume / 100;

  apppath := dirtowin(extractfilepath(string(sys_getapplicationpath)));
  conffile := apppath + 'xelplayer15.conf';
  LLog.Add(format('conffile=%s', [conffile]));
  playlistspath := dirtowin(apppath + 'playlists/');
  LLog.Add(format('playlistspath=%s', [playlistspath]));
  forcedirectories(playlistspath);
  shemepath := dirtowin(apppath + 'schemas/');
  forcedirectories(shemepath);
  shemeimagepath := dirtowin(apppath + 'schemas/images/');
  langspath := dirtowin(apppath + 'langs/');
  forcedirectories(shemeimagepath);
  radiotvpath := apppath;

  CFGChannelsName := apppath + 'channelstv.ini';
  loadfo.l_.caption := 'Loading TV tuner settings...';
  loadfo.update;
  loadtvtuner;
  player.getavaliableoutputs;
  spl1_left := spl1.left / width;
end;

procedure tmainfo.on_createdform(const sender: TObject);
begin
  LLog.Add('tmainfo.on_createdform()');

  loadfo.l_.caption := 'Prepare interface...';
  loadfo.update;

  on_navigate2(b_l_3);
  on_navigate2(b_l_2);
  on_navigate_media(bl1);
  on_hide_addons(sender);
  spl1_left := spl1.left / width;
  loadshemas;
  loadicons;

  loadfo.l_.caption := 'Load settings...';
  loadfo.update;
  loadsettings;
  on_showplaylist(sender);
  setsfo.on_set_lang(setsfo.trichbutton5);
  loadfo.hide;
  mainfo.visible := true;
end;

procedure tmainfo.on_showform(const sender: TObject);
begin
  if fo_minimized in mainfo.options then
    mainfo.options := mainfo.options - [fo_minimized]
end;

procedure tmainfo.resizevideo(screenwidth, screenheight: integer);
var
  r: real;
  rect: rectty;
  i: integer;
begin
  LLog.Add(format('tmainfo.resizevideo(%d,%d)', [screenwidth, screenheight]));

  if screenheight = 0 then exit;
  if screenwidth = screenheight then
  begin
    screenwidth := 640;
    screenheight := 480;
  end;

  if player.videoeffect_rotate90 then
  begin
    i := screenwidth;
    screenwidth := screenheight;
    screenheight := i;
  end;

  mainfo.ww_video.left := 0;
  mainfo.ww_video.top := 0;

  viewfo.ww_video.left := 0;
  viewfo.ww_video.top := 0;

  if ber1.value then
    r := screenwidth / screenheight;
  if ber2.value then r := 4 / 3;
  if ber3.value then r := 16 / 9;

  if button_windowstyle.tag = 1 then
  begin
    viewfo.ww_video.height := viewfo.s_video.HEIGHT;
    viewfo.ww_video.bounds_cx := round(viewfo.ww_video.bounds_cy * r);

    if viewfo.ww_video.bounds_cx <= viewfo.s_video.bounds_cx then
      viewfo.ww_video.left := (viewfo.s_video.bounds_cx - viewfo.ww_video.bounds_cx) div 2
    else
    begin
      viewfo.ww_video.bounds_cx := viewfo.s_video.bounds_cx;
      viewfo.ww_video.bounds_cy := round(viewfo.ww_video.bounds_cx / r);
      viewfo.ww_video.top := (viewfo.s_video.bounds_cy - viewfo.ww_video.bounds_cy) div 2;
    end;

    rect.cx := viewfo.ww_video.viewport.cx;
    rect.cy := viewfo.ww_video.viewport.cy;
  end else
  begin
    mainfo.ww_video.height := mainfo.s_video.HEIGHT;
    mainfo.ww_video.bounds_cx := round(mainfo.ww_video.bounds_cy * r);

    if mainfo.ww_video.bounds_cx <= mainfo.s_video.bounds_cx
      then mainfo.ww_video.left := (mainfo.s_video.bounds_cx - mainfo.ww_video.bounds_cx) div 2
    else
    begin
      mainfo.ww_video.bounds_cx := mainfo.s_video.bounds_cx;
      mainfo.ww_video.bounds_cy := round(mainfo.ww_video.bounds_cx / r);
      mainfo.ww_video.top := (mainfo.s_video.bounds_cy - mainfo.ww_video.bounds_cy) div 2;
    end;

    rect.cx := mainfo.ww_video.viewport.cx;
    rect.cy := mainfo.ww_video.viewport.cy;
  end;

  if player.playing then
    if (not setsfo.ber_on_desktop.value) and (player.videooutput <> 'null') then
    begin
      if button_windowstyle.tag = 1 then
      begin
        viewfo.visible := true;
        viewfo.ww_video.visible := true;
      end
      else
        mainfo.ww_video.visible := true;
    end;

  if viewfo.visible then
    viewfo.ww_video.update
  else
    mainfo.ww_video.update;
end;

procedure tmainfo.on_resizevideo(const sender: TObject);
begin
  if player <> nil then
    case player.mode of
      __tv, __webcamera: resizevideo(640, 480);
    else
      resizevideo(player.currenttrack.video.width, player.currenttrack.video.height);
    end;
end;

procedure tmainfo.on_resizeform(const sender: TObject);
begin
  on_showdubble(sender);
  s_btn_center.left := width div 2 - s_btn_center.width div 2;
  if __showfirstresize then
    __showfirstresize := false
  else
  begin
    if spl1.anchors = [an_right] then
      spl1.left := round(spl1_left * width)
    else
      spl1.left := s_left.width;
  end;
end;

procedure tmainfo.on_childmouseevents(const sender: twidget; var ainfo: mouseeventinfoty);
begin
  if b_fullscreen.tag = 1 then
    if ainfo.eventkind = EK_MOUSEMOVE then
      case b_library.tag of
        0:
          if ((ainfo.pos.y > bottomfo.TOP - mainfo.top) or (ainfo.pos.y < topfo.height)) then
          begin
            if b_playlist.tag = 0 then
              if (sender <> s_video) and (mainfo.ww_video <> sender)
                and (sender <> mainfo.im_emblem) and (sender <> mainfo.b_animate) then exit;
            showpanels;
          end else
          begin
            bottomfo.VISIBLE := FALSE;
            topfo.VISIBLE := FALSE;
          end;
        1: showpanels;
      end;

  if (ainfo.eventkind = EK_BUTTONPRESS) and (ss_double in ainfo.shiftstate) then
  begin
    if (s_top = sender) then if button_maximize.enabled then button_maximize_onexecute(sender);
    if (s_video = sender) or (mainfo.ww_video = sender) or (mainfo.b_animate = sender) or (mainfo.im_emblem = sender) then
    begin
      if b_fullscreen.tag = 0 then
      begin
        on_fullscreen(sender);
        if b_playlist.tag = 0 then on_showplaylist(sender);
      end
      else if b_library.tag = 1 then on_showlibrary(sender)
      else on_fullscreen(sender);
    end;
  end
  else
    if (ainfo.eventkind = EK_BUTTONPRESS) then
    begin
      if sender = sl_volume then
        sl_volume.value := (gui_getpointerpos.x - left - s_bottom_right.left - sl_volume.left - mainfoframewidth - mainfo.s_bottom.frame.framewidth) / sl_volume.width
      else
        if sender = sl_position then
          sl_position.value := (gui_getpointerpos.x - left - sl_position.left - mainfoframewidth - mainfo.s_bottom.frame.framewidth) / sl_position.width
        else
          if (mainfo.ww_video = sender) or (l_video_inf = sender) or (sender.classname = 'tspacer') or (mainfo.b_animate = sender) or (mainfo.im_emblem = sender) then
          begin
            moveXX := gui_getpointerpos.x - left;
            moveYY := gui_getpointerpos.y - top;
            timer_move.enabled := true;
          end
          else
            if (tdockhandle1 = sender) then
            begin
              moveXX := gui_getpointerpos.x - width;
              moveYY := gui_getpointerpos.y - height;
              timer_resize.enabled := true;
            end;
    end;
  if (ainfo.eventkind = EK_BUTTONRELEASE) then
  begin
    if timer_move.enabled then timer_move.enabled := false;
    if timer_resize.enabled then timer_resize.enabled := false;
  end;
end;

procedure tmainfo.on_show_about(const sender: TObject);
begin
  if __showaboutfofirst then
  begin
    __showaboutfofirst := false;
    aboutfo.left := mainfo.left + (mainfo.width - aboutfo.width) div 2;
    aboutfo.top := mainfo.top + (mainfo.height - aboutfo.height) div 2;
  end;
  aboutfo.bringtofront;
  aboutfo.show;
end;

procedure tmainfo.button_close_onexecute(const sender: TObject);
begin
  application.terminate;
end;

procedure tmainfo.on_terminatequery(var terminate: Boolean);
begin
  if setsfo.be_ask_forexit.value then
    if show_ask(mainfo, 'Quit XelPlayer?', '', false, false, true) <> MR_OK then
    begin
      terminate := false;
      exit;
    end;
  if button_windowstyle.tag = 1 then
    player.sendcommandtomplayer('quit')
  else
    player.stop;
  savesettings;
end;

procedure tmainfo.button_minimize_onexecute(const sender: TObject);
begin
  if fo_minimized in mainfo.options = false then
    mainfo.options := mainfo.options + [fo_minimized];
  dubblefo.visible := false;
  if b_fullscreen.tag = 1 then
  begin
    bottomfo.visible := false;
    topfo.visible := false;
  end;
end;

procedure tmainfo.button_maximize_onexecute(const sender: TObject);
begin
  if b_fullscreen.tag = 1 then
  begin
    on_fullscreen(sender);
    exit;
  end;
  s_btn_center.left := 0;

  if fo_maximized in mainfo.options then
  begin
    mainfo.options := mainfo.options - [fo_maximized];
    tdockhandle1.visible := true;
  end else
  begin
    mainfo.options := mainfo.options + [fo_maximized];
    tdockhandle1.visible := false;
  end;
  application.processmessages;
  on_resizevideo(sender);
end;

procedure tmainfo.on_timer_move(const sender: TObject);
begin
  left := gui_getpointerpos.x - moveXX;
  top := gui_getpointerpos.y - moveYY;
  on_showdubble(sender);
end;

procedure tmainfo.on_timer_resize(const sender: TObject);
begin
  width := gui_getpointerpos.x - moveXX;
  height := gui_getpointerpos.y - moveYY;
end;

procedure tmainfo.autocorrect_middle;
begin
  s_main.sendtoback;
  if (b_fullscreen.tag = 1) and (b_library.tag = 0) then
    s_middle.anchors := []
  else
  begin
    s_middle.anchors := [an_top, an_bottom];
    s_middle.top := s_top.height + mainfoframewidth;
    s_middle.height := mainfo.height - s_top.height - s_bottom.height - mainfoframewidth * 2;
  end;
end;

procedure tmainfo.autocorrect_spl1_position(const sender: TObject);
begin
  if s_main.width > 1 then
  begin
    if s_middle.parentwidget = mainfo.container then
    begin
      spl1_left := spl1.left / width;
      on_resizevideo(sender);
    end else
    begin
      spl1_left := spl1.left / playlistfo.width;
    end;
  end;
end;

procedure tmainfo.showpanels;
begin
  topfo.width := mainfo.width;
  topfo.top := mainfo.top;
  topfo.left := mainfo.left;
  if b_fullscreen.tag = 1 then topfo.visible := true;
  topfo.bringtofront;

  bottomfo.width := topfo.width;
  bottomfo.top := mainfo.top + mainfo.height - bottomfo.height;
  bottomfo.left := mainfo.left;
  if b_fullscreen.tag = 1 then bottomfo.VISIBLE := TRUE;
  bottomfo.bringtofront;

  if b_fullscreen.tag = 0 then
  begin
    bottomfo.visible := false;
    topfo.visible := false;
  end;
end;

procedure tmainfo.button_windowstyle_onexecute(const sender: TObject);
var
  bb: boolean;
  r: real;
begin
  LLog.Add(format('tmainfo.on_changewindowstyle(%s) button_windowstyle.tag=%d', [sender.classname, button_windowstyle.tag]));
  bb := false;
  if (player.playing) and (player.havevideostream) then
  begin
    bb := true;
    __continueplay := player.pausing;
    __continueplaypos := player.position;
    r := sl_position.value;
    on_stop(sender);
  end;

  autocorrect_spl1_position(sender);

  case button_windowstyle.tag of
    0:
      begin
        _formws := mainfo.width;
        _formhs := mainfo.height;
        __lastviewstyle := b_playlist.tag;
        if fo_maximized in mainfo.options then mainfo.options := mainfo.options - [fo_maximized];
        if b_library.tag = 0 then on_showlibrary(sender);
        s_middle.parentwidget := playlistfo.container;
        s_middle.height := playlistfo.height - playlistfo.s_top.height - mainfoframewidth * 2;
        s_btn_center.visible := false;
        mainfo.bounds_cymin := s_bottom.height + mainfoframewidth * 2;
        mainfo.height := s_bottom.height + mainfoframewidth * 2;
        s_top.visible := false;
        mainfo.bounds_cxmin := 500;
        mainfo.width := 500;
        s_btn_center.visible := true;
        playlistfo.top := mainfo.top + mainfo.height + mainfoframewidth * 2;
        playlistfo.width := mainfo.width + (mainfo.width div 2);
        playlistfo.height := 300;
        playlistfo.left := mainfo.left;
        playlistfo.tdockhandle1.bringtofront;
        s_bottom.bringtofront;
        tw_playlist.left := tw_playlist.left + 3;
        tw_playlist.height := tw_playlist.height + 2;
        b_library.onexecute := @playlistfo.on_show_media;
        playlistfo.on_show_media(sender);
        button_maximize.enabled := false;
        tdockhandle1.visible := false;
        b_close2.visible := true;
        button_windowstyle.tag := 1;
        im_emblem.parentwidget := viewfo.s_video;
        b_animate.parentwidget := viewfo.s_video;

        s_video.height := 0;
        s_left.height := s_middle.height;
        player.display := viewfo.ww_video;
        viewfo.top := mainfo.top + mainfo.height + mainfoframewidth * 2;
        viewfo.left := mainfo.left;
      end;
    1:
      begin
        if playlistfo.b_showmedia.tag = 1 then
          playlistfo.on_show_media(sender);
        s_middle.parentwidget := mainfo.container;
        mainfo.height := _formhs;
        mainfo.bounds_cymin := 350;
        autocorrect_middle;
        playlistfo.visible := false;
        b_library.onexecute := @on_showlibrary;
        tdockhandle1.visible := true;
        button_maximize.enabled := true;
        s_top.visible := true;
        b_close2.visible := false;
        tw_playlist.left := tw_playlist.left - 3;
        tw_playlist.height := tw_playlist.height - 2;

        button_windowstyle.tag := 0;
        mainfo.width := _formws;

        if mainfo.spl1.anchors = [an_right] then
          mainfo.spl1.left := round(main.spl1_left * width)
        else
          mainfo.spl1.left := mainfo.s_left.width;

        viewfo.visible := false;

        s_video.height := s_left.width div 2;
        s_left.top := s_video.height;
        s_left.height := s_middle.height - s_video.height;
        im_emblem.parentwidget := mainfo.s_video;
        b_animate.parentwidget := mainfo.s_video;

        player.display := ww_video;
        application.processmessages;
        if __lastviewstyle = 1 then on_showplaylist(sender);
      end;
  end;
  on_showdubble(sender);

  if bb then
  begin
    sl_position.value := r;
    if not __continueplay then
    begin
      if tw_main.activepage = tp_dvd then
        on_open_stop_dvd(sender)
      else
        if tw_main.activepage = tp_tuner then
        begin
          player.OpenTV();
          __continueplay := false;
        end else
          if tw_main.activepage = tp_webcam then
          begin
            player.openwebcamera;
            __continueplay := false;
          end else
            player.play(__continueplaypos);
    end;
  end;
end;

procedure tmainfo.on_fullscreen(const sender: TObject);
begin
  LLog.Add(format('tmainfo.on_fullscreen(%s) button_windowstyle.tag=%d', [sender.classname, button_windowstyle.tag]));

  if button_windowstyle.tag = 1 then
  begin
    player.sendcommandtomplayer('vo_fullscreen');
    mainfo.bringtofront;
    exit;
  end;

  case b_fullscreen.tag of
    0:
      begin
{$IFDEF mswindows}
        _formt := top;
        _forml := left;
        _formh := height;
        _formw := width;
{$ENDIF}

        mainfo.frame.framewidth := 0;
        mainfo.invalidate;
        mainfo.options := mainfo.options + [fo_fullscreen] - [fo_defaultpos];
        application.processmessages;
        mainfo.update;

        topfo.left := mainfo.left;
        topfo.top := 0;
        topfo.width := mainfo.width;
        s_top.parentwidget := topfo;

        bottomfo.height := s_bottom.height + mainfoframewidth * 2;
        bottomfo.left := mainfo.left;
        bottomfo.width := mainfo.width;
        bottomfo.top := mainfo.height - bottomfo.height;
        s_bottom.parentwidget := bottomfo;
        s_bottom.top := mainfoframewidth;
        tdockhandle1.visible := false;
        button_windowstyle.enabled := false;
        b_fullscreen.tag := 1;
        if b_library.tag = 1 then
        begin
          bottomfo.visible := true;
          topfo.visible := true;
        end;
        b_fullscreen.imagenr := 7;
        button_minimize.enabled := false;
      end;
    1:
      begin
        s_btn_center.left := 0;
        mainfo.invalidate;
        mainfo.options := mainfo.options - [fo_fullscreen] + [fo_defaultpos];
{$IFDEF mswindows}
        height := _formh;
        top := _formt;
        left := _forml;
        width := _formw;
{$ENDIF}
        application.processmessages;
        mainfo.update;
        mainfo.frame.framewidth := mainfoframewidth;
        s_top.parentwidget := mainfo.container;
        s_top.top := 0;
        s_bottom.parentwidget := mainfo.container;
        s_bottom.top := height - s_bottom.height - mainfoframewidth;
        tdockhandle1.visible := true;
        button_windowstyle.enabled := true;
        bottomfo.visible := false;
        topfo.visible := false;
        b_fullscreen.tag := 0;
        b_fullscreen.imagenr := 6;
        button_minimize.enabled := true;
      end;
  end;
  autocorrect_middle;
end;

procedure tmainfo.on_showlibrary(const sender: TObject);
begin
  LLog.Add(format('tmainfo.on_showlibrary(%s) button_windowstyle.tag=%d', [sender.classname, button_windowstyle.tag]));

  if button_windowstyle.tag = 1 then
  begin
    playlistfo.visible := true;
    playlistfo.bringtofront;
    playlistfo.on_show_media(sender);
    exit;
  end else
    case b_library.tag of
      1: on_showplaylist(sender);
      0:
        begin
          b_library.tag := 1;
          b_playlist.tag := 0;

          if spl1.anchors = [an_left] then spl1.left := s_left.width;

          spl1.linkleft := s_main;
          s_video.width := s_left.width;
          s_video.height := s_video.width div 2;
          s_video.anchors := [an_left, an_top];
          if spl1.left < s_video.width then spl1.left := s_left.width + (width - s_left.width) div 2;
          if b_fullscreen.tag = 1 then
          begin
            autocorrect_middle;
            showpanels;
          end;
          on_resizevideo(sender);
          s_left.visible := true;
          s_main.visible := true;
          s_playlist.visible := true;
          spl1.visible := true;
        end;
    end;
  try
    on_showdubble(sender);
    s_video.setfocus;
  except
  end;
end;

procedure tmainfo.on_showplaylist(const sender: TObject);
begin
  LLog.Add(format('tmainfo.on_showplaylist(%s) button_windowstyle.tag=%d', [sender.classname, button_windowstyle.tag]));

  b_library.tag := 0;
  if button_windowstyle.tag = 1 then
    playlistfo.visible := not playlistfo.visible
  else
    case b_playlist.tag of
      0:
        begin
          spl1.linkleft := nil;
          s_video.anchors := [];
          b_playlist.tag := 1;
          s_left.visible := false;
          s_main.visible := false;
          s_playlist.visible := false;
          spl1.visible := false;
        end;
      1:
        begin
          s_video.anchors := [an_left, an_right];
          spl1.linkleft := s_video;
          b_playlist.tag := 0;
          s_left.visible := false;
          s_main.visible := false;
          s_playlist.visible := true;
          spl1.visible := true;
        end;
    end;
  if b_fullscreen.tag = 1 then
  begin
    autocorrect_middle;
    showpanels;
  end;
  on_resizevideo(sender);
  on_showdubble(sender);
  try
    s_video.setfocus;
  except
  end;
end;

procedure tmainfo.button_fastmotion_onexecute(const sender: TObject);
begin
  case button_fastmotion.tag of
    0:
      begin
        s_left.width := s_left.width * 2;
        button_fastmotion.tag := 1;
        if sender = button_fastmotion then
          setsfo.b_dubblevideowimdow.value := true;
      end;
    1:
      begin
        s_left.width := s_left.width div 2;
        button_fastmotion.tag := 0;
        setsfo.b_dubblevideowimdow.value := false;
      end;
  end;

  s_video.width := s_left.width;
  s_video.height := s_left.width div 2;
  s_left.height := s_middle.height - s_video.height;
  sb_left.left := (s_left.width - sb_left.width) div 2;
  if spl1.anchors = [an_right] then
    spl1.left := s_left.width + (width - s_left.width) div 2
  else
    spl1.left := s_left.width;
  spl1.update;
  spl1_left := spl1.left / width;
  on_resizevideo(sender);
  on_showdubble(sender);
end;

procedure tmainfo.on_navigate_media(const sender: TObject);
var
  i, y: integer;
begin
  if (sender.classname = 'tmenuitem') then
    i := (sender as tmenuitem).tag
  else
    i := (sender as trichbutton).tag;
  case i of
    0..5:
      begin
        tw_playlist.activepageindex := 0;
        b_repeat.parentwidget := s_pl_top;
        b_forward.parentwidget := s_pl_top;
      end;
    6: tw_playlist.activepageindex := 3;
    8: tw_playlist.activepageindex := 1;
    7: tw_playlist.activepageindex := 2;
    9:
      begin
        tw_playlist.activepageindex := 4;
        b_repeat.parentwidget := s_audio_top;
        b_forward.parentwidget := s_audio_top;
      end;
  else
    tw_playlist.activepageindex := 5;
  end;

  case i of
    6:
      begin
        s_bottom_pos.visible := false;
        s_bottom_right.visible := false;
      end;
    8:
      begin
        s_bottom_pos.visible := false;
        s_bottom_right.visible := true;
      end;
  else
    begin
      s_bottom_pos.visible := true;
      s_bottom_right.visible := true;
    end;
  end;

  case i of
    6, 8:
      begin
        b_prev.visible := false;
        b_next.visible := false;
        b_repeat.visible := false;
      end;
  else
    begin
      b_prev.visible := true;
      b_next.visible := true;
      b_repeat.visible := true;
    end;
  end;

  case i of
    7:
      begin
        s_btn_player.visible := false;
        s_btn_dvd.visible := true;
      end;
  else
    begin
      s_btn_player.visible := true;
      s_btn_dvd.visible := false;
    end;
  end;

  case i of
    6, 7, 8, 9:
      begin
        spl1.anchors := [an_left];
        s_main.bounds_cxmax := 1;
        s_main.bounds_cxmin := 1;
        spl1.left := s_left.width;
      end;
  else
    begin
      spl1.anchors := [an_right];
      s_main.bounds_cxmax := 0;
      s_main.bounds_cxmin := 0;
      if s_middle.parentwidget = mainfo.container
        then
      begin
        spl1.left := round(spl1_left * width);
      end
      else
      begin
        spl1.left := round(spl1_left * playlistfo.width);
      end;
    end;
  end;

  for y := 0 to 9 do
  begin
    (findcomponent('bl' + inttostr(y)) as trichbutton).frame.template := frame_buttons;
    if i = y then
      (findcomponent('bl' + inttostr(y)) as trichbutton).frame.template := frame_edits;
  end;

  tw_main.activepageindex := i;
end;

procedure tmainfo.on_navigate2(const sender: TObject);
var
  s: string;
begin
  s := (sender as trichbutton).name;
  delete(s, 1, 4);
  if (findcomponent('s_l_' + s) as tspacer).height = 5 then
    (findcomponent('s_l_' + s) as tspacer).height := (sender as trichbutton).tag
  else
    (findcomponent('s_l_' + s) as tspacer).height := 5;
end;

procedure tmainfo.button_showsettings_onexecute(const sender: TObject);
begin
  if not __showsetsfofirst then
  begin
    __showsetsfofirst := true;
    setsfo.left := mainfo.left + (mainfo.width - setsfo.width) div 2;
    setsfo.top := mainfo.top + (mainfo.height - setsfo.height) div 2;
  end;
  dubblefo.visible := false;
  set2fo.bringtofront;
  if setsfo.visible then
  begin
    setsfo.visible := false;
    application.processmessages;
    mainfo.activate;
  end else
  begin
    setsfo.bringtofront;
    setsfo.show;
  end;
end;

procedure tmainfo.loadsettings;
var
  lstr: msestring;
  fi: tinifile;
  b: boolean;
  fs: tsearchrec;
{$IFDEF windows}
  i: integer;
{$ENDIF}
begin
  LLog.Add('tmainfo.loadsettings');
  with setsfo do
  begin
    lstr := '128'; edit_mplayercache.dropdown.cols.addrow(lstr);
    lstr := '256'; edit_mplayercache.dropdown.cols.addrow(lstr);
    lstr := '512'; edit_mplayercache.dropdown.cols.addrow(lstr);
    lstr := '1024'; edit_mplayercache.dropdown.cols.addrow(lstr);
    lstr := '2048'; edit_mplayercache.dropdown.cols.addrow(lstr);
    lstr := '4096'; edit_mplayercache.dropdown.cols.addrow(lstr);
    lstr := '6144'; edit_mplayercache.dropdown.cols.addrow(lstr);
  end;

  fi := tinifile.create(conffile);

{$IFDEF linux}
  setsfo.be_showonlymediafiles.value := fi.readbool('xelplayer', 'showonlymediafiles', true);
  setsfo.edit_magic.value := fi.readstring('xelplayer', 'MagicFile', '/usr/share/file/misc/magic.mgc');
  if not fileexists(filemagic) then setsfo.be_showonlymediafiles.value := false;
{$ENDIF}
{$IFDEF windows}
  setsfo.be_showonlymediafiles.value := false;
  setsfo.be_showonlymediafiles.enabled := false;
  setsfo.fne_magic.enabled := false;
{$ENDIF}

  lstr := dirtowin(fi.readstring('xelplayer', 'FileNavigationFromDirectory', ''));
  if (lstr = '') or (not directoryexists(lstr)) then lstr := apppath;

  setsfo.edit_navigatorhome.VALUE := lstr;
  DD_DIR.VALUE := lstr;

  mainfo.wg_fl.visible := setsfo.be_showonlymediafiles.value;
  mainfo.flv.visible := not setsfo.be_showonlymediafiles.value;

  if setsfo.be_showonlymediafiles.value then
    readlist(string(lstr))
  else
  begin
    flv.directory := lstr;
    flv.readlist;
  end;

  lstr := dirtowin(fi.readstring('xelplayer', 'PlaylistsDirectory', ''));
  if (lstr = '') or (not directoryexists(lstr)) then lstr := playlistspath;
  setsfo.edit_playlistsdir.VALUE := lstr;

  setsfo.b_dubblevideowimdow.value := fi.readbool('xelplayer', 'DubbleVideoWindow', false);
  if setsfo.b_dubblevideowimdow.value then
  begin
    button_fastmotion.tag := 0;
    button_fastmotion_onexecute(setsfo.b_dubblevideowimdow);
  end;

  lstr := dirtowin(fi.readstring('xelplayer', 'ScreenshotsDirectory', ''));
  if (lstr = '') or (not directoryexists(lstr)) then lstr := apppath;

  setsfo.edit_scheme.value := fi.readstring('xelplayer', 'sheme', 'default');
  setsfo.on_load_sheme(setsfo.trichbutton7);

  setsfo.edit_icons.value := fi.readstring('xelplayer', 'icons', 'default');
  setsfo.on_load_icons(setsfo.trichbutton7);

  setsfo.be_nexttrackallow.value := fi.readbool('xelplayer', 'NextTrackAllow', b);
  player.PlayNextTrackAllow := setsfo.be_nexttrackallow.value;

  setsfo.be_ask_forexit.value := fi.readbool('xelplayer', 'AskForExit', true);

  setsfo.be_showplayling_debug.value := fi.readbool('xelplayer', 'ShowPlayling', true);
  player.debug_showplaying := setsfo.be_showplayling_debug.value;

  setsfo.be_writetoconsole.value := fi.readbool('xelplayer', 'WriteToConsole', true);
  player.debug_writeouttoconsole := setsfo.be_writetoconsole.value;

  setsfo.be_writetoconsolewithmplayeroutput.value := fi.readbool('xelplayer', 'WriteToConsoleWithMplayerOutput', true);
  player.debug_AddMplayerOutputToDebug := setsfo.be_writetoconsolewithmplayeroutput.value;

  setsfo.ber_on_desktop.value := fi.readbool('xelplayer', 'VideoToOtherWidget', false);
  setsfo.se_videoID.value := fi.readstring('xelplayer', 'OtherDesktopID', '0');
  player.videotoanotherdesktop := setsfo.ber_on_desktop.value;
  player.device_CastomDisplayID := string(setsfo.se_videoID.value);

  setsfo.be_allowloademblems.value := fi.readbool('xelplayer', 'AllowLoadRadioAndTVEmblems', true);
  if not setsfo.be_allowloademblems.value then
    setsfo.on_setallowemblems_change(setsfo.be_allowloademblems);

  setsfo.be_useonecopy.value := fi.readbool('xelplayer', 'useonecopy', true);
  if not setsfo.be_useonecopy.value then b_useone := TRUE else b_useone := false;

  b_repeat.tag := fi.readinteger('xelplayer', 'repeatplaylist', 0);
  b_forward.tag := fi.readinteger('xelplayer', 'forwardplaylist', 0);

  button_windowstyle.tag := fi.readinteger('xelplayer', 'style', 0);
  if button_windowstyle.tag = 1 then
  begin
    button_windowstyle.tag := 0;
    button_windowstyle_onexecute(button_windowstyle);
  end;

  lstr := fi.readstring('mplayer', 'Path', '');
  setsfo.edit_mplayerpath.value := lstr;
  setsfo.on_set_mplayer(setsfo.edit_mplayerpath, lstr, b);

  setsfo.edit_mplayercache.value := fi.readstring('mplayer', 'Cache', '');
  player.cache := string(setsfo.edit_mplayercache.value);

  setsfo.be_usefifo.value := fi.readbool('mplayer', 'usefifo', false);
  player.usefifo := setsfo.be_usefifo.value;

  setsfo.edit_audiooutput.value := fi.readstring('mplayer', 'AudioOutput', 'default');
  setsfo.edit_videooutput.value := fi.readstring('mplayer', 'VideoOutput', 'default');
  setsfo.be_aval_ao.value := fi.readbool('mplayer', 'ShowAvalAudioVideoOutputOnly', false);
  lstr := setsfo.edit_videooutput.value; setsfo.on_setvideooutput(setsfo.edit_videooutput, lstr, b);
  lstr := setsfo.edit_audiooutput.value; setsfo.on_setaudioOutput(setsfo.edit_audiooutput, lstr, b);
  b := setsfo.be_aval_ao.value; setsfo.on_aval_audio_only(setsfo.be_aval_ao, b, b);

  setsfo.se_webcam_preload.value := fi.readstring('mplayer', 'WebcamPreload', '');
  setsfo.se_additional_video_params.value := fi.readstring('mplayer', 'AditionalVideoParams', '');
  setsfo.se_additional_params.value := fi.readstring('mplayer', 'AditionalParams', '');
  setsfo.be_indexing.value := fi.readbool('mplayer', 'indexing', false);
  player.indexing := setsfo.be_indexing.value;
  setsfo.be_doublebuf.value := fi.readbool('mplayer', 'doublebuffering', false);
  player.doublebuf := setsfo.be_doublebuf.value;

  setsfo.be_noscreensaver.value := fi.readbool('mplayer', 'disablescreensaver', true);
  player.disablescreensaver := setsfo.be_noscreensaver.value;

  player.volume := fi.readinteger('xelplayer', 'volume', 50);

  setsfo.edit_webcam.value := fi.readstring('Devices', 'Webcamera', '');
  ddd_webcam.value := fi.readstring('Devices', 'Webcamera', '');
  setsfo.edit_tvtuner.value := fi.readstring('Devices', 'TVTuner', '');
  setsfo.edit_cdrom.value := fi.readstring('Devices', 'CDROM', '');
  ddd_audioCDdevice2.value := fi.readstring('Devices', 'CDROM', '');
  player.device_webcamera := setsfo.edit_webcam.value;
  player.device_tvtuner := setsfo.edit_tvtuner.value;
  player.device_cdrom := setsfo.edit_cdrom.value;
  setsfo.on_update_devices(setsfo.b_update_devices);

  loadfo.l_.caption := 'Set scheme...'; loadfo.update;

  setsfo.on_load_sheme(setsfo.trichbutton7);

  if b_repeat.tag = 1 then
  begin
    player.repeatplaylist := TRUE;
    b_repeat.imagenr := 27;
  end;

  if b_forward.tag = 1 then
  begin
    player.playplaylistback := TRUE;
    b_forward.imagenr := 26;
  end;

  loadfo.l_.caption := 'Set language...';
  loadfo.update;

  setsfo.edit_language.dropdown.cols.clear;
  if findfirst(langspath + '*.lang', faanyfile, fs) = 0 then
    repeat
      setsfo.edit_language.dropdown.cols.addrow(msestring(fs.name));
    until findnext(fs) <> 0;
  findclose(fs);
  setsfo.edit_language.value := fi.readstring('xelplayer', 'language', 'en.lang');

  fi.free;
  if not fileexists(conffile) then
    show_ask(mainfo, 'Configuration file not found!'#10'Use defaults settings.', '', false, false, false);
end;

procedure tmainfo.savesettings;
var
  fi: tinifile;
begin
  LLog.Add('tmainfo.savesettings');
  try
    fi := tinifile.create(conffile);

    fi.writestring('xelplayer', 'FileNavigationFromDirectory', setsfo.edit_navigatorhome.VALUE);
    fi.writestring('xelplayer', 'PlaylistsDirectory', setsfo.edit_playlistsdir.VALUE);
    fi.writebool('xelplayer', 'DubbleVideoWindow', setsfo.b_dubblevideowimdow.value);

    fi.writebool('xelplayer', 'NextTrackAllow', setsfo.be_nexttrackallow.value);
    fi.writebool('xelplayer', 'AskForExit', setsfo.be_ask_forexit.value);
    fi.writebool('xelplayer', 'WriteToConsole', setsfo.be_writetoconsole.value);
    fi.writebool('xelplayer', 'WriteToConsoleWithMplayerOutput', setsfo.be_writetoconsolewithmplayeroutput.value);
    fi.writebool('xelplayer', 'ShowPlayling', setsfo.be_showplayling_debug.value);
    fi.writebool('xelplayer', 'VideoToOtherWidget', setsfo.ber_on_desktop.value);
    fi.writestring('xelplayer', 'OtherDesktopID', setsfo.se_videoID.value);
    fi.writestring('xelplayer', 'sheme', setsfo.edit_scheme.value);
    fi.writestring('xelplayer', 'icons', setsfo.edit_icons.value);
    fi.writestring('xelplayer', 'language', setsfo.edit_language.value);
    fi.writebool('xelplayer', 'AllowLoadRadioAndTVEmblems', setsfo.be_allowloademblems.value);
    fi.writebool('xelplayer', 'useonecopy', setsfo.be_useonecopy.value);
    fi.writebool('xelplayer', 'showonlymediafiles', setsfo.be_showonlymediafiles.value);
    fi.writestring('xelplayer', 'MagicFile', setsfo.edit_magic.value);
    fi.writeinteger('xelplayer', 'repeatplaylist', b_repeat.tag);
    fi.writeinteger('xelplayer', 'forwardplaylist', b_forward.tag);
    fi.writeinteger('xelplayer', 'style', button_windowstyle.tag);
    fi.writeinteger('xelplayer', 'volume', player.volume);

    fi.writestring('mplayer', 'Path', setsfo.edit_mplayerpath.value);
    fi.writestring('mplayer', 'Cache', setsfo.edit_mplayercache.value);
    fi.writestring('mplayer', 'AudioOutput', setsfo.edit_audiooutput.value);
    fi.writestring('mplayer', 'VideoOutput', setsfo.edit_videooutput.value);
    fi.writebool('mplayer', 'ShowAvalAudioVideoOutputOnly', setsfo.be_aval_ao.value);
    fi.writebool('mplayer', 'usefifo', setsfo.be_usefifo.value);
    fi.writestring('mplayer', 'WebcamPreload', setsfo.se_webcam_preload.value);
    fi.writestring('mplayer', 'AditionalVideoParams', setsfo.se_additional_video_params.value);
    fi.writestring('mplayer', 'AditionalParams', setsfo.se_additional_params.value);
    fi.writebool('mplayer', 'Indexing', setsfo.be_indexing.value);
    fi.writebool('mplayer', 'doublebuffering', setsfo.be_doublebuf.value);
    fi.writebool('mplayer', 'disablescreensaver', setsfo.be_noscreensaver.value);

    fi.writestring('Devices', 'Webcamera', setsfo.edit_webcam.value);
    fi.writestring('Devices', 'TVTuner', setsfo.edit_tvtuner.value);
    fi.writestring('Devices', 'CDROM', setsfo.edit_cdrom.value);

    fi.free;
  except
    show_ask(mainfo, 'Cannot create settings file!'#10'(' + conffile + ')', '', false, false, false);
  end;
end;

procedure tmainfo.on_hide_addons(const sender: TObject);
begin
  spl2.top := s_middle.height - spl2.height;
end;

function tmainfo.show_ask(const sender: tobject; capt, fname: string; ddd_visible, se_visible, yes_visible: boolean): modalresultty;
begin
  askfo.left := (sender as tcustommseform).left + ((sender as tcustommseform).width - askfo.width) div 2;
  askfo.top := (sender as tcustommseform).top + ((sender as tcustommseform).height - askfo.height) div 2;

  askfo.l_.caption := capt;
  askfo.SE.value := fname;
  askfo.ddd.visible := ddd_visible;
  askfo.se.visible := se_visible;
  askfo.b_yes.visible := yes_visible;
  askfo.bringtofront;
  result := askfo.show(true);
end;

function tmainfo.get_mainwindow: tcustommseform;
begin
  if button_windowstyle.tag = 0 then
    result := mainfo
  else
    result := playlistfo;
end;

procedure tmainfo.rename_file(fold: string; updateplaylist, update_flv: boolean);
var
  i: integer;
begin
  if show_ask(get_mainwindow, 'Rename?', extractfilename(fold), false, true, true) = MR_OK then
    if not msefileutils.renamefile(fold, extractfilepath(fold) + '/' + askfo.SE.value) then
      show_ask(get_mainwindow, 'Cannot rename file!', '', false, false, false)
    else
    begin
      if updateplaylist then
      begin
        if wg_playlist.rowcount > 0 then
          for i := 0 to wg_playlist.rowhigh do
            if se_url[i] = fold then
            begin
              se_url[i] := extractfilepath(fold) + '/' + askfo.SE.value;
              se_name[i] := askfo.SE.value;
            end;
      end;
      if update_flv
        then
      begin
        if not setsfo.be_showonlymediafiles.value then
          for i := 0 to flv.filelist.count - 1 do
            if flv.itemlist[i].caption = extractfilename(fold) then
            begin
              flv.readlist;
              break;
            end;
        if setsfo.be_showonlymediafiles.value then
          for i := 0 to wg_fl.rowhigh do
            if se_fname[i] = extractfilename(fold) then
            begin
              readlist(dd_dir.value);
              break;
            end;
      end;
    end;
end;

procedure tmainfo.copyfiles(sl, sl2: tstringlist; copy_: boolean);
var
  i: integer;
begin
  if sl.count > 0 then
  begin
    copyfo.left := mainfo.left + (mainfo.width - copyfo.width) div 2;
    copyfo.top := mainfo.top + (mainfo.height - copyfo.height) div 2;
    copyfo.l_.caption := '0 / ' + inttostr(sl.count);
    copyfo.pb.value := 0;
    copyfo.bringtofront;
    copyfo.show;
    application.processmessages;
    for i := 0 to sl.count - 1 do
    begin
      if copy_ then
        copyfile(sl[i], sl2[i])
      else
        renamefile(sl[i], sl2[i]);
      copyfo.l_.caption := inttostr(i + 1) + ' / ' + inttostr(sl.count);
      copyfo.pb.value := (i + 1) / sl.count;
      application.processmessages;
    end;
    show_ask(get_mainwindow, 'Done', '', false, false, false);
    copyfo.close;
  end;
end;

procedure tmainfo.addfile(fcapt, url, emblem: string);
var
  r: integer;
begin
{$IFDEF mswindows}
  if url[1] = '/' then delete(url, 1, 1);
{$ENDIF}
  r := wg_playlist.rowcount;
  wg_playlist.rowcount := r + 1;
  se_url[r] := url;
  se_name[r] := fcapt;
  se_emblem[r] := emblem;
  player.playlist.add(url);
end;

procedure tmainfo.adddir(path: string);
var
  fs: tsearchRec;
begin
{$IFDEF mswindows}
  if path > '' then if path[1] = '/' then delete(path, 1, 1);
{$ENDIF}
  if findfirst(path + '/*', faanyfile, fs) = 0 then
    repeat
      if (fs.name = '') or (fs.name = '.') or (fs.name = '..') then continue;
      if fs.Attr and faDirectory <> 0
        then adddir(path + '/' + fs.name)
      else addfile(fs.name, path + '/' + fs.name, '');
    until findnext(fs) <> 0;
  findclose(fs);
end;

procedure tmainfo.on_set_directory_finav(const sender: TObject; var avalue: msestring; var accept: Boolean);
begin
  if setsfo.be_showonlymediafiles.value
    then readlist(avalue)
  else
  begin
    flv.directory := avalue;
    flv.readlist;
  end;
end;

procedure tmainfo.on_go_home(const sender: TObject);
begin
  dd_dir.value := setsfo.edit_navigatorhome.value;
  if setsfo.be_showonlymediafiles.value
    then readlist(setsfo.edit_navigatorhome.value)
  else
  begin
    flv.directory := setsfo.edit_navigatorhome.value;
    flv.readlist;
  end;
end;

procedure tmainfo.on_go_up(const sender: TObject);
var
  s: string;
begin
  if not setsfo.be_showonlymediafiles.value
    then
  begin
    flv.updir;
    dd_dir.value := flv.directory;
    exit;
  end;
  s := dd_dir.value;
  if s[length(s)] = '/' then delete(s, length(s), 1);
  if s = '' then exit;
  s := extractfilepath(s);
  dd_dir.value := s;
  readlist(s);
end;

procedure tmainfo.on_rename_file_in_flv(const sender: TObject);
begin
  if not setsfo.be_showonlymediafiles.value
    then
  begin
    if flv.focusedindex < 0 then exit;
    rename_file(flv.directory + flv.items[flv.focusedindex].caption, true, true);
    exit;
  end;
  if wg_fl.row < 0 then exit;
  rename_file(se_furl[wg_fl.row], true, true);
end;

procedure tmainfo.on_deletefile_flv(const sender: TObject);
var
  a: array of msestring;
  i: integer;
  s: string;
begin
  if not setsfo.be_showonlymediafiles.value
    then
  begin
    if flv.focusedindex < 0 then exit;
    if show_ask(get_mainwindow, 'Delete file(s)?', '', false, false, true) <> MR_OK then exit;
    a := flv.selectednames;
    s := flv.directory;
    for i := high(a) downto 0 do
      if flv.filelist.isdir(flv.filelist.indexof(a[i]))
        then myremovedir(s + a[i])
      else trydeletefile(s + a[i]);
    flv.readlist;
    exit;
  end;

  if wg_fl.rowcount = 0 then exit;
  if wg_fl.row < 0 then exit;
  if show_ask(get_mainwindow, 'Delete file(s)?', '', false, false, true) <> MR_OK then exit;
  for i := wg_fl.rowcount downto 0 do
  begin
    if extractfilename(se_furl[wg_fl.row]) <> se_fname[wg_fl.row]
      then myremovedir(se_furl[i])
    else trydeletefile(se_furl[i]);
  end;
  readlist(dd_dir.value);
  show_ask(get_mainwindow, 'Done', '', false, false, false);
end;

procedure tmainfo.on_reread_flv(const sender: TObject);
begin
  if setsfo.be_showonlymediafiles.value
    then readlist(dd_dir.value)
  else flv.readlist;
end;

procedure tmainfo.on_filenavigate(const sender: tcustomlistview; const index: integer; var info: celleventinfoty);
begin
  if iscellclick(info, [CCR_DBLCLICK]) then
    if flv.filelist.isdir(flv.focusedindex)
      then
    begin
      flv.directory := flv.directory + flv.items[flv.focusedindex].caption;
      flv.readlist;
      dd_dir.value := flv.directory;
    end
    else on_addfilefromdisk(b_open_from_flv);
end;

procedure tmainfo.on_addfilefromdisk(const sender: TObject);
var
  i, t: integer;
  ar: integerarty;
begin
  LLog.Add('tmainfo.on_addfilefromdisk()');
  if setsfo.be_showonlymediafiles.value then
  begin
    if wg_fl.row < 0 then exit;
  end
  else if flv.focusedindex < 0 then exit;

  if sender.classname = 'trichbutton' then
    t := (sender as trichbutton).tag
  else
    t := (sender as tmenuitem).tag;

  if t = 0 then
  begin
    on_stop(sender);
    on_clearplaylist(sender);
  end;

  if setsfo.be_showonlymediafiles.value then
    if wg_fl.rowcount > 0 then
    begin
      ar := wg_fl.datacols.selectedrows;
      for i := 0 to high(ar) do
        if extractfilename(se_furl[ar[i]]) <> se_fname[ar[i]] then
          adddir(se_furl[ar[i]])
        else
          addfile(se_fname[ar[i]], se_furl[ar[i]], '');
    end;

  if not setsfo.be_showonlymediafiles.value then
    for i := 0 to flv.filelist.count - 1 do
      if flv.itemlist[i].selected then
      begin
        if flv.filelist.isdir(i) then
          adddir(flv.path + flv.itemlist[i].caption)
        else
          addfile(flv.itemlist[i].caption, flv.path + flv.itemlist[i].caption, '');
      end;

  if t = 0 then
    on_play(sender);
end;

procedure tmainfo.on_add_from_lastplayed(const sender: TObject);
var
  i, t: integer;
  ar: integerarty;
begin
  if wg_last.row < 0 then exit;
  if sender.classname = 'trichbutton'
    then t := (sender as trichbutton).tag
  else t := (sender as tmenuitem).tag;

  if t = 0 then on_clearplaylist(sender);

  ar := wg_last.datacols.selectedrows;
  for i := 0 to high(ar) do
    addfile(se_name_last[ar[i]], se_url_last[ar[i]], se_emblem_last[ar[i]]);

  if t = 0 then
  begin
    on_stop(sender);
    on_play(sender);
  end;
end;

procedure tmainfo.on_add_file_from_last_tracks(const sender: TObject; var info: celleventinfoty);
begin
  if wg_last.ROW < 0 then EXIT;
  if iscellclick(info, [CCR_DBLCLICK]) then on_add_from_lastplayed(trichbutton11);
end;

procedure tmainfo.on_clear_list_of_last_played(const sender: TObject);
begin
  if show_ask(get_mainwindow, 'Clear ?', '', false, false, true) <> MR_OK then exit;
  if trydeletefile(dirtowin(setsfo.edit_playlistsdir.value) + '__LAST_PLAYED__')
    then
  begin
    wg_last.clear;
  end
  else show_ask(get_mainwindow, 'Can`t delete ' + dirtowin(setsfo.edit_playlistsdir.value) + '__LAST_PLAYED__', '', false, false, true);
end;

procedure tmainfo.on_delete_from_last_played(const sender: TObject);
var
  i: integer;
  f: textfile;
  ar: integerarty;
begin
  if wg_last.row < 0 then exit;
  if show_ask(get_mainwindow, 'Erase record(s)?', '', false, false, true) <> MR_OK then exit;
  ar := wg_last.datacols.selectedrows;
  for i := high(ar) downto 0 do wg_last.deleterow(ar[i]);
  if wg_last.rowhigh >= 0 then
  begin
    assignfile(f, dirtowin(setsfo.edit_playlistsdir.value) + '__LAST_PLAYED__');
    rewrite(f);
    for i := 0 to wg_last.rowhigh do
    begin
      writeln(f, se_name_last[i]);
      writeln(f, se_url_last[i]);
      writeln(f, se_emblem_last[i]);
    end;
    closefile(f);
  end;
end;

procedure tmainfo.on_loadplaylists_list(const sender: TObject);
var
  fs: tsearchrec;
  dir: string;
  sl: tstringlist;
begin
  wg_myplaylists.clear;
  sl := tstringlist.create;
  dir := dirtowin(SETSFO.edit_playlistsdir.value);
  if findfirst(dir + '*', faanyfile, fs) = 0 then
    repeat
      if (fs.name = '.') or (fs.name = '..') or (fs.name = '') then continue;
      try
        if fs.name = '__LAST_PLAYED__' then
        begin
          loadlast(dir);
          continue;
        end;

        sl.loadfromfile(dir + fs.name);
        if trim(sl[0]) <> '[playlist]' then continue;
        wg_myplaylists.rowcount := wg_myplaylists.rowcount + 1;
        se_myplaylist[wg_myplaylists.rowcount - 1] := fs.name;
      except
        wg_myplaylists.deleterow(wg_myplaylists.rowhigh);
      end;
    until findnext(fs) <> 0;
  sl.free;
  wg_myplaylists.sort;
  findclose(fs);
end;

procedure tmainfo.save_in_my_playlists(const wg: twidgetgrid; const se: tstringedit; fname: string);
var
  i: integer;
  sl: tstringlist;
begin
  sl := tstringlist.create;
  sl.add('[playlist]');
  sl.add('NumberOfEntries=' + inttostr(wg.rowcount));
  if wg.rowcount > 0 then
    for i := 0 to wg.rowhigh do sl.add('File' + inttostr(i) + '=' + se[i]);
  sl.savetofile(fname);
  sl.free;
  on_loadplaylists_list(b_reloadplaylists);
end;

procedure tmainfo.on_rename_my_playlist(const sender: TObject);
begin
  if wg_myplaylists.row < 0 then exit;
  rename_file(dirtowin(setsfo.edit_playlistsdir.value) + se_myplaylist[wg_myplaylists.row], false, false);
  on_loadplaylists_list(sender);
end;

procedure tmainfo.on_delete_from_my_playlists(const sender: TObject);
var
  i: integer;
  ar: integerarty;
  b: boolean;
begin
  if wg_myplaylists.row < 0 then exit;
  if show_ask(get_mainwindow, 'Erase playlist(s)?', '', false, false, true) <> MR_OK then exit;
  b := false;
  ar := wg_myplaylists.datacols.selectedrows;
  for i := high(ar) downto 0 do
    if deletefile(dirtowin(setsfo.edit_playlistsdir.value) + se_myplaylist[ar[i]])
      then wg_myplaylists.deleterow(ar[i])
    else b := true;
  if b then show_ask(get_mainwindow, 'Not all selected playlists were deleted.', '', false, false, false);
end;

procedure tmainfo.on_add_myplaylist_To_Playlist(const sender: TObject);
var
  sl: tstringlist;
  y, i, t: integer;
  ar: integerarty;
begin
  if wg_myplaylists.row < 0 then exit;

  if sender.classname = 'trichbutton'
    then t := (sender as trichbutton).tag
  else t := (sender as tmenuitem).tag;

  if t = 0 then
  begin
    on_stop(sender);
    on_clearplaylist(sender);
  end;

  ar := wg_myplaylists.datacols.selectedrows;
  for i := 0 to high(ar) do
  begin
    sl := tstringlist.create;
    openpls_playlist(dirtowin(setsfo.edit_playlistsdir.value) + se_myplaylist[ar[i]], sl);
    for y := 0 to sl.count - 1 do addfile(extractfilename(sl[y]), sl[y], '');
    sl.free;
  end;
  if t = 0 then on_play(sender);
end;

procedure tmainfo.on_dblclick_myplaylists(const sender: TObject; var info: celleventinfoty);
begin
  if iscellclick(info, [CCR_DBLCLICK]) then on_add_myplaylist_To_Playlist(b_open_from_myplaylists);
end;

procedure tmainfo.on_save_in_my_playlists(const sender: TObject);
begin
  if wg_playlist.rowcount = 0 then exit;
  if show_ask(get_mainwindow, 'Save in my playlists?', '', false, true, true) = MR_OK
    then save_in_my_playlists(wg_playlist, se_url, dirtowin(setsfo.edit_playlistsdir.value) + askfo.SE.text);
end;

procedure tmainfo.on_rewrite_my_playlist(const sender: TObject);
begin
  if wg_myplaylists.row < 0 then exit;
  if wg_playlist.rowcount = 0 then exit;
  if show_ask(get_mainwindow, 'Rewrite this playlist?', '', false, false, true) <> MR_OK then exit;
  save_in_my_playlists(wg_playlist, se_url, dirtowin(setsfo.edit_playlistsdir.value) + se_myplaylist[wg_myplaylists.row]);
end;

procedure tmainfo.on_LOADRADIOtv(const sender: TObject);
var
  t: integer;
begin
  if sender.classname = 'trichbutton'
    then t := (sender as trichbutton).tag
  else t := tw_main.activepageindex - 4;

  if t = 0
    then
  begin
    loadradiotv(radiotvpath + 'radio/radio.conf', radiotvpath + 'radio/images/', wg_radio);
    wg_radio.sort;
  end
  else
  begin
    loadradiotv(radiotvpath + 'tv/tv.conf', radiotvpath + 'tv/images/', wg_tv);
    wg_tv.sort;
  end;
end;

procedure tmainfo.loadradiotv(fname, prev: string; wg: twidgetgrid);
const
  thumbsize: sizety = (cx: 60; cy: 30);
var
  fi: tinifile;
  sl: tstringlist;
  i, r: integer;
  s, s2, s3: string;
  astream: tstream;
begin
{$IFDEF mswindows}
  if fname <> '' then if fname[1] = '/' then delete(fname, 1, 1);
{$ENDIF}
  sl := tstringlist.create;
  fi := tinifile.create(fname);
  fi.readsections(sl);
  wg.clear;
  if wg = wg_radio then s2 := 'radio'
  else s2 := 'tv';

  if not setsfo.be_allowloademblems.value
    then wg.datacols[0].width := 0
  else wg.datacols[0].width := 60;

  if sl.count > 0 then
    for i := 0 to sl.count - 1 do
    begin
      r := wg.rowcount;
      wg.rowcount := r + 1;
      s := fi.readstring(sl[i], 'emblem', '');
      try
        if setsfo.be_allowloademblems.value
          then if s <> '' then
            if fileexists(prev + s)
              then
            begin
              (findcomponent('di_emblem_' + s2) as tdataimage).gridvalue[r] := readfiledatastring(prev + s);
            end;
      except
      end;
      (findcomponent('se_name_' + s2) as tstringedit)[r] := utf8toansi(sl[i]);
      (findcomponent('se_url_' + s2) as tstringedit)[r] := fi.readstring(sl[i], 'url', '');
      (findcomponent('se_emblem_' + s2) as tstringedit)[r] := prev + s;
      (findcomponent('se_site_' + s2) as tstringedit)[r] := fi.readstring(sl[i], 'site', '');
    end;
  fi.free;
end;

procedure tmainfo.on_delete_chanel(const sender: TObject);
var
  fi: tinifile;
  wg: twidgetgrid;
  se: tstringedit;
  i: integer;
  ar: integerarty;
begin
  if tw_main.activepage = tp_radio
    then wg := wg_radio
  else wg := wg_tv;

  if wg.row < 0 then exit;
  if show_ask(get_mainwindow, 'Erase channel(s)?', '', false, false, true) <> MR_OK then exit;

  if tw_main.activepage = tp_radio
    then
  begin
    se := se_name_radio; fi := tinifile.create(radiotvpath + 'radio/radio.conf');
  end
  else
  begin
    se := se_name_tv; fi := tinifile.create(radiotvpath + 'tv/tv.conf');
  end;

  ar := wg.datacols.selectedrows;
  for i := high(ar) downto 0 do
    fi.EraseSection(ansitoutf8(se[ar[i]]));

  fi.free;
  if tw_main.activepage = tp_radio
    then on_loadradiotv(trichbutton20)
  else on_loadradiotv(trichbutton21);
end;

procedure tmainfo.on_add_radio_chanel(const sender: TObject);
begin
  chanelfo.im_emblem.bitmap.clear;
  dubblefo.visible := false;
  chanelfo.se_name.value := '';
  chanelfo.se_url.value := '';
  chanelfo.fne_emblem.value := '';
  chanelfo.se_key.value := '';
  chanelfo.bringtofront;
  if chanelfo.show(true) = MR_OK then on_save_chanel(sender);
end;

procedure tmainfo.on_edit_chanel(const sender: TObject);
begin
  dubblefo.visible := false;
  chanelfo.im_emblem.bitmap.clear;
  if tw_main.activepage = tp_radio
    then
  begin
    if wg_radio.row < 0 then exit;
    chanelfo.se_key.value := se_name_radio[wg_radio.row];
    chanelfo.se_name.value := se_name_radio[wg_radio.row];
    chanelfo.se_url.value := se_url_radio[wg_radio.row];
    chanelfo.se_site.value := se_site_radio[wg_radio.row];
    chanelfo.fne_emblem.value := se_emblem_radio[wg_radio.row];
  end
  else
  begin
    if wg_tv.row < 0 then exit;
    chanelfo.se_key.value := se_name_tv[wg_tv.row];
    chanelfo.se_name.value := se_name_tv[wg_tv.row];
    chanelfo.se_url.value := se_url_tv[wg_tv.row];
    chanelfo.se_site.value := se_site_tv[wg_tv.row];
    chanelfo.fne_emblem.value := se_emblem_tv[wg_tv.row];
  end;
  try
    chanelfo.im_emblem.bitmap.loadfromfile(chanelfo.fne_emblem.value);
  except
    writeln('error while loading ' + chanelfo.fne_emblem.value);
  end;
  chanelfo.bringtofront;
  if chanelfo.show(true) = MR_OK then on_save_chanel(sender);
end;

procedure tmainfo.on_save_chanel(const sender: TObject);
var
  fi: tinifile;
  S: string;
begin
  if tw_main.activepage = tp_radio then fi := tinifile.create(radiotvpath + 'radio/radio.conf')
  else fi := tinifile.create(radiotvpath + 'tv/tv.conf');
  if tw_main.activepage = tp_radio then s := radiotvpath + 'radio/images/'
  else s := radiotvpath + 'tv/images/';

  fi.EraseSection(ansitoutf8(chanelfo.se_key.text));

  fi.writestring(ansitoutf8(chanelfo.se_name.value), 'url', chanelfo.se_url.value);
  fi.writestring(ansitoutf8(chanelfo.se_name.value), 'emblem', extractfilename(chanelfo.fne_emblem.value));
  fi.writestring(ansitoutf8(chanelfo.se_name.value), 'site', extractfilename(chanelfo.se_site.value));
  fi.free;
  copyfile(chanelfo.fne_emblem.value, s + extractfilename(chanelfo.fne_emblem.value));
  if tw_main.activepage = tp_radio then on_loadradiotv(trichbutton20)
  else on_loadradiotv(trichbutton21);
end;

procedure tmainfo.on_add_radioTVchanel_to_playlist(const sender: TObject);
var
  i, t: integer;
  wg: twidgetgrid;
  s2: string;
  ar: integerarty;
begin
  if sender.classname = 'trichbutton'
    then t := (sender as trichbutton).tag
  else t := (sender as tmenuitem).tag;

  if t = 0 then
  begin
    on_stop(sender);
    on_clearplaylist(sender);
  end;

  if tw_main.activepage = tp_radio
    then wg := wg_radio
  else wg := wg_tv;

  if wg = wg_radio then s2 := 'radio'
  else s2 := 'tv';

  if wg.row < 0 then exit;
  ar := wg.datacols.selectedrows;
  for i := 0 to high(ar) do
    addfile((findcomponent('se_name_' + s2) as tstringedit)[ar[i]],
      (findcomponent('se_url_' + s2) as tstringedit)[ar[i]],
      (findcomponent('se_emblem_' + s2) as tstringedit)[ar[i]]);

  if t = 0 then on_play(sender);
end;

procedure tmainfo.on_radioTVlist_cell_event(const sender: TObject; var info: celleventinfoty);
begin
  if iscellclick(info, [CCR_DBLCLICK]) then on_add_radioTVchanel_to_playlist(b_open_from_radiolist);
end;

procedure tmainfo.ON_SHOW_PL_POPUP(const sender: TObject);
var
  gr: mouseeventinfoty;
begin
  gr.pos.x := 0;
  gr.pos.y := b_menu.height;
  pm_pl.show(b_menu, gr);
end;

procedure tmainfo.findcurrentruck(_restore: boolean);
var
  i, r: integer;
begin
  r := wg_playlist.row;
  wg_playlist.row := -1;

  if _restore then player.playlist.clear;
  for i := 0 to wg_playlist.rowhigh do
  begin
    if _restore then player.playlist.add(se_url[i]);
    if se_cur[i] > '' then
    begin
      player.tracknum := i;
      wg_playlist.rowfontstate[i] := 0;
    end
    else
      wg_playlist.rowfontstate[i] := -1;
  end;
  wg_playlist.row := r;
end;

procedure tmainfo.on_addfiles(const sender: TObject);
var
  s: array of msestring;
  i: integer;
begin
  fd.controller.filter := '';
  if fd.execute = MR_OK then
  begin
    s := fd.controller.filenames;
    for i := 0 to high(s) do
      addfile(extractfilename(s[i]), s[i], '');
    if player.tracknum = -1 then
      player.tracknum := 0;
    if not player.playing then
      on_play(sender);
  end;
end;

procedure tmainfo.on_add_url(const sender: TObject);
begin
  if show_ask(get_mainwindow, 'Add URL?', '', false, true, true) = MR_OK then addfile(askfo.se.value, askfo.se.value, '');
end;

procedure tmainfo.on_openplaylist(const sender: TObject);
var
  s: array of msestring;
  i: integer;
  sl, sl2: tstringlist;

  procedure addfiles;
  var
    y: integer;
  begin
    if sl.count > 0 then
      for y := 0 to sl.count - 1 do
        addfile(extractfilename(sl[y]), sl[y], '');
  end;

  procedure addfiles2;
  var
    y: integer;
  begin
    if sl.count > 0 then
      for y := 0 to sl.count - 1 do
        addfile(sl2[y], sl[y], '');
  end;

begin
  fd.controller.filterlist.clear;
  fd.controller.filterlist.add('All files', '*');
  fd.controller.filterlist.add('PLS playlists', '*.pls');
  fd.controller.filterlist.add('M3U playlists', '*.m3u');
  fd.controller.filterlist.add('KPL playlists', '*.kpl');
  fd.controller.filterlist.add('ASX playlists', '*.asx');
  fd.controller.filterlist.add('Online playlists', '*.online');

  if fd.execute = MR_OK then
  begin
    s := fd.controller.filenames;
    wg_playlist.clear;
    player.playlist.clear;
    sl := tstringlist.create;
    sl2 := tstringlist.create;
    for i := 0 to high(s) do
    begin
      s[i] := dirtowin(extractfilepath(s[i])) + extractfilename(s[i]);
      sl.loadfromfile(s[i]);
      sl.text := trim(sl.text);
      if (lowercase(extractfileext(s[i])) = '.pls') or ((trim(sl[0]) = '[playlist]') and (system.pos('NumberOfEntries', sl[1]) > 0)) then
      begin
        openpls_playlist(s[i], sl);
        addfiles;
      end;
      if (lowercase(extractfileext(s[i])) = '.m3u') or (trim(sl[0]) = '#EXTM3U') then
      begin
        openm3u_playlist(s[i], sl, sl2);
        addfiles2;
      end;
      if (lowercase(extractfileext(s[i])) = '.kpl') or ((trim(sl[0]) = '[playlist]') and (system.pos('NumberOfEntries', sl[1]) = 0)) then
      begin
        openkpl_playlist(s[i], sl);
        addfiles;
      end;
      if (lowercase(extractfileext(s[i])) = '.asx') or (system.pos('<Asx', sl[0]) > 0) then
      begin

      end;
      if lowercase(extractfileext(s[i])) = '.online' then
      begin
        openOnline_playlist(s[i], sl2, sl);
        addfiles2;
      end;
    end;
    sl.free;
    sl2.free;
  end;
end;

procedure tmainfo.on_open_directory(const sender: TObject);
begin
  if show_ask(get_mainwindow, 'Add directory?', '', true, false, true) = MR_OK then adddir(askfo.ddd.value);
end;

procedure tmainfo.on_copyfile(const sender: TObject);
var
  sl, sl2: tstringlist;
  r: integer;
begin
  sl := tstringlist.create;
  sl2 := tstringlist.create;
  for r := 0 to wg_playlist.rowhigh do
    if wg_playlist.datacols[0].selected[r] then
    begin
      sl.add(se_url[r]);
      sl2.add((sender as tmenuitem).caption + '/' + extractfilename(se_url[r]));
    end;
  if (sender as tmenuitem).tag = 0
    then copyfiles(sl, sl2, true)
  else copyfiles(sl, sl2, false);
  sl.free;
  sl2.free;
end;

procedure tmainfo.on_saveinMyPlayLists(const sender: TObject);
begin
  if wg_playlist.rowcount = 0 then exit;
  if show_ask(get_mainwindow, 'Enter new playlist name', '', false, true, true) = MR_OK
    then save_in_my_playlists(wg_playlist, se_url, setsfo.edit_playlistsdir.value + '/' + askfo.SE.text);
end;

procedure tmainfo.on_save_as_radio_or_tv_chanel(const sender: TObject);
begin
  if (sender as tmenuitem).tag = 0 then tw_main.activepage := tp_radio
  else tw_main.activepage := tp_tv;
  chanelfo.se_name.value := se_name[wg_playlist.row];
  chanelfo.se_url.value := se_url[wg_playlist.row];
  chanelfo.fne_emblem.value := se_emblem[wg_playlist.row];
  chanelfo.se_key.value := se_name[wg_playlist.row];
  chanelfo.bringtofront;
  if chanelfo.show = MR_OK then on_save_chanel(sender);
end;

procedure tmainfo.on_save_urls_to_clipboard(const sender: TObject);
var
  r: integer;
  s: string;
begin
  edit_.text := '';
  s := '';
  for r := 0 to wg_playlist.rowhigh do
    if wg_playlist.datacols[0].selected[r] then
      s := s + #10 + se_url[R];
  edit_.text := trim(s);
  edit_.editor.SELECTALL;
  edit_.editor.copytoclipboard;
end;

procedure tmainfo.on_save_as__(const sender: TObject);
var
  sl, sl2: tstringlist;
begin
  if wg_playlist.row < 0 then exit;
  fd.controller.filter := '';
  fd.controller.filename := se_name[wg_playlist.row];
  if fd.execute = MR_OK then
  begin
    sl := tstringlist.create;
    sl2 := tstringlist.create;
    sl.add(se_url[wg_playlist.row]);
    sl2.add(fd.controller.filename);
    copyfiles(sl, sl2, true);
    sl.free;
    sl2.free;
  end;
end;

procedure tmainfo.on_save_files_to__(const sender: TObject);
var
  itm: tmenuitem;
begin
  if wg_playlist.row < 0 then exit;
  if show_ask(get_mainwindow, 'Copy file(s) in ', '', true, false, true) = MR_OK then
  begin
    itm := tmenuitem.create;
    itm.tag := 0;
    itm.caption := askfo.ddd.value;
    itm.state := [as_localcaption, as_localonexecute];
    itm.onexecute := @on_copyfile;
    pm_pl.menu.items[2].items[8].submenu.insert(2, itm);
    on_copyfile(itm);
  end;
end;

procedure tmainfo.on_movefiles_to__(const sender: TObject);
var
  itm: tmenuitem;
begin
  if wg_playlist.row < 0 then exit;
  if show_ask(get_mainwindow, 'Move file(s) in ', '', true, false, true) = MR_OK then
  begin
    itm := tmenuitem.create;
    itm.tag := 1;
    itm.caption := askfo.ddd.value;
    itm.state := [as_localcaption, as_localonexecute];
    itm.onexecute := @on_copyfile;
    pm_pl.menu.items[2].items[9].submenu.insert(2, itm);
    on_copyfile(itm);
  end;
end;

procedure tmainfo.on_rename_file_in_playlist(const sender: TObject);
begin
  if wg_playlist.row > -1 then
    rename_file(se_url[wg_playlist.row], true, true);
end;

procedure tmainfo.on_deleteformplaylist_pm(const sender: TObject);
var
  r, i, y: integer;
  delfiles, delfilesresult, selected: boolean;
  s: string;
  a: array of integer;
begin
  if wg_playlist.row < 0 then exit;
  r := 0;
  delfiles := false;
  if (sender as tmenuitem).tag = 2 then
  begin
    if show_ask(get_mainwindow, 'Are you sure you want to delete the file(s) from the disk?', '', false, false, true) <> MR_OK then
      exit;
    delfiles := true;
  end;

  if (sender as tmenuitem).tag = 5 then
    selected := false
  else
    selected := true;

  y := 0;
  for i := 0 to wg_playlist.rowhigh do
    if wg_playlist.datacols[0].selected[i] = selected then
    begin
      inc(y);
      setlength(a, y);
      a[y - 1] := i;
    end;
  s := player.currenttrack.filename;
  delfilesresult := true;
  r := player.tracknum;
  for i := high(a) downto 0 do
  begin
    if delfiles
      then if not deletefile(se_url[a[i]])
      then delfilesresult := false;
    wg_playlist.deleterow(a[i]);
    player.playlist.delete(a[i]);
    if a[i] <= r then dec(r);
  end;
  player.tracknum := r;
  if player.playing then
    if s <> player.playlist[r] then
    begin
      player.stop; player.play;
    end;
  if (sender as tmenuitem).tag = 2 then
  begin
    if setsfo.be_showonlymediafiles.value
      then readlist(dd_dir.value)
    else flv.readlist;
  end;
  if not delfilesresult
    then show_ask(get_mainwindow, 'Not all files have been deleted!', '', false, false, false)
  else if (sender as tmenuitem).tag = 2 then
  begin
    if setsfo.be_showonlymediafiles.value
      then readlist(dd_dir.value)
    else flv.readlist;
  end;
end;

procedure tmainfo.on_topTruckinPlaylist(const sender: TObject);
var
  r, y: integer;
begin
  y := -1;
  if wg_playlist.rowcount = 0 then exit;
  for r := 0 to wg_playlist.rowhigh do
    if wg_playlist.datacols[0].selected[r] then
    begin
      inc(y);
      wg_playlist.moverow(r, y);
      player.playlist.move(r, y);
    end;
  findcurrentruck(false);
end;

procedure tmainfo.on_upTrackInPlaylist(const sender: TObject);
var
  r: integer;
begin
  if wg_playlist.rowcount = 0 then exit;
  for r := 0 to wg_playlist.rowhigh do
    if wg_playlist.datacols[0].selected[r] then
    begin
      if r = 0 then exit;
      wg_playlist.moverow(r, r - 1);
      player.playlist.move(r, r - 1);
    end;
  findcurrentruck(false);
end;

procedure tmainfo.on_downTrackInPlaylist(const sender: TObject);
var
  r: integer;
begin
  if wg_playlist.rowcount = 0 then exit;
  for r := wg_playlist.rowhigh downto 0 do
    if wg_playlist.datacols[0].selected[r] then
    begin
      if r = wg_playlist.rowhigh then exit;
      wg_playlist.moverow(r, r + 1);
      player.playlist.move(r, r + 1);
    end;
  findcurrentruck(false);
end;

procedure tmainfo.on_bottomTrauckinPlaylist(const sender: TObject);
var
  r, y: integer;
begin
  if wg_playlist.rowcount = 0 then exit;
  y := wg_playlist.rowhigh + 1;
  for r := wg_playlist.rowhigh downto 0 do
    if wg_playlist.datacols[0].selected[r] then
    begin
      dec(y);
      wg_playlist.moverow(r, y);
      player.playlist.move(r, y);
    end;
  findcurrentruck(false);
end;

procedure tmainfo.on_shuffle(const sender: TObject);
var
  r, c, p: integer;
begin
  c := wg_playlist.rowhigh;
  if c > 0 then
    for r := 0 to c do
    begin
      p := random(c);
      wg_playlist.moverow(r, p);
      player.playlist.move(r, p);
    end;
  findcurrentruck(false);
end;

procedure tmainfo.on_sort_playlist(const sender: TObject);
var
  i: integer;
begin
  if wg_playlist.rowhigh < 0 then exit;
  wg_playlist.sort;
  player.playlist.clear;
  for i := 0 to wg_playlist.rowhigh do
    player.playlist.add(se_url[i]);
  findcurrentruck(false);
end;

procedure tmainfo.on_clearplaylist(const sender: TObject);
begin
  wg_playlist.clear;
  player.playlist.clear;
  player.tracknum := -1;
end;

procedure tmainfo.on_dblclick_on_playlist(const sender: TObject; var info: celleventinfoty);
begin
  if iscellclick(info, [CCR_DBLCLICK])
    then
  begin
    findcurrentruck(true);
    player.trackNum := wg_playlist.row;
    player.play;
  end;
end;

procedure tmainfo.on_changeVolumeEvent(const Sender: TObject; avolume: integer);
begin
  if avolume = 0 then b_volume.imagenr := 11
  else if (avolume > 0) and (avolume <= 25) then b_volume.imagenr := 12
  else if (avolume > 25) and (avolume <= 50) then b_volume.imagenr := 13
  else if (avolume > 50) and (avolume <= 75) then b_volume.imagenr := 14
  else if (avolume > 75) then b_volume.imagenr := 15;

  sl_volume.value := avolume / 100;
end;

procedure tmainfo.onConnectingEvent(const Sender: TObject; msg: AnsiString);
var
  i: integer;
begin
  if player.mode = __webcamera then
  begin
    if viewfo.visible then
      viewfo.ww_video.visible := true
    else mainfo.ww_video.visible := true;
  end
  else
    if player.mode = __cd then
    begin
    end
    else
      if wg_playlist.rowcount > 0 then
      begin
        for i := 0 to wg_playlist.rowhigh do
        begin
          if i = player.tracknum
            then
          begin
            se_cur[i] := _cursor;
            wg_playlist.rowfontstate[i] := 0;
          end
          else
          begin
            se_cur[i] := '';
            wg_playlist.rowfontstate[i] := -1;
          end;
        end;
        if se_emblem[player.tracknum] > '' then
        begin
          if fileexists(se_emblem[player.tracknum]) then
          try
            mainfo.im_emblem.bitmap.clear;
            mainfo.im_emblem.bitmap.loadfromfile(se_emblem[player.tracknum]);
            mainfo.im_emblem.visible := true;
            mainfo.b_animate.visible := false;
          except
            mainfo.im_emblem.visible := false;
            mainfo.b_animate.visible := true;
          end;
        end
      end;

  l_video_inf.caption := msg;
  b_play.imagenr := 4;
  b_playDVD.imagenr := 4;
end;

procedure tmainfo.on_EndOfTrackEvent(const Sender: TObject; msg: AnsiString);
begin
  l_video_inf.caption := msg;
  l_trackinfo.caption := '';
  caption := 'XELPLAYER';
  l_video_inf.frame.caption := '';

  if button_windowstyle.tag = 1
    then
  begin
    viewfo.ww_video.visible := false;
    if viewfo.be_autoclose.value then viewfo.visible := false;
  end
  else mainfo.ww_video.visible := false;

  mainfo.im_emblem.visible := false;
  mainfo.b_animate.visible := true;

  b_play.imagenr := 1;
  b_playDVD.imagenr := 1;
  sl_position.value := 0;
  if player2.playing then
  begin
    if audioN = 1 then on_set_audio_0(sender);
    player2.stop;
    player2.playlist.clear;
  end;
end;

procedure tmainfo.on_ErrorEvent(const Sender: TObject; errormsg: AnsiString);
begin
  l_video_inf.caption := errormsg;
end;

procedure tmainfo.on_GetDebugMessage(const Sender: TObject; msg: AnsiString);
var
  i, r: integer;
  sl_debug: tstringlist;
begin
  sl_debug := tstringlist.create;
  sl_debug.text := msg;
  if sl_debug.count > 0 then
    for i := 0 to sl_debug.count - 1 do
      if setsfo <> nil then
      begin
        r := setsfo.sg_debug.rowcount;
        setsfo.sg_debug.rowcount := r + 1;
        setsfo.sg_debug[0][r] := sl_debug[i];
        setsfo.sg_debug.row := r;
      end;
  sl_debug.free;
end;

procedure tmainfo.on_PauseEvent(const Sender: TObject; const apausing: Boolean; msg: AnsiString);
begin
  l_video_inf.caption := msg;
  if apausing then b_play.imagenr := 1 else b_play.imagenr := 4;
  if apausing then b_playDVD.imagenr := 1 else b_playDVD.imagenr := 4;

  if player2.playing then
  begin
    player2.pause;
  end;
end;

procedure tmainfo.on_PlayingEvent(const Sender: TObject; position: integer; length: integer; msg: AnsiString);
begin
  if length = 0
    then sl_position.value := 0
  else sl_position.value := position / length;

  l_pos.caption := SecondsToFmtStr(position) + ' / ' + SecondsToFmtStr(length);
  l_video_inf.caption := msg;
end;

procedure tmainfo.on_StartPlayEvent(const Sender: TObject; videoW: integer; videoH: integer; msg: AnsiString);
var
  I: integer;
  s: string;
  f: textfile;
begin
  l_video_inf.caption := msg;
  if player.mode = __cd then
  begin
    for i := 0 to player.playlist.count - 1 do
      if i <> player.tracknum
        then sg_cdplaylist.datacols[0][i] := ''
      else sg_cdplaylist.datacols[0][i] := _cursor;
  end;

  if player.havevideostream then
  begin
    mainfo.im_emblem.visible := false;
    mainfo.b_animate.visible := false;
{$IFDEF mswindows}
    application.processmessages;
{$ENDIF}
    on_resizevideo(sender);

  end;
  if PLAYER2.PLAYLIST.COUNT > 0
    then
  begin
    on_set_audio_1(sender);
    player2.play;
  end;
  l_trackinfo.caption :=
    'File    :  ' + player.currenttrack.filename + #10 +
    'Artist  :  ' + player.currenttrack.Artist + #10 +
    'Title   :  ' + player.currenttrack.Title + #10 +
    'Album   :  ' + player.currenttrack.Album + #10 +
    'Year    :  ' + player.currenttrack.Year + #10 +
    'Comment :  ' + player.currenttrack.Comment + #10 +
    'Genre   :  ' + player.currenttrack.Genre + #10 +
    'Video   :  ' + inttostr(player.currenttrack.video.width) + ' x ' + inttostr(player.currenttrack.video.height);
{$IFDEF mswindows}
  application.processmessages;
{$ENDIF}
  if tw_playlist.ACTIVEPAGE = tp_audiocd_pl
    then
  begin
    caption := 'XELPLAYER : ' + sg_cdplaylist[1][player.tracknum];
    l_video_inf.frame.caption := '(' + sg_cdplaylist[1][player.tracknum] + ')';
  end else
  begin
    caption := 'XELPLAYER : ' + se_name[player.tracknum];
    l_video_inf.frame.caption := '(' + se_name[player.tracknum] + ')';
    if se_url_last[0] <> player.currenttrack.filename
      then
    begin
      wg_last.insertrow(0);
      se_name_last[0] := se_name[player.tracknum];
      se_url_last[0] := se_url[player.tracknum];
      se_emblem_last[0] := se_emblem[player.tracknum];
      assignfile(f, dirtowin(setsfo.edit_playlistsdir.value) + '__LAST_PLAYED__');
      if not fileexists(dirtowin(setsfo.edit_playlistsdir.value) + '__LAST_PLAYED__') then rewrite(f) else append(f);
      writeln(f, se_name_last[0]);
      writeln(f, se_url_last[0]);
      writeln(f, se_emblem_last[0]);
      closefile(f);
    end;
  end;
end;

procedure tmainfo.on_startplayevent2(const Sender: TObject; videoW: integer; videoH: integer; msg: AnsiString);
begin
  player2.position := player.position;
  on_set_audio_1(sender);
end;

procedure tmainfo.on_playingevent2(const Sender: TObject; position: integer; length: integer; msg: AnsiString);
begin
  if abs(position - player.position) > 1 then player2.position := player.position;
end;

procedure tmainfo.on_play(const sender: TObject);
var
  i: integer;
begin
  LLog.Add('tmainfo.on_play()');
  if __continueplay then
  begin
    __continueplay := false;
    if tw_main.activepage = tp_tuner then
      player.OpenTV()
    else
      if tw_main.activepage = tp_webcam then
        player.openwebcamera
      else
        player.play(__continueplaypos);
  end else
  begin
    if player.playing then
    begin
      player.pause;
      if audioN = 1 then player2.pause;
    end else
    begin
      if tw_main.activepage = tp_tuner then
        player.OpenTV()
      else
        if tw_main.activepage = tp_audiocd then
        begin
          if player.playing then
            player.pause
          else
          begin
            player.connecttoaudiocd;
            if player.tracknum > sg_cdplaylist.rowcount - 1 then
              player.tracknum := 1;
            player.openaudiocd(player.tracknum);
          end;
        end else
          if tw_main.activepage = tp_webcam then
            player.openwebcamera
          else
          begin
            if wg_playlist.rowcount > 0 then
            begin
              player.playlist.clear;
            end;
            findcurrentruck(true);
            player.play;
            if audioN = 1 then
              player2.play;
          end;
    end;
  end;
end;

procedure tmainfo.on_stop(const sender: TObject);
begin
  player.stop;
end;

procedure tmainfo.on_prev(const sender: TObject);
begin
  player.prev;
end;

procedure tmainfo.on_next(const sender: TObject);
begin
  player.next;
end;

procedure tmainfo.on_load_audio_1(const sender: TObject);
begin
  set2fo.visible := false;
  fd.controller.filter := '';
  if fd.execute = MR_OK then
  begin
    player2.stop;
    player2.playlist.clear;
    player2.playlist.add(fd.controller.filename);
    if PLAYER.PLAYing then player2.play;
  end;
end;

procedure tmainfo.on_set_audio_1(const sender: TObject);
var
  i: integer;
begin
  if player2.playlist.count = 0 then exit;
  audioN := 1;
  i := player.volume;
  player.volume := 0;
  player2.volume := i;
  ber_audio1.value := true;
end;

procedure tmainfo.on_set_audio_0(const sender: TObject);
var
  i: integer;
begin
  audioN := 0;
  if player2.playlist.count = 0 then exit;
  i := player2.volume;
  player2.volume := 0;
  player.volume := i;
  ber_audio0.value := true;
end;

procedure tmainfo.on_change_position(const sender: TObject);
var
  s: string;
begin
  s := (sender as tslider).name;
  delete(s, 1, 2);
  (findcomponent('pb' + s) as tprogressbar).value := (sender as tslider).value;
end;

procedure tmainfo.on_setposition(const sender: TObject; var avalue: realty; var accept: Boolean);
begin
  player.position := round(avalue * player.length);
end;

procedure tmainfo.on_setvolume(const sender: TObject; var avalue: realty; var accept: Boolean);
begin
  if audioN = 1 then
    player2.volume := round(avalue * 100)
  else
    player.volume := round(avalue * 100);
end;

procedure tmainfo.on_mute(const sender: TObject);
begin
  if audioN = 1 then
    player2.mute
  else
    player.mute;
end;

procedure tmainfo.on_showVsetform(const sender: TObject);
var
  i: integer;
begin
  if __showset2fofirst then
  begin
    __showset2fofirst := false;
    set2fo.left := left + width - set2fo.width;
    i := top + height - set2fo.height - s_bottom.height;
    if i < 0 then set2fo.top := 0 else set2fo.top := i;
  end;
  set2fo.bringtofront;
  if set2fo.visible then set2fo.visible := false else set2fo.show;
end;

procedure tmainfo.on_setbrightness(const sender: TObject; var avalue: realty; var accept: Boolean);
begin
  player.video_brightness := round(avalue * 200 - 100);
end;

procedure tmainfo.on_setcontrast(const sender: TObject; var avalue: realty; var accept: Boolean);
begin
  player.video_contrast := round(avalue * 200 - 100);
end;

procedure tmainfo.on_setsaturation(const sender: TObject; var avalue: realty; var accept: Boolean);
begin
  player.video_saturation := round(avalue * 200 - 100);
end;

procedure tmainfo.on_sethue(const sender: TObject; var avalue: realty; var accept: Boolean);
begin
  player.video_hue := round(avalue * 200 - 100);
end;

procedure tmainfo.on_setgamma(const sender: TObject; var avalue: realty; var accept: Boolean);
begin
  player.video_gamma := round(avalue * 200 - 100);
end;

procedure tmainfo.on_setbalance(const sender: TObject; var avalue: realty; var accept: Boolean);
begin
  player.balance := avalue * 2 - 1;
end;

procedure tmainfo.on_reset_video_options(const sender: TObject);
begin
  sl_brightness.value := 0.5; player.video_brightness := 0;
  sl_contrast.value := 0.5; player.video_contrast := 0;
  sl_gamma.value := 0.5; player.video_gamma := 0;
  sl_hue.value := 0.5; player.video_hue := 0;
  sl_saturation.value := 0.5; player.video_saturation := 0;
end;

procedure tmainfo.on_set_resolution(const sender: TObject);
begin
  if (sender as tbooleaneditradio).value = true then on_resizevideo(sender);
end;

procedure tmainfo.on_set_equalizer(const sender: TObject);
begin
  player.equalizer := (sl_eq1.frame.caption) + ':' +
    (sl_eq2.frame.caption) + ':' +
    (sl_eq3.frame.caption) + ':' +
    (sl_eq4.frame.caption) + ':' +
    (sl_eq5.frame.caption) + ':' +
    (sl_eq6.frame.caption) + ':' +
    (sl_eq7.frame.caption) + ':' +
    (sl_eq8.frame.caption) + ':' +
    (sl_eq9.frame.caption) + ':' +
    (sl_eq10.frame.caption);
  player2.equalizer := player.equalizer;
end;

procedure tmainfo.on_set_equalizer_row(const sender: TObject; var avalue: realty; var accept: Boolean);
begin
  (sender as tslider).frame.caption := floattostr(round((avalue - 0.5) * 24));
  on_set_equalizer(sender);
end;

procedure tmainfo.on_cancel_equalizer(const sender: TObject);
begin
  sl_eq1.value := 0.5; sl_eq1.frame.caption := '0';
  sl_eq2.value := 0.5; sl_eq2.frame.caption := '0';
  sl_eq3.value := 0.5; sl_eq3.frame.caption := '0';
  sl_eq4.value := 0.5; sl_eq4.frame.caption := '0';
  sl_eq5.value := 0.5; sl_eq5.frame.caption := '0';
  sl_eq6.value := 0.5; sl_eq6.frame.caption := '0';
  sl_eq7.value := 0.5; sl_eq7.frame.caption := '0';
  sl_eq8.value := 0.5; sl_eq8.frame.caption := '0';
  sl_eq9.value := 0.5; sl_eq9.frame.caption := '0';
  sl_eq10.value := 0.5; sl_eq10.frame.caption := '0';
  on_set_equalizer(sender);
end;

procedure tmainfo.on_set_audioN(const sender: TObject; var avalue: Boolean; var accept: Boolean);
begin
  if player2.playlist.count = 0 then
  begin
    accept := false; exit;
  end;
  if not player2.playing then
  begin
    accept := false; exit;
  end;

  if sender = ber_audio0
    then on_set_audio_0(sender)
  else on_set_audio_1(sender);
end;

procedure tmainfo.on_set_volnorm(const sender: TObject; var avalue: Boolean;
  var accept: Boolean);
begin
  player.AudioEffect_volumeNorm := avalue;
end;

procedure tmainfo.on_set_karaoke(const sender: TObject; var avalue: Boolean;
  var accept: Boolean);
begin
  player.AudioEffect_karaoke := avalue;
end;

procedure tmainfo.on_set_extrastereo(const sender: TObject; var avalue: Boolean;
  var accept: Boolean);
begin
  player.AudioEffect_extrastereo := avalue;
end;

procedure tmainfo.on_set_vectors(const sender: TObject; var avalue: Boolean; var accept: Boolean);
begin
  player.videoeffect_vectors := avalue;
  restart_play;
end;

procedure tmainfo.on_set_videoeffect(const sender: TObject; var avalue: Boolean; var accept: Boolean);
begin
  if sender = b_mirror then player.videoeffect_mirror := avalue;
  if sender = b_flip then player.videoeffect_flip := avalue;
  if sender = b_rotate90 then player.videoeffect_rotate90 := avalue;
  restart_play;
end;

procedure tmainfo.restart_play;
var
  pos_, a: string;
begin
  if player.playing then
  begin
    pos_ := SecondsToFmtStr(player.position);
    a := '';
    if player2.playlist.count > 0 then a := player2.playlist[0];
    on_stop(b_stop);
    player.playfrompositionStr := pos_;
    on_play(b_play);
    player.playfrompositionStr := '';
    if a > '' then
    begin
      player2.playlist.add(a);
      player2.playfrompositionStr := pos_;
      player2.play;
      player2.playfrompositionStr := '';
    end;
  end;
end;

procedure tmainfo.on_maintimer(const sender: TObject);
var
  f: textfile;
  s, s2: string;
  sl, sl2: tstringlist;
  plfile: string;

  procedure addfiles;
  var
    y: integer;
  begin
    if sl.count > 0 then
      for y := 0 to sl.count - 1 do
        addfile(extractfilename(sl[y]), sl[y], '');
  end;

  procedure addfiles2;
  var
    y: integer;
  begin
    if sl.count > 0 then
      for y := 0 to sl.count - 1 do
        addfile(sl2[y], sl[y], '');
  end;

begin

  if (setsfo.be_useonecopy.value) or (b_useone) then
  begin
    b_useone := false;

    S := sys_getuserhomedir;
{$IFDEF windows}
    if S > '' then if S[1] = '/' then delete(S, 1, 1);
{$ENDIF}
    forcedirectories(S + '/.Almin-Soft/xelplayer');
    plfile := S + '/.Almin-Soft/xelplayer/xelplayer.pl';

    if fileexists(plfile) then
    begin
      on_clearplaylist(sender);

      assignfile(f, plfile);
      reset(f);
      sl := tstringlist.create;
      sl2 := tstringlist.create;
      while not eof(f) do
      begin
        readln(f, s);
        if lowercase(extractfileext(s)) = '.pls' then
        begin
          openpls_playlist(s, sl);
          addfiles;
        end
        else if lowercase(extractfileext(s)) = '.m3u' then
        begin
          openm3u_playlist(s, sl, sl2);
          addfiles2;
        end
        else if lowercase(extractfileext(s)) = '.kpl' then
        begin
          openkpl_playlist(s, sl);
          addfiles;
        end
        else if lowercase(extractfileext(s)) = '.asx' then
        begin

        end
        else if extractfileext(s) = '.online' then
        begin
          openOnline_playlist(s, sl2, sl);
          addfiles;
        end
        else addfile(extractfilename(s), s, '');

      end;
      sl.free;
      sl2.free;
      closefile(f);
      deletefile(plfile);
      on_stop(sender);
      on_play(sender);
    end;
  end;
end;

procedure tmainfo.on_open_stop_dvd(const sender: TObject);
begin
  __continueplay := false;
  if (player.playing) and (player.mode <> __dvd) then player.stop;
  if (player.playing) and (player.mode = __dvd)
    then player.stop
  else
  begin
    if tbooleaneditradio2.value
      then player.aditionalparams := '-dvd-device "' + ddd_dvdpath.value + '"'
    else player.aditionalparams := '';
    player.opendvd;
  end;
end;

procedure tmainfo.on_dvd_menu(const sender: TObject);
begin
  player.dvdnavigation_menu;
end;

procedure tmainfo.on_dvd_left(const sender: TObject);
begin
  player.dvdnavigation_left;
end;

procedure tmainfo.on_dvd_right(const sender: TObject);
begin
  player.dvdnavigation_right;
end;

procedure tmainfo.on_dvd_up(const sender: TObject);
begin
  player.dvdnavigation_up;
end;

procedure tmainfo.on_dvd_down(const sender: TObject);
begin
  player.dvdnavigation_down;
end;

procedure tmainfo.on_dvd_select(const sender: TObject);
begin
  player.dvdnavigation_select;
  if player.pausing then player.pause;
end;

procedure tmainfo.on_dvdpause(const sender: TObject);
begin
  player.pause;
end;

procedure tmainfo.on_set_dvd_iso_path(const sender: TObject;
  var avalue: Boolean; var accept: Boolean);
begin
  ddd_dvdpath.enabled := avalue;
  ddd_audioCDdevice.enabled := not avalue;
  trichbutton5.enabled := not avalue;
end;

function StrToTvNorm(NormStr: string): tvnorm;
var
  I: tvnorm;
begin
  Result := SECAM_DK;
  for I in TvNorm do
    if Tvstr[I] = NormStr then Result := I;
end;

procedure tmainfo.loadtvtuner;
var
  i: integer;
  Itv: TTVData;
  str: string;
begin
  sc := TIniFile.Create(CFGChannelsName);
  for i := 1 to sc.ReadInteger('TVcount', 'Count', 0) do
  begin
    Itv.Freq := sc.ReadInteger('TV' + inttostr(i), 'freqtv', 45);
    str := Sc.ReadString('TV' + inttostr(i), 'normtv', 'SECAM-DK');
    Itv.Norm := StrToTvNorm(str);
    Itv.Name := sc.ReadString('TV' + inttostr(i), 'tvname', 'TV');
    player.ListofChanell.AddItem(Itv);
    st.rowcount := st.rowcount + 1;
    st.datacols[0][st.rowcount - 1] := Itv.Name;
    st.datacols[1][st.rowcount - 1] := inttostr(Itv.Freq);
    st.datacols[2][st.rowcount - 1] := str;
  end;
  sc.free;
end;

procedure tmainfo.on_dblclickOnTVlist(const sender: TObject; var info: celleventinfoty);
var
  sf: focuscellactionty;
  t: twidgetgrid;
begin
  if iscellclick(info, [CCR_DBLCLICK]) then
  begin
    player.SetChannelByfreq(player.ListofChanell.GetData(CountSelectionGrid).Freq);
    player.SetChannelNorm(player.ListofChanell.GetData(CountSelectionGrid).Norm);
  end;
  sf := info.selectaction;
  t := twidgetgrid(Sender);
  if (sf = fca_focusinforce) or (sf = fca_focusin) or (sf = fca_entergrid) then
  begin
    if (info.cell.col <= t.datacols.Count) then
    begin
      if (info.cell.col >= 0) then
      begin
        if (info.cell.row <= t.rowcount) then
        begin
          if (info.cell.row >= 0) then
          begin
            CountSelectionGrid := info.cell.row;
            e_tvname.value := st.datacols[0][CountSelectionGrid];
            e_freqtv.value := StrToInt(st.datacols[1][CountSelectionGrid]);
            e_normtv.value := st.datacols[2][CountSelectionGrid];
          end;
        end;
      end;
    end;
  end;
end;

procedure tmainfo.on_addchanell(const sender: TObject);
var
  tvd: TTVData;
begin
  tvd.Freq := e_freqtv.value;
  tvd.Norm := StrToTvNorm(e_normtv.value);
  tvd.Name := e_tvname.value;
  player.ListofChanell.AddItem(tvd);
  st.rowcount := st.rowcount + 1;
  st.datacols[0][st.rowcount - 1] := e_tvname.value;
  st.datacols[1][st.rowcount - 1] := inttostr(e_freqtv.value);
  st.datacols[2][st.rowcount - 1] := e_normtv.value;
  SaveChannel();
end;

procedure tmainfo.on_deletechanell(const sender: TObject);
begin
  st.deleterow(st.row);
  player.ListofChanell.RemoveItem(CountSelectionGrid);
  SaveChannel();
end;

procedure tmainfo.SaveChannel();
var
  i: integer;
begin
  sc := TIniFile.Create(CFGChannelsName);
  sc.WriteInteger('TVcount', 'Count', mainfo.st.rowcount);
  for i := 1 to player.ListofChanell.Count do
  begin
    sc.WriteString('TV' + inttostr(i), 'tvname', player.ListofChanell.GetData(i - 1).Name);
    sc.WriteInteger('TV' + inttostr(i), 'freqtv', player.ListofChanell.GetData(i - 1).Freq);
    Sc.WriteString('TV' + inttostr(i), 'normtv', tvstr[player.ListofChanell.GetData(i - 1).Norm]);
  end;
  sc.WriteInteger('TVcount', 'Count', mainfo.st.rowcount);
  sc.free;
end;

procedure tmainfo.on_setchanneltv(const sender: TObject);
var
  idata: TTVData;
begin
  st.datacols[0][CountSelectionGrid] := e_tvname.value;
  st.datacols[1][CountSelectionGrid] := inttostr(e_freqtv.value);
  st.datacols[2][CountSelectionGrid] := e_normtv.value;

  idata.Name := e_tvname.value;
  idata.Freq := e_freqtv.value;
  idata.Norm := StrToTvNorm(e_normtv.value);
  player.stop;
  player.ListofChanell.SetData(CountSelectionGrid, idata);
  player.opentv;
  player.SetChannelByfreq(player.ListofChanell.GetData(CountSelectionGrid).Freq);
  player.SetChannelNorm(player.ListofChanell.GetData(CountSelectionGrid).Norm);
  SaveChannel();
end;

procedure tmainfo.on_set_webcam2(const sender: TObject; var avalue: msestring;
  var accept: Boolean);
begin
  setsfo.edit_webcam.value := avalue;
  setsfo.on_setwebcameradevice(sender, avalue, accept);
end;

procedure tmainfo.on_updatedivices2(const sender: TObject);
begin
  setsfo.on_update_devices(sender);
end;

procedure tmainfo.on_setplayplaylist_up_down(const sender: TObject);
begin
  case b_forward.tag of
    0:
      begin
        player.playplaylistback := TRUE;
        b_forward.imagenr := 26;
        b_forward.tag := 1;
      end;
    1:
      begin
        player.playplaylistback := FALSE;
        b_forward.imagenr := 25;
        b_forward.tag := 0;
      end;
  end;
end;

procedure tmainfo.on_setrepeatplaylist(const sender: TObject);
begin
  case b_repeat.tag of
    0:
      begin
        player.repeatplaylist := TRUE;
        b_repeat.imagenr := 27;
        b_repeat.tag := 1;
      end;
    1:
      begin
        player.repeatplaylist := FALSE;
        b_repeat.imagenr := 28;
        b_repeat.tag := 0;
      end;
  end;
end;

procedure tmainfo.on_getpreview_pl(const sender: TObject);
begin
  if wg_playlist.row < 0 then exit;
  getpreview(se_url[wg_playlist.row]);
end;

procedure tmainfo.getpreview(fname: string);
var
  fs: tsearchrec;
  curdir: string;

  procedure get__(I, t1: integer; t2: real);
  begin
    player_preview.getpreview(fname, curdir, t1);
    with previewfo do
      if findfirst(curdir + '*1.png', faanyfile, fs) = 0
        then (FINDCOMPONENT('im' + INTTOSTR(I)) as TIMAGE).bitmap.loadfromfile(curdir + fs.name)
      else (FINDCOMPONENT('im' + INTTOSTR(I)) as TIMAGE).bitmap := im0.bitmap;
    deletefile(curdir + fs.name);
    previewfo.pb.value := t2;
    previewfo.pb.update;
  end;

begin
  curdir := dirtowin(getcurrentdir);

  previewfo.im1.bitmap.clear;
  previewfo.im2.bitmap.clear;
  previewfo.im3.bitmap.clear;
  previewfo.im4.bitmap.clear;
  previewfo.im5.bitmap.clear;
  previewfo.im6.bitmap.clear;
  previewfo.pb.value := 0;
  previewfo.pb.visible := true;
  previewfo.bringtofront;
  previewfo.show;
  previewfo.l_.caption := fname;
  application.processmessages;
  try
    get__(1, 50, 0.16);
    get__(2, 100, 0.33);
    get__(3, 150, 0.49);
    get__(4, 200, 0.66);
    get__(5, 250, 0.82);
    get__(6, 300, 1);
    previewfo.pb.visible := false;
    previewfo.bringtofront;
    previewfo.show;
  except

  end;
end;

procedure tmainfo.on_showdubble(const sender: TObject);
begin
  if dubblefo = nil then exit;
  dubblefo.left := mainfo.left + s_video.width - dubblefo.width + mainfoframewidth;
  dubblefo.top := mainfo.top + s_video.height + s_top.height - dubblefo.height + mainfoframewidth + mainfoframewidth;
  if (b_library.tag = 1) and (button_windowstyle.tag = 0)
    then
  begin
    if not dubblefo.visible then dubblefo.show;
  end
  else dubblefo.visible := false;
end;

procedure tmainfo.on_deactivateform(const sender: TObject);
begin
  dubblefo.visible := false;
end;

procedure tmainfo.loadshemas;
var
  fs: tsearchrec;
  s: string;
begin
  setsfo.edit_scheme.dropdown.cols.clear;
  if findfirst(shemepath + '*.xelsheme', faanyfile, fs) = 0 then
    repeat
      s := fs.name;
      delete(s, system.pos('.xelsheme', s), length('.xelsheme'));
      setsfo.edit_scheme.dropdown.cols.addrow(msestring(s));
    until findnext(fs) <> 0;
  findclose(fs);
end;

procedure tmainfo.loadicons;
var
  fs: tsearchrec;
  s: string;
begin
  setsfo.edit_icons.dropdown.cols.clear;
  if findfirst(shemepath + 'icons/*', faanyfile, fs) = 0 then
    repeat
      s := fs.name;
      if (s = '.') or (s = '..') or (s = '.') then continue;
      setsfo.edit_icons.dropdown.cols.addrow(msestring(s));
    until findnext(fs) <> 0;
  findclose(fs);
end;

procedure tmainfo.on_timer_animate(const sender: TObject);
begin
  inc(n_animate);
  if n_animate = im_animate.count * 4 then n_animate := 0;
  if n_animate < im_animate.count then mainfo.b_animate.imagenr := n_animate;
end;

procedure tmainfo.button_screenshot_onexecute(const sender: TObject);
begin
  LLog.Add('tmainfo.button_screenshot_onexecute()');
  player.screenshot;
end;

procedure tmainfo.on_set_dvd_device(const sender: TObject; var avalue: Boolean; var accept: Boolean);
begin
  ddd_dvdpath.enabled := not avalue;
  ddd_audioCDdevice.enabled := avalue;
  trichbutton5.enabled := avalue;
end;

procedure tmainfo.on_set_cdrom_device3(const sender: TObject; var avalue: msestring; var accept: Boolean);
begin
  setsfo.edit_cdrom.value := avalue;
  setsfo.on_setaudiocddevice(sender, avalue, accept);
end;

procedure tmainfo.on_keyup_form(const sender: twidget; var ainfo: keyeventinfoty);
var
  r: realty;
  i: integer;
begin
  if ainfo.key = KEY_SPACE then on_play(sender);
  if ainfo.key = KEY_M then on_mute(sender);
  if ainfo.key = KEY_Z then on_prev(sender);
  if ainfo.key = KEY_X then on_play(sender);
  if ainfo.key = KEY_C then on_stop(sender);
  if ainfo.key = KEY_V then on_next(sender);
  if ainfo.key = KEY_F then on_fullscreen(sender);
  if ainfo.key = KEY_L then on_showlibrary(b_library);
  if ainfo.key = KEY_S then button_windowstyle_onexecute(button_windowstyle);
  if player.playing then

  begin

    begin
      i := player.position;
      if ainfo.key = key_Right then
        if i + 5 <= player.length then player.position := i + 5;
      if ainfo.key = key_Left then
        if i - 5 >= 0 then player.position := i - 5;
      if player.length > 0 then
        sl_position.value := player.position / player.length;
    end;
  end;
end;

procedure tmainfo.on_mousewheelform(const sender: twidget; var ainfo: mousewheeleventinfoty);
var
  r: realty;
  i: integer;
begin
  if player.playing then
  begin
    LLog.Add(format('tmainfo.on_mousewheelform(%s,)', [sender.classname]));
    i := player.volume;
    if ainfo.wheel = mw_up then
      if i + 5 <= 100 then player.volume := player.volume + 5;
    if ainfo.wheel = mw_down then
      if i - 5 >= 0 then player.volume := player.volume - 5;
  end;
end;

procedure tmainfo.on_get_icon(const sender: TObject; const ainfo: fileinfoty; var imagelist: timagelist; var imagenr: integer);

begin

end;

procedure tmainfo.on_show_flv_popup(const sender: TObject);
var
  gr: mouseeventinfoty;
begin
  gr.pos.x := 0;
  gr.pos.y := b_menuflv.height;
  pm_flv.show(b_menuflv, gr);
end;

procedure tmainfo.on_show_flv_menu(const sender: tcustommenu);
var
  itm: tmenuitem;
  fs: tsearchrec;
  i, y: integer;
  path: string;
  sl: tstringlist;
  s: string;
  Drives: array[1..33] of string;
  count: integer;
begin
  pm_flv.menu.items[3].submenu.clear;
{$IFDEF windows}
  try
    Count := GetLogicalDriveStrings(Drives);
    if count > 33 then count := 33;
    for I := 1 to count do
    begin

      itm := tmenuitem.create;
      itm.tag := 0;
      itm.caption := Drives[i];
      itm.hint := Drives[i] + '/';
      itm.state := [as_localcaption, as_localonexecute];
      itm.onexecute := @on_open_dir_pm;
      pm_flv.menu.items[3].submenu.insert(0, itm);
    end;
  except
    on E: exception do
    begin
    end;
  end;
{$ENDIF}
{$IFDEF linux}

  sl := tstringlist.create;
  sl.loadfromfile('/etc/mtab');
  if sl.count > 0 then
    for i := 0 to sl.count - 1 do
    begin
      s := trimleft(sl[i]);
      if system.pos('/dev/', s) = 1 then
      begin
        delete(s, 1, system.pos(' ', s));
        s := copy(trim(s), 1, system.pos(' ', s) - 1);
        if trim(s) = '/' then continue;
        while system.pos('\040', s) > 0 do
        begin
          y := system.pos('\040', s);
          delete(s, y, length('\040'));
          insert(' ', s, y);
        end;
        itm := tmenuitem.create;
        itm.tag := 0;
        itm.caption := extractfilename(s);
        itm.hint := s + '/';

        itm.state := [as_localcaption, as_localonexecute];
        itm.onexecute := @on_open_dir_pm;
        pm_flv.menu.items[3].submenu.insert(0, itm);
      end;
    end;
  sl.free;
{$ENDIF}
end;

procedure tmainfo.on_open_dir_pm(const sender: TObject);
var
  s: string;
begin
  s := dd_dir.value;
  dd_dir.value := (sender as tmenuitem).hint;
  if not setsfo.be_showonlymediafiles.value
    then
  begin
    try
      flv.directory := (sender as tmenuitem).hint;
      flv.readlist;
    except
      dd_dir.value := s;
      flv.directory := s;
      flv.readlist;
      show_ask(mainfo, 'Drive not ready', '', false, false, false);
    end;
  end
  else readlist((sender as tmenuitem).hint);
end;

procedure tmainfo.loadlast(dir: string);
var
  sl: tstringlist;
  i: integer;
  f: textfile;
  s1, s2, s3: string;
begin
  assignfile(f, dir + '__LAST_PLAYED__');
  if not fileexists(dir + '__LAST_PLAYED__') then rewrite(f) else reset(f);
  while not eof(f) do
  begin
    readln(f, s1);
    readln(f, s2);
    readln(f, s3);
    wg_last.insertrow(0);
    se_name_last[0] := s1;
    se_url_last[0] := s2;
    se_emblem_last[0] := s3;
  end;
  closefile(f);
end;

procedure tmainfo.on_last_played_change(const sender: tfilechangenotifyer; const info: filechangeinfoty);
begin
  loadlast(dirtowin(SETSFO.edit_playlistsdir.value));
end;

procedure tmainfo.on_createitemflv(const sender: tcustomitemlist; var item: tlistedititem);
var
  s: string;
begin

  item.free;

end;

procedure tmainfo.on_ckeckfile(const sender: TObject; const streaminfo: dirstreaminfoty; const fileinfo: fileinfoty; var accept: Boolean);
begin
end;

procedure tmainfo.on_listread_flv(const sender: TObject);
begin
end;

procedure tmainfo.readlist(dir: string);
var
  fs: tsearchrec;
  r, i: integer;
  s: string;
  sl1, sl2: tstringlist;
begin
  if not fileexists(filemagic) then
  begin
    show_ask(get_mainwindow, 'Magic file not found.', '', false, false, false);
    exit;
  end;
  wg_fl.clear;
  wg_fl.rowcount := 1;
  se_fname[0] := 'Building, please wait ...';
  APPLICATION.PROCESSMESSAGES;
  sl1 := tstringlist.create;
  sl2 := tstringlist.create;
  if findfirst(dir + '*', faanyfile, fs) = 0 then
    repeat
      if (fs.name = '') or (fs.name = '.') or (fs.name = '..') then continue;
      if (fadirectory and fs.attr) <> 0 then
        sl1.add(dir + fs.name)
      else
      begin
{$IFDEF linux}
        s := '';
{$ENDIF}
{$IFDEF windows}
        s := dir + fs.name;
{$ENDIF}
        if (system.pos('video', s) = 1) or (system.pos('audio', s) = 1) then
          sl2.add(dir + fs.name);
      end;
    until findnext(fs) <> 0;
  findclose(fs);
  sl1.sort;
  sl2.sort;
  wg_fl.clear;
  wg_fl.rowcount := sl1.count + sl2.count;
  if sl1.count > 0 then
    for i := 0 to sl1.count - 1 do
    begin
      se_fname[i] := extractfilename(sl1[i]);
      se_furl[i] := sl1[i] + '/';
      di_fl.gridvalue[i] := 0;
      application.processmessages;
    end;
  if sl2.count > 0 then
    for i := 0 to sl2.count - 1 do
    begin
      se_fname[sl1.count + i] := extractfilename(sl2[i]);
      se_furl[sl1.count + i] := sl2[i];
      di_fl.gridvalue[sl1.count + i] := 2;
      application.processmessages;
    end;
  sl1.free;
  sl2.free;
end;

procedure tmainfo.on_fl_event(const sender: TObject; var info: celleventinfoty);
begin
  if iscellclick(info, [CCR_DBLCLICK]) then
  begin
    if wg_fl.row < 0 then exit;
    if extractfilename(se_furl[wg_fl.row]) <> se_fname[wg_fl.row]
      then
    begin
      dd_dir.value := se_furl[wg_fl.row];
      readlist(se_furl[wg_fl.row]);
    end
    else on_addfilefromdisk(b_open_from_flv);
  end;
end;

procedure tmainfo.on_getpreview_flv(const sender: TObject);
begin
  if flv.focusedindex < 0 then exit;
  getpreview(flv.directory + flv.items[flv.focusedindex].caption);
end;

procedure tmainfo.on_getpreview_last(const sender: TObject);
begin
  if wg_last.row < 0 then exit;
  getpreview(se_url_last[wg_last.row]);
end;

procedure tmainfo.on_showtrackinfo(const sender: TObject);

var
  i, r: integer;
  s: string;
begin

  r := mainfo.wg_playlist.row;
  if r < 0 then exit;
  s := mainfo.se_url[r];
{$IFDEF windows}
  for i := 1 to length(s) do if s[i] = '/' then s[i] := '\';
  if s[1] = '/' then delete(s, 1, 1);
{$ENDIF}
  mp3f := TMP3File.create;
  mp3f.ReadTag(s);
  fileinfofo.SE_FILE.value := s;
  fileinfofo.se_artist.value := mp3f.Artist;
  fileinfofo.se_Album.value := mp3f.Album;
  fileinfofo.se_Title.value := mp3f.Title;
  fileinfofo.se_Track.value := mp3f.Track;
  fileinfofo.se_Year.value := mp3f.Year;
  fileinfofo.se_Comment.value := mp3f.Comment;
  fileinfofo.se_Genre.value := mp3f.Genre;
  fileinfofo.se_Playtime.value := mp3f.Playtime;
  fileinfofo.se_Playlength.value := inttostr(mp3f.Playlength);
  fileinfofo.se_Bitrate.value := inttostr(mp3f.Bitrate);
  fileinfofo.se_Samplerate.value := inttostr(mp3f.Samplerate);
  mp3f.free;
  fileinfofo.show(true);
end;

procedure tmainfo.on_player_preview_startplay(const Sender: TObject; videoW: integer; videoH: integer; msg: AnsiString);
begin
  player_preview.stop;
end;

procedure tmainfo.on_player_info_tartplay_event(const Sender: TObject; videoW: integer; videoH: integer; msg: AnsiString);
begin

  player_info.stop;
end;

procedure tmainfo.on_beforedragbegin_pl(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var processed: Boolean);
var
  co1: gridcoordty;
  r: integer;
  s: string;
begin
  if (asender = wg_playlist) then
    with twidgetgrid(asender) do
    begin
      if cellatpos(apos, co1) = ck_data then
      begin
        s := '';
        for r := 0 to wg_playlist.rowhigh do
          if wg_playlist.datacols[0].selected[r] then s := inttostr(r) + ',' + s;
        tstringdragobject.create(asender, adragobject, apos).data := s;
        processed := true;
      end;
    end;

  if (asender.classname = 'twidgetgrid') then
    with twidgetgrid(asender) do
    begin
      if cellatpos(apos, co1) = ck_data then
      begin
        tstringdragobject.create(asender, adragobject, apos).data := inttostr(co1.row);
        processed := true;
      end;
    end;

  if asender.classname = 'tfilelistview' then
    with tfilelistview(asender) do
    begin
      if cellatpos(apos, co1) = ck_data then
      begin
        tstringdragobject.create(asender, adragobject, apos).data := inttostr(co1.row);
        processed := true;
      end;
    end;

end;

procedure tmainfo.on_beforedragdrop_pl(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var processed: Boolean);
var
  co1: gridcoordty;
  s: string;
  r, r2, r_old: integer;
begin
  if adragobject is tstringdragobject then
  begin
    if (adragobject.sender = wg_playlist) then
      with twidgetgrid(asender) do
      begin
        if (asender = adragobject.sender) then
        begin
          if (cellatpos(apos, co1) = ck_data) then
          begin
            s := tstringdragobject(adragobject).data;
            r := 0; r2 := 0;
            while system.pos(',', s) > 0 do
            begin
              r_old := strtoint(copy(s, 1, system.pos(',', s) - 1));
              moverow(r_old + r2, co1.row - r);
              player.playlist.move(r_old + r2, co1.row - r);
              delete(s, 1, system.pos(',', s));
              if r_old <= co1.row then inc(r) else inc(r2);
            end;
            findcurrentruck(false);
          end;
        end;
      end;

    processed := true;
    if (adragobject.sender = flv) or (adragobject.sender = wg_fl) then on_addfilefromdisk(b_add_from_flv);
    if (adragobject.sender = wg_myplaylists) then on_add_myplaylist_To_Playlist(b_add_myplaylist_To_Playlist);
    if (adragobject.sender = wg_last) then on_add_from_lastplayed(b_add_from_lastplayed);
    if (adragobject.sender = wg_tv) or (adragobject.sender = wg_radio) then
    begin
      if tw_main.activepage = tp_radio then on_add_radioTVchanel_to_playlist(b_add_from_radiolist);
      if tw_main.activepage = tp_tv then on_add_radioTVchanel_to_playlist(b_add_from_tvlist);
    end;
  end;
end;

procedure tmainfo.on_beforedragover_pl(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var accept: Boolean; var processed: Boolean);
begin
  if adragobject is tstringdragobject then
  begin
    accept := true;
    processed := true;
  end;
end;

procedure tmainfo.on_dragbegin(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var processed: Boolean);
begin
  processed := true;
end;

procedure tmainfo.on_dragdrop(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var processed: Boolean);
var
  ar1: msestringarty;
  po1: pdropfilesty;
  pc1, pc2: pchar;
  pw1, pw2: pmsechar;
  pend: pointer;
  i: integer;
  fs: tsearchrec;
  s: string;
begin
  if adragobject is tmimedragobject then
  begin
    with tmimedragobject(adragobject) do
    begin
      case wantedformatindex of
        0:
          begin
            ar1 := breaklines(msestring(trim(data)));
          end;
        1:
          begin
            if length(data) > sizeof(dropfilesty) then
            begin
              po1 := pointer(data);
              pend := pointer(po1) + length(data);
              if po1^.fwide then
              begin
                pw1 := pointer(po1) + po1^.pfiles;
                while pw1 < pend do
                begin
                  pw2 := pw1;
                  while (pw1 < pend) and (pw1^ <> #0) do
                    inc(pw1);
                  additem(ar1, psubstr(pw2, pw1));
                  if pw1^ <> #0 then
                  begin
                    break;
                  end;
                  inc(pw1);
                  if pw1^ = #0 then
                    break;
                end;
              end else
              begin
                pc1 := pointer(po1) + po1^.pfiles;
                while pc1 < pend do
                begin
                  pc2 := pc1;
                  while (pc1 < pend) and (pc1^ <> #0) do
                    inc(pc1);
                  additem(ar1, psubstr(pc2, pc1));
                  if pc1^ <> #0 then
                    break;
                  inc(pc1);
                  if pc1^ <> #0 then
                    break;
                end;
              end;
            end;
          end;
      end;
      if high(ar1) > -1 then
        for i := 0 to high(ar1) do
        begin
          s := urldecode(string(ar1[i]));
          if system.pos('file://', s) = 1 then delete(s, 1, 7);
          if findfirst(s, faanyfile, fs) = 0 then
            repeat
              if (fs.name = '') or (fs.name = '.') or (fs.name = '..') then
                continue;
              if fs.Attr and fadirectory <> 0 then
                adddir(s)
              else
                addfile(extractfilename(s), s, '');
            until findnext(fs) <> 0;
          findclose(fs);
        end;
    end;
    processed := true;
  end;
end;

procedure tmainfo.on_dragover(const asender: TObject; const apos: pointty; var adragobject: tdragobject; var accept: Boolean; var processed: Boolean);
begin
  if adragobject is tmimedragobject then
  begin
    with tmimedragobject(adragobject) do
    begin
      actions := [dnda_copy];
      if checkformat(msestringarty(knownformats)) then
      begin
        try
          if (widgetatpos(apos) = wg_playlist) or (widgetatpos(apos).parentwidget.name = 'wg_playlist') then accept := true;
          processed := true;
        except
        end;
      end;
    end;
  end;
end;

procedure tmainfo.on_dblclick_audiocd(const sender: TObject; var info: celleventinfoty);
begin
  if iscellclick(info, [CCR_DBLCLICK]) then
    if sg_cdplaylist.row >= 0 then
    begin
      player.tracknum := sg_cdplaylist.row;

      player.openaudiocd(player.tracknum);
    end;
end;

procedure tmainfo.on_getaudiocdplaylist(const Sender: TObject; msg: AnsiString);
var
  i: integer;
begin
  sg_cdplaylist.clear;
  sg_cdplaylist.rowcount := player.playlist.count;
  if player.playlist.count > 0 then
    for i := 0 to player.playlist.count - 1 do
      sg_cdplaylist.datacols[1][i] := player.playlist[i];
end;

end.
