
unit Log;

interface

uses
  SysUtils;

type
  TLog = class
  private
    FName: TFileName;
  public
    constructor Create; overload;
    constructor Create(const AName: TFileName); overload;
    procedure Add(const ALine: string; const ARewrite: boolean = FALSE);
  end;

var
  LLog: TLog;

implementation

const
  CBuildInfo = 'FPC ' + {$I %FPCVERSION%} + ' ' + {$I %DATE%} + ' ' + {$I %TIME%} + ' ' + {$I %FPCTARGETOS%} + '-' + {$I %FPCTARGETCPU%};

constructor TLog.Create;
begin
  inherited;
  FName := ChangeFileExt(extractfilename(paramstr(0)), '.log');
end;

constructor TLog.Create(const AName: TFileName);
begin
  inherited Create;
  FName := AName;
end;

procedure TLog.Add(const ALine: string; const ARewrite: boolean);
var
  LLog: TextFile;
  LTime: string;
begin
  Assign(LLog, FName);
  if ARewrite or not FileExists(FName) then
    Rewrite(LLog)
  else
    Append(LLog);
  LTime := DateTimeToStr(Now);
  WriteLn(LLog, LTime, ' ', ALine);
  Close(LLog);
end;

initialization
  LLog := TLog.Create;
  LLog.Add(CBuildInfo, TRUE);

finalization
  LLog.Free;

end.
