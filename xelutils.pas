
unit xelutils;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

function dirtowin(str: string): string;
function whoami: string;
function httpencode(const AStr: string): string;
function myencodeurl(source: string): string;
function urldecode(const s: string): string;
function myremovedir(sDir: string): Boolean;

implementation

uses
  sysutils,
  classes,
  process,
  msetypes,
  msefileutils;

function dirtowin(str: string): string;
{$IFDEF windows}
var
  i: integer;
{$ENDIF}
begin
  if length(str) > 0 then
{$IFDEF linux}
    if str[length(str)] <> '/' then str := str + '/';
{$ENDIF}
{$IFDEF windows}
  if str[1] = '/' then delete(str, 1, 1);
  if length(str) > 0 then
    for i := 1 to length(str) do
      if str[i] = '/' then str[i] := '\';
  if length(str) > 0 then
    if str[length(str)] <> '\' then str := str + '\';
{$ENDIF}
  result := str;
end;

function whoami: string;
var
  lprocess: tprocess;
  sl: tstringlist;
begin
  sl := tstringlist.create;
  lprocess := tprocess.create(nil);
  lprocess.executable := 'whoami';
  lprocess.options := lprocess.options + [powaitonexit, pousepipes];
  lprocess.execute;
  sl.loadfromstream(lprocess.Output);
  result := trim(sl.text);
  lprocess.free;
  sl.free;
end;

function httpencode(const AStr: string): string;
const
  NoConversion = ['A'..'Z', 'a'..'z', '*', '@', '.', '_', '-'];
var
  Sp, Rp: PChar;
begin
  setlength(Result, length(AStr) * 3);
  Sp := PChar(AStr);
  Rp := PChar(Result);
  while Sp^ <> #0 do
  begin
    if Sp^ in NoConversion then
      Rp^ := Sp^
    else
      if Sp^ = ' ' then
        Rp^ := '+'
      else
      begin
        FormatBuf(Rp^, 3, '%%%.2x', 6, [Ord(Sp^)]);
        Inc(Rp, 2);
      end;
    Inc(Rp);
    Inc(Sp);
  end;
  setlength(Result, Rp - PChar(Result));
end;

function myencodeurl(source: string): string;
var
  i: integer;
begin
  result := '';
  for i := 1 to length(source) do
    if not (source[i] in ['A'..'Z', 'a'..'z', '0', '1'..'9', '-', '_', '~', '.'])
      then result := result + '%' + inttohex(ord(source[i]), 2) else result := result + source[i];
end;

function urldecode(const s: string): string;
var
  sAnsi: string;
  sUtf8: string;
  sWide: WideString;
  i, len: Cardinal;
  ESC: string[2];
  CharCode: integer;
  c: char;
begin
  sAnsi := PChar(s);
  setlength(sUtf8, length(sAnsi));
  i := 1;
  len := 1;
  while i <= Cardinal(length(sAnsi)) do
  begin
    if sAnsi[i] <> '%' then
    begin
      if sAnsi[i] = '+' then
      begin
        c := ' ';
      end else
      begin
        c := sAnsi[i];
      end;
      sUtf8[len] := c;
      Inc(len);
    end else
    begin
      Inc(i);
      ESC := Copy(sAnsi, i, 2);
      Inc(i, 1);
      try
        CharCode := StrToInt('$' + ESC);
        c := Char(CharCode);
        sUtf8[len] := c;
        Inc(len);
      except
      end;
    end;
    Inc(i);
  end;
  Dec(len);
  setlength(sUtf8, len);
  sWide := UTF8Decode(sUtf8);
  len := length(sWide);
  Result := string(sWide);
end;

function myremovedir(sDir: string): Boolean;
var
  iIndex: integer;
  SearchRec: TSearchRec;
  sFileName: string;
  c: msechar;
begin
  Result := False;
{$IFDEF mswindows}
  c := '\';
  if sdir[1] = '/' then delete(sdir, 1, 1);
  for iindex := 1 to length(sdir) do
    if sdir[iindex] = '/' then sdir[iindex] := '\';
{$ENDIF}
{$IFDEF linux}
  c := '/';
{$ENDIF}

  sDir := sDir + c + '*';
  iIndex := FindFirst(sDir, faAnyFile, SearchRec);

  while iIndex = 0 do
  begin
    sFileName := ExtractFileDir(sDir) + c + SearchRec.Name;
    if ((faDirectory and SearchRec.Attr) <> 0) then
    begin
      if (SearchRec.Name <> '') and (SearchRec.Name <> '.') and (SearchRec.Name <> '..') then
        myremovedir(sFileName);
    end else
    begin
{$IFDEF mswindows}
      if (faArchive and SearchRec.Attr) <> 0 then FileSetAttr(sFileName, faArchive);
{$ENDIF}
      if (SearchRec.Name <> '') and (SearchRec.Name <> '.') and (SearchRec.Name <> '..') then
        if not tryDeleteFile(sFileName) then
        begin

        end;
    end;
    iIndex := FindNext(SearchRec);
  end;

  FindClose(SearchRec);
{$IFDEF mswindows}
  RemoveDir(msestring(ExtractFileDir(sDir)));
{$ENDIF}
{$IFDEF inux}
  RemoveDir(ExtractFileDir(sDir));
{$ENDIF}
  Result := True;
end;

end.
