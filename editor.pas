
unit editor;

{$IFDEF FPC}{$MODE objfpc}{$H+}{$ENDIF}

interface

uses
  mseglob,
  mseguiglob,
  mseguiintf,
  mseapplication,
  msestat,
  msemenus,
  msegui,
  msegraphics,
  msegraphutils,
  mseevent,
  mseclasses,
  mseforms,
  msesimplewidgets,
  msewidgets,
  msecolordialog,
  msedataedits,
  mseedit,
  mseifiglob,
  msestrings,
  msetypes,
  msegraphedits,
  msebitmap,
  msedatanodes,
  msefiledialog,
  msegrids,
  mselistbrowser,
  msesys,
  msestatfile,
  msetimer,
  msesplitter,
  msedock,
  mseimage;

type
  teditshemefo = class(tmseform)
    tscrollbox2: tscrollbox;
    tlabel27: tlabel;
    tcoloredit27: tcoloredit;
    tintegeredit8: tintegeredit;
    tcoloredit26: tcoloredit;
    tcoloredit25: tcoloredit;
    tcoloredit28: tcoloredit;
    tcoloredit31: tcoloredit;
    tcoloredit15: tcoloredit;
    tcoloredit32: tcoloredit;
    tcoloredit18: tcoloredit;
    tlabel34: tlabel;
    tlabel23: tlabel;
    tlabel24: tlabel;
    tcoloredit17: tcoloredit;
    tcoloredit16: tcoloredit;
    tcoloredit8: tcoloredit;
    tcoloredit7: tcoloredit;
    tintegeredit4: tintegeredit;
    tcoloredit6: tcoloredit;
    tlabel18: tlabel;
    tintegeredit9: tintegeredit;
    tcoloredit35: tcoloredit;
    tlabel31: tlabel;
    tlabel26: tlabel;
    tcoloredit24: tcoloredit;
    tintegeredit7: tintegeredit;
    tcoloredit23: tcoloredit;
    tcoloredit34: tcoloredit;
    tcoloredit33: tcoloredit;
    tcoloredit22: tcoloredit;
    tcoloredit19: tcoloredit;
    tcoloredit20: tcoloredit;
    tintegeredit6: tintegeredit;
    tcoloredit21: tcoloredit;
    tlabel25: tlabel;
    tbooleanedit2: tbooleanedit;
    tfilenameedit2: tfilenameedit;
    tcoloredit30: tcoloredit;
    tlabel32: tlabel;
    tcoloredit13: tcoloredit;
    tcoloredit14: tcoloredit;
    tlabel22: tlabel;
    tcoloredit11: tcoloredit;
    tcoloredit12: tcoloredit;
    tlabel21: tlabel;
    tcoloredit10: tcoloredit;
    tintegeredit5: tintegeredit;
    tcoloredit9: tcoloredit;
    tlabel20: tlabel;
    tintegeredit2: tintegeredit;
    tcoloredit3: tcoloredit;
    tlabel15: tlabel;
    tlabel14: tlabel;
    tcoloredit1: tcoloredit;
    tintegeredit1: tintegeredit;
    tcoloredit2: tcoloredit;
    fne_form_image: tfilenameedit;
    tbooleanedit1: tbooleanedit;
    tlabel30: tlabel;
    tlabel19: tlabel;
    tlabel17: tlabel;
    tlabel28: tlabel;
    l_Width: tlabel;
    tlabel13: tlabel;
    tlabel33: tlabel;
    tcoloredit29: tcoloredit;
    tcoloredit5: tcoloredit;
    tintegeredit3: tintegeredit;
    tcoloredit4: tcoloredit;
    tlabel16: tlabel;
    trichbutton5: trichbutton;
    trichbutton6: trichbutton;
    se_shemename: tstringedit;
    tlabel12: tlabel;
    sf: tstatfile;
    timer_resize: ttimer;
    timer_move: ttimer;
    s_top: tspacer;
    trichbutton1: trichbutton;
    tdockhandle1: tdockhandle;
    b_maximize: trichbutton;
    tspacer1: tspacer;
    tspacer2: tspacer;
    tspacer3: tspacer;
    procedure on_save(const sender: TObject);
    procedure on_apply(const sender: TObject);
    procedure on_resize(const sender: TObject);
    procedure on_move(const sender: TObject);
    procedure on_childmouseevens(const sender: twidget;
      var ainfo: mouseeventinfoty);
    procedure on_close(const sender: TObject);
    procedure on_maximize(const sender: TObject);
  end;

var
  editshemefo: teditshemefo;
  moveXX, moveYY: integer;

implementation

uses
  editor_mfm,
  main,
  top,
  bottom,
  playlist,
  askform,
  settings,
  viewform,
  chanelform,
  copyform,
  aboutform,
  loadform,
  sysutils,
  msefileutils,
  settings2,
  previewform;

procedure teditshemefo.on_save(const sender: TObject);
begin
  if (se_shemename.value) = '' then
    mainfo.show_ask(editshemefo, 'Empty scheme name!', '', false, false, false)
  else
  try
    sf.writestat(shemepath + se_shemename.value + '.xelsheme');
  except
    mainfo.show_ask(editshemefo, 'Incorrect scheme name!', '', false, false, false)
  end;
  mainfo.loadshemas;
end;

procedure teditshemefo.on_apply(const sender: TObject);
var
  c: colorty;
  z: integer;
  b: boolean;
  f: tfont;

  procedure setformcolor(c: colorty; maintoo: boolean);
  var
    i: integer;
  begin
    topfo.color := c;
    bottomfo.color := c;
    playlistfo.color := c;
    setsfo.color := c;
    set2fo.color := c;
    askfo.color := c;
    chanelfo.color := c;
    copyfo.color := c;
    aboutfo.color := c;
    previewfo.color := c;
    viewfo.color := c;

    if maintoo then
      mainfo.color := c
    else
      mainfo.color := cl_transparent;
  end;

begin
  mainfo.face.image.clear;
  topfo.face.image.clear;
  bottomfo.face.image.clear;
  playlistfo.face.image.clear;
  setsfo.face.image.clear;
  set2fo.face.image.clear;
  askfo.face.image.clear;
  chanelfo.face.image.clear;
  copyfo.face.image.clear;
  aboutfo.face.image.clear;
  viewfo.face.image.clear;

  c := tcoloredit2.value;

  if fne_form_image.value > '' then
  try
    b := true;
    if not fileexists(main.shemeimagepath + extractfilename(fne_form_image.value))
      then
      if not trycopyfile(fne_form_image.value, main.shemeimagepath + extractfilename(fne_form_image.value))
        then b := false;

    if b then fne_form_image.value := main.shemeimagepath + extractfilename(fne_form_image.value);

    mainfo.face.image.loadfromfile(fne_form_image.value);
    if not tbooleanedit1.value then
    begin
      topfo.face.image.loadfromfile(fne_form_image.value);
      bottomfo.face.image.loadfromfile(fne_form_image.value);
      playlistfo.face.image.loadfromfile(fne_form_image.value);
      setsfo.face.image.loadfromfile(fne_form_image.value);
      set2fo.face.image.loadfromfile(fne_form_image.value);
      askfo.face.image.loadfromfile(fne_form_image.value);
      chanelfo.face.image.loadfromfile(fne_form_image.value);
      copyfo.face.image.loadfromfile(fne_form_image.value);
      aboutfo.face.image.loadfromfile(fne_form_image.value);
      previewfo.face.image.loadfromfile(fne_form_image.value);
      viewfo.face.image.loadfromfile(fne_form_image.value);
      setformcolor(cl_transparent, true);
    end
    else setformcolor(c, false);
  except
    loadfo.hide;
    mainfo.show_ask(editshemefo, 'Error while loading', '', false, false, false);
    if not fileexists(fne_form_image.value) then writeln(' not found');
    writeln('"', fne_form_image.value, '"');
    setformcolor(c, true);
  end else
  begin
    setformcolor(c, true);
  end;

  c := tcoloredit3.value;
  mainfo.font.color := c;
  mainfo.l_pos.font.color := c;
  mainfo.l_webcam_capt.font.color := c;
  mainfo.l_audiocd_capt.font.color := c;
  mainfo.l_dvd_capt.font.color := c;
  mainfo.l_tvtuner_capt.font.color := c;
  mainfo.s_top.frame.font.color := c;
  aboutfo.l_.font.color := c;
  topfo.font.color := c;
  bottomfo.font.color := c;
  playlistfo.font.color := c;
  setsfo.font.color := cl_black;
  setsfo.ttabwidget1.font.color := c;
  set2fo.font.color := c;
  askfo.font.color := c;
  chanelfo.font.color := c;
  copyfo.font.color := c;
  aboutfo.font.color := c;
  previewfo.font.color := c;
  mainfo.font.color := c;
  viewfo.font.color := c;
  mainfo.pm_main.menu.font.color := c;
  mainfo.pm_pl.menu.font.color := c;
  mainfo.pm_flv.menu.font.color := c;
  mainfo.pm_last.menu.font.color := c;
  mainfo.pm_chanels.menu.font.color := c;
  mainfo.pm_mypls.menu.font.color := c;
  mainfo.l_trackinfo.font.color := c;

  z := tintegeredit2.value;
  mainfo.font.height := z;
  mainfo.s_top.frame.font.height := z;
  topfo.font.height := z;
  bottomfo.font.height := z;
  playlistfo.font.height := z;
  setsfo.font.height := z;
  set2fo.font.height := z;
  askfo.font.height := z;
  chanelfo.font.height := z;
  copyfo.font.height := z;
  aboutfo.font.height := z;
  previewfo.font.height := z;
  viewfo.font.height := z;

  mainfo.pm_main.menu.font.height := z;
  mainfo.pm_pl.menu.font.height := z;
  mainfo.pm_flv.menu.font.height := z;
  mainfo.pm_last.menu.font.height := z;
  mainfo.pm_chanels.menu.font.height := z;
  mainfo.pm_mypls.menu.font.height := z;

  f := tfont.create;
  f.color := c;
  f.height := z;
  f.style := [fs_bold];
  mainfo.wg_playlist.rowfonts[0].assign(f);

  mainfo.fr_form.template.colorframe := tcoloredit1.value;
  mainfo.fr_form.template.framewidth := tintegeredit1.value;

  mainfo.frame_edits.template.colorframe := tcoloredit4.value;
  mainfo.frame_edits.template.framewidth := tintegeredit3.value;
  mainfo.frame_edits.template.colorclient := tcoloredit5.value;

  mainfo.fr_panels.template.colorframe := tcoloredit9.value;
  mainfo.fr_panels.template.framewidth := tintegeredit5.value;
  mainfo.fr_panels.template.colorclient := tcoloredit10.value;

  mainfo.fr_menu.template.colorclient := tcoloredit10.value;

  mainfo.f_top.template.fade_color[0] := tcoloredit12.value;
  mainfo.f_top.template.fade_color[1] := tcoloredit11.value;
  if (tcoloredit12.value = cl_transparent) and (tcoloredit12.value = cl_transparent)
    then mainfo.f_top.template.fade_opacity := cl_transparent
  else mainfo.f_top.template.fade_opacity := cl_none;

  mainfo.f_bottom.template.fade_color[0] := tcoloredit14.value;
  mainfo.f_bottom.template.fade_color[1] := tcoloredit13.value;
  if (tcoloredit14.value = cl_transparent) and (tcoloredit14.value = cl_transparent)
    then mainfo.f_bottom.template.fade_opacity := cl_transparent
  else mainfo.f_bottom.template.fade_opacity := cl_none;

  mainfo.fr_topbuttoms.template.colorframe := tcoloredit21.value;
  mainfo.fr_topbuttoms.template.framewidth := tintegeredit6.value;
  mainfo.f_topbuttons.template.fade_color[0] := tcoloredit20.value;
  mainfo.f_topbuttons.template.fade_color[1] := tcoloredit19.value;
  if (tcoloredit20.value = cl_transparent) and (tcoloredit19.value = cl_transparent)
    then mainfo.f_topbuttons.template.fade_opacity := cl_transparent
  else mainfo.f_topbuttons.template.fade_opacity := cl_none;

  mainfo.fr_mainbuttons.template.colorframe := tcoloredit24.value;
  mainfo.fr_mainbuttons.template.framewidth := tintegeredit7.value;
  mainfo.f_mainbuttons.template.fade_color[0] := tcoloredit23.value;
  mainfo.f_mainbuttons.template.fade_color[1] := tcoloredit22.value;

  if (tcoloredit23.value = cl_transparent) and (tcoloredit22.value = cl_transparent)
    then mainfo.f_mainbuttons.template.fade_opacity := cl_transparent
  else mainfo.f_mainbuttons.template.fade_opacity := cl_none;

  mainfo.fr_bottombutton.template.colorframe := tcoloredit35.value;
  mainfo.fr_bottombutton.template.framewidth := tintegeredit9.value;
  mainfo.f_bottombutton.template.fade_color[0] := tcoloredit34.value;
  mainfo.f_bottombutton.template.fade_color[1] := tcoloredit33.value;

  if (tcoloredit34.value = cl_transparent) and (tcoloredit33.value = cl_transparent)
    then mainfo.f_bottombutton.template.fade_opacity := cl_transparent
  else mainfo.f_bottombutton.template.fade_opacity := cl_none;

  mainfo.frame_buttons.template.colorframe := tcoloredit6.value;
  mainfo.frame_buttons.template.framewidth := tintegeredit4.value;
  mainfo.f_buttons.template.fade_color[0] := tcoloredit7.value;
  mainfo.f_buttons.template.fade_color[1] := tcoloredit8.value;
  if (tcoloredit7.value = cl_transparent) and (tcoloredit8.value = cl_transparent)
    then mainfo.f_buttons.template.fade_opacity := cl_transparent
  else mainfo.f_buttons.template.fade_opacity := cl_none;

  mainfo.f_tabbutton.template.fade_color[0] := tcoloredit32.value;
  mainfo.f_tabbutton.template.fade_color[1] := tcoloredit31.value;
  if (tcoloredit32.value = cl_transparent) and (tcoloredit31.value = cl_transparent)
    then mainfo.f_tabbutton.template.fade_opacity := cl_transparent
  else mainfo.f_tabbutton.template.fade_opacity := cl_none;

  mainfo.f_scrollH.template.fade_color[0] := tcoloredit17.value;
  mainfo.f_scrollH.template.fade_color[1] := tcoloredit16.value;
  if (tcoloredit17.value = cl_transparent) and (tcoloredit16.value = cl_transparent)
    then mainfo.f_scrollH.template.fade_opacity := cl_transparent
  else mainfo.f_scrollH.template.fade_opacity := cl_none;

  mainfo.f_scrollV.template.fade_color[0] := tcoloredit18.value;
  mainfo.f_scrollV.template.fade_color[1] := tcoloredit15.value;
  if (tcoloredit18.value = cl_transparent) and (tcoloredit15.value = cl_transparent)
    then mainfo.f_scrollV.template.fade_opacity := cl_transparent
  else mainfo.f_scrollV.template.fade_opacity := cl_none;

  mainfo.fr_progress.template.colorframe := tcoloredit27.value;
  mainfo.fr_progress.template.framewidth := tintegeredit8.value;
  mainfo.fr_progress.template.colorclient := tcoloredit26.value;

  mainfo.pb_position.bar_face.fade_color[0] := tcoloredit25.value;
  mainfo.pb_position.bar_face.fade_color[1] := tcoloredit28.value;

  if (tcoloredit25.value = cl_transparent) and (tcoloredit28.value = cl_transparent)
    then mainfo.pb_position.bar_face.fade_opacity := cl_transparent
  else mainfo.pb_position.bar_face.fade_opacity := cl_none;

  mainfo.pb_volume.bar_face.fade_color[0] := tcoloredit25.value;
  mainfo.pb_volume.bar_face.fade_color[1] := tcoloredit28.value;

  if (tcoloredit25.value = cl_transparent) and (tcoloredit28.value = cl_transparent)
    then mainfo.pb_volume.bar_face.fade_opacity := cl_transparent
  else mainfo.pb_volume.bar_face.fade_opacity := cl_none;

  mainfo.pb_brightness.bar_face.fade_color[0] := tcoloredit25.value;
  mainfo.pb_brightness.bar_face.fade_color[1] := tcoloredit28.value;

  if (tcoloredit25.value = cl_transparent) and (tcoloredit28.value = cl_transparent)
    then mainfo.pb_brightness.bar_face.fade_opacity := cl_transparent
  else mainfo.pb_brightness.bar_face.fade_opacity := cl_none;

  mainfo.pb_saturation.bar_face.fade_color[0] := tcoloredit25.value;
  mainfo.pb_saturation.bar_face.fade_color[1] := tcoloredit28.value;

  if (tcoloredit25.value = cl_transparent) and (tcoloredit28.value = cl_transparent)
    then mainfo.pb_saturation.bar_face.fade_opacity := cl_transparent
  else mainfo.pb_saturation.bar_face.fade_opacity := cl_none;

  mainfo.pb_contrast.bar_face.fade_color[0] := tcoloredit25.value;
  mainfo.pb_contrast.bar_face.fade_color[1] := tcoloredit28.value;

  if (tcoloredit25.value = cl_transparent) and (tcoloredit28.value = cl_transparent)
    then mainfo.pb_contrast.bar_face.fade_opacity := cl_transparent
  else mainfo.pb_contrast.bar_face.fade_opacity := cl_none;

  mainfo.pb_hue.bar_face.fade_color[0] := tcoloredit25.value;
  mainfo.pb_hue.bar_face.fade_color[1] := tcoloredit28.value;

  if (tcoloredit25.value = cl_transparent) and (tcoloredit28.value = cl_transparent)
    then mainfo.pb_hue.bar_face.fade_opacity := cl_transparent
  else mainfo.pb_hue.bar_face.fade_opacity := cl_none;

  mainfo.pb_gamma.bar_face.fade_color[0] := tcoloredit25.value;
  mainfo.pb_gamma.bar_face.fade_color[1] := tcoloredit28.value;

  if (tcoloredit25.value = cl_transparent) and (tcoloredit28.value = cl_transparent)
    then mainfo.pb_gamma.bar_face.fade_opacity := cl_transparent
  else mainfo.pb_gamma.bar_face.fade_opacity := cl_none;

  if tbooleanedit2.value
    then
  begin
    mainfo.timer_animate.enabled := true;
    mainfo.b_animate.imagelist := mainfo.im_animate;
  end
  else
  begin
    mainfo.timer_animate.enabled := false;
    mainfo.b_animate.imagelist := nil;
  end;

  mainfo.b_animate.face.image.clear;
  if tfilenameedit2.value > ''
    then
  try
    b := true;
    if not fileexists(main.shemeimagepath + extractfilename(tfilenameedit2.value))
      then
      if not trycopyfile(tfilenameedit2.value, main.shemeimagepath + extractfilename(tfilenameedit2.value))
        then b := false;

    if b then tfilenameedit2.value := main.shemeimagepath + extractfilename(tfilenameedit2.value);

    mainfo.b_animate.face.image.loadfromfile(tfilenameedit2.value);
  except
    mainfo.show_ask(editshemefo, 'Error while loading video fon', '', false, false, false);
    writeln(tfilenameedit2.value);
  end;

  c := tcoloredit29.value;
  mainfo.flv.datacols.colorselect := c;
  mainfo.wg_fl.datacols.colorselect := c;
  mainfo.wg_last.datacols.colorselect := c;
  mainfo.wg_myplaylists.datacols.colorselect := c;
  mainfo.wg_radio.datacols.colorselect := c;
  mainfo.wg_tv.datacols.colorselect := c;
  mainfo.wg_playlist.datacols.colorselect := c;
  mainfo.st.datacols.colorselect := c;
  setsfo.sg_debug.datacols.colorselect := c;
  setsfo.edit_mplayercache.dropdown.cols[0].colorselect := c;
  setsfo.edit_audiooutput.dropdown.cols[0].colorselect := c;
  setsfo.edit_videooutput.dropdown.cols[0].colorselect := c;
  setsfo.edit_cdrom.dropdown.cols[0].colorselect := c;
  setsfo.edit_tvtuner.dropdown.cols[0].colorselect := c;
  setsfo.edit_webcam.dropdown.cols[0].colorselect := c;
  setsfo.edit_scheme.dropdown.cols[0].colorselect := c;
  setsfo.edit_language.dropdown.cols[0].colorselect := c;
  setsfo.edit_icons.dropdown.cols[0].colorselect := c;

  mainfo.s_video.color := tcoloredit30.value;
  viewfo.s_video.color := tcoloredit30.value;

  mainfo.frame.framewidth := tintegeredit1.value;
  main.mainfoframewidth := mainfo.frame.framewidth;
  if mainfo.button_windowstyle.tag = 1 then
  begin
    mainfo.bounds_cymin := mainfo.s_bottom.height + main.mainfoframewidth * 2;
    mainfo.height := mainfo.s_bottom.height + main.mainfoframewidth * 2;
  end;
end;

procedure teditshemefo.on_resize(const sender: TObject);
begin
  width := gui_getpointerpos.x - moveXX;
  height := gui_getpointerpos.y - moveYY;
end;

procedure teditshemefo.on_move(const sender: TObject);
begin
  left := gui_getpointerpos.x - moveXX;
  top := gui_getpointerpos.y - moveYY;
end;

procedure teditshemefo.on_childmouseevens(const sender: twidget;
  var ainfo: mouseeventinfoty);
begin
  if (ainfo.eventkind = EK_BUTTONPRESS) then
  begin
    bringtofront;
    if (tdockhandle1 = sender) then
    begin
      moveXX := gui_getpointerpos.x - width;
      moveYY := gui_getpointerpos.y - height;
      timer_resize.enabled := true;
    end
    else
      if (s_top = sender)
        or (sender = editshemefo)
        then
      begin
        moveXX := gui_getpointerpos.x - left;
        moveYY := gui_getpointerpos.y - top;
        timer_move.enabled := true;
      end;
  end;
  if (ainfo.eventkind = EK_BUTTONRELEASE) then
  begin
    if timer_move.enabled then timer_move.enabled := false;
    if timer_resize.enabled then timer_resize.enabled := false;
  end;
end;

procedure teditshemefo.on_close(const sender: TObject);
begin
  close;
end;

procedure teditshemefo.on_maximize(const sender: TObject);
begin
  if fo_maximized in editshemefo.options
    then
  begin
    editshemefo.options := editshemefo.options - [fo_maximized];
    tdockhandle1.visible := true;
  end else
  begin
    editshemefo.options := editshemefo.options + [fo_maximized];
    tdockhandle1.visible := false;
  end;
  application.processmessages;
end;

end.
